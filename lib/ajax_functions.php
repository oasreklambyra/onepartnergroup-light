<?php 
/*==============================================================================

  # Ajax hide cookie
  # Ajax update_preference_cookie
  # ajax_sort_blog_posts()
  # ajax_load_courses()
  # ajax_load_documents()
  # ajax_load_jobs()
  # ajax_filter_jobs()
  # ajax_load_organic()
  # ajax_load_posts()
  # ajax_load_news()

==============================================================================*/

/*==============================================================================
  # Ajax hide cookie
==============================================================================*/

function set_seen_cookie_box(){

  setcookie( 'seen_cookie_box', 'seen', time()+num_of_weeks(26), '/' );

  die();
}
add_action("wp_ajax_set_seen_cookie_box", "set_seen_cookie_box");
add_action("wp_ajax_nopriv_set_seen_cookie_box", "set_seen_cookie_box");


/*==============================================================================
  # Ajax update_preference_cookie
==============================================================================*/

function update_preference_cookie(){

  $location = false;
  $data = $_POST['data'];

  $value = ( $data['value'] !== 'empty_cookie' ) ? $data['value'] : "";

  if( $value ) :
    setcookie( $data['action'], $value, time()+num_of_weeks(26), '/' );

  else :
    setcookie( $data['action'], '', time()-3600, '/' );

  endif;

  if( $data['action'] === 'office' ) :

    $main_office = get_field( 'main_office', 'options' );
    $main_office_slug = ( $main_office ) ? $main_office->post_name : '';
    $location_slug = ( $data['value'] === 'main_office' ) ? $main_office_slug : $data['value'];
    $location = ( $data['value'] !== 'empty_cookie' ) ? $location_slug : "/";
  

  elseif( $data['action'] === 'office-contact' ) :

    $main_office = get_field( 'main_office', 'options' );
    $template_kontakt = get_field( 'template_kontakt', 'options' );
    $link = ( $template_kontakt ) ? get_the_permalink( $template_kontakt->ID ) : ''; 
    $contact_slug = str_replace( get_home_url(), '', $link );

    $main_office_slug = ( $main_office ) ? $contact_slug : '';
    $location_slug = ( $data['value'] === 'main_office' ) ? $main_office_slug : $data['value'].$contact_slug;
    $location = ( $data['value'] !== 'empty_cookie' ) ? $location_slug : $contact_slug;


  elseif( $data['action'] === 'preference' ) :

    $template_name = 'template_'.$data['value'];

    if ( $template = get_field( $template_name, 'options' ) ) : 
      $standard_link = get_permalink( $template->ID );
      $standard_slug = str_replace( get_home_url(), '', $standard_link);

      $url = explode('/', $_SERVER[REQUEST_URI]);
      $url = array_filter($url, function($value) { return $value !== ''; });

      $office_slug = ( isset($url[1]) && $url[1] ) ? esc_attr( $url[1] ) : 'no_slug';
      $args = array(
        'posts_per_page'   => 1,
        'post_type'        => 'vacant-job',
        'post_status'      => 'publish',
        'suppress_filters' => false,
        'tax_query' => array(
            array(
              'taxonomy' => 'job_city',
              'field'    => 'slug',
              'terms'    => $office_slug,
            ),
          ),
      );
      $local_job = get_posts( $args );

      if ( $local_job ) :
        $standard_slug = '/'.$office_slug.$standard_slug;

      elseif ( $_COOKIE['office'] && $_COOKIE['office'] !== 'main_office' ) :
        $standard_slug = '/'.$_COOKIE['office'].$standard_slug;

      endif;

      $location = $standard_slug;
    endif;                         

  endif; 

  if( $location ) :
    $location = esc_url(home_url('/'.$location));
  endif;

  echo json_encode([ 'location' => $location ]);

  die();
}
add_action("wp_ajax_update_preference_cookie", "update_preference_cookie");
add_action("wp_ajax_nopriv_update_preference_cookie", "update_preference_cookie");


/*==============================================================================
  # ajax_sort_blog_posts()
==============================================================================*/

function ajax_sort_blog_posts(){

  $term_id = $_POST['term_id'];
  $args = array(
    'posts_per_page'   => 3,
  );
 
  if( $term_id != 'alla' ) : 
    $args['category'] = esc_attr( $term_id );
  endif;

  $posts_array = get_posts( $args );

  ob_start();
    if( $posts_array ) :
      foreach( $posts_array as $post ) :
        output_blog_item_card( $post );
      endforeach; 
    endif;
  $output = ob_get_clean();

  echo json_encode([ 'output' => $output ]);
  die();
}
add_action("wp_ajax_ajax_sort_blog_posts", "ajax_sort_blog_posts");
add_action("wp_ajax_nopriv_ajax_sort_blog_posts", "ajax_sort_blog_posts");


/*==============================================================================
  # ajax_load_courses()
==============================================================================*/

function ajax_load_courses(){

  $data = $_POST['data'];
  $per_page = 18;

  $args = array(
    'posts_per_page'   => $per_page,
    'offset'           => $data['lmOffset'],
    'post_type'        => 'course',
    'orderby'          => 'menu_order title',
    'order'            => 'ASC',
    'post_status'      => 'publish',
    'suppress_filters' => false 
  );

  if ( $data['searchQuery'] ) :

    $args['s'] = $data['searchQuery'];

  endif;

  if ( $data['categories'] ) :

    $terms = array();

    $args['tax_query'] = array(
      array(
        'taxonomy'  => 'course_category',
        'field'     => 'slug',
        'terms'     => $data['categories'],
      )
    );

  endif;

  $courses = get_posts( $args );

  $debug = $args;

//Get all posts so it is possible to count total
  $args['posts_per_page'] = -1;
  $total_results = count( get_posts( $args ) );
  $has_more = ( $data['lmOffset']+$per_page < $total_results ) ? true : false;

  ob_start();

    if( $courses ) :
      foreach( $courses as $course ) :

        $terms = wp_get_post_terms( $course->ID, 'course_category', $args );
        $url = '';

        foreach ( $terms as $term ) :
          if ( $term->parent  == 0 ) :

            $category_image = get_field( 'category_image', $term );
            $url = ( $category_image ) ? $category_image['url'] : '';

            break;
            
          endif;
        endforeach;

        $args = array(
          'type'      => 'course',
          'permalink'   => get_permalink( $course->ID ),
          'image_sizes' => array(
            array(
              'url' => $url,
            ),
          ),
          'title'     => get_the_title( $course->ID ),
        );
        output_item_card( $args );

      endforeach;
    endif;


  $output = ob_get_clean();

  echo json_encode([ 'output' => $output, 'has_more' => $has_more, 'total_results' => $total_results, 'debug' => $debug ]);
  die();
}
add_action("wp_ajax_ajax_load_courses", "ajax_load_courses");
add_action("wp_ajax_nopriv_ajax_load_courses", "ajax_load_courses");


/*==============================================================================
  # ajax_load_documents()
==============================================================================*/

function ajax_load_documents(){

  $data = $_POST['data'];
  $per_page = 18;

  $args = array(
    'posts_per_page'   => $per_page,
    'offset'           => $data['lmOffset'],
    'post_type'        => 'document',
    'orderby'          => 'menu_order title',
    'order'            => 'ASC',
    'post_status'      => 'publish',
    'suppress_filters' => false 
  );

  if ( $data['searchQuery'] ) :

    $args['s'] = $data['searchQuery'];

  endif;

  if ( $data['categories'] ) :

    $terms = array();

    $args['tax_query'] = array(
      array(
        'taxonomy'  => 'document_category',
        'field'     => 'slug',
        'terms'     => $data['categories'],
      )
    );

  endif;


  $documents = get_posts( $args );

//Get all posts so it is possible to count total
  $args['posts_per_page'] = -1;
  $total_results = count( get_posts( $args ) );
  $has_more = ( $data['lmOffset']+$per_page < $total_results ) ? true : false;

  ob_start();

    if( $documents ) :
      foreach( $documents as $document ) :

        $icon = get_field( 'icon', $document->ID );
        $args = array(
          'type'      => 'document',
          'permalink'   => get_field( 'document', $document->ID ),
          'target'      => '_blank',
          'image_sizes' => array(
            array(
              'url'   => $icon['sizes']['icon'],
              'retina' => $icon['sizes']['icon_retina'],
            ),
          ),
          'title'     => $document->post_title,
        );

        output_item_card( $args );

      endforeach;
    endif;

  $output = ob_get_clean();

  echo json_encode([ 'output' => $output, 'has_more' => $has_more, 'total_results' => $total_results ]);
  die();
}
add_action("wp_ajax_ajax_load_documents", "ajax_load_documents");
add_action("wp_ajax_nopriv_ajax_load_documents", "ajax_load_documents");



/*==============================================================================
  # ajax_load_jobs()
==============================================================================*/

function ajax_load_jobs(){

  $data = $_POST['data'];

  $args = array(
    'offset' => $data['lmOffset'],
  );

  if( isset($data['searchQuery']) && $data['searchQuery'] ) : 
    $args['s'] = $data['searchQuery'];
  endif;

  if( isset($data['serviceTypes']) && $data['serviceTypes'] ) :
    $tax_query[] = array(
      'taxonomy' => 'job_servicetype',
      'field'    => 'slug',
      'terms'    => $data['serviceTypes']
    );
  endif;

  if( isset($data['cities']) && $data['cities'] ) : 
    $tax_query[] = array(
      'taxonomy' => 'job_city',
      'field'    => 'slug',
      'terms'    => $data['cities']
    );
  endif;

  if( isset($data['categories']) && $data['categories'] ) : 
    $tax_query[] = array(
      'taxonomy' => 'job_category',
      'field'    => 'slug',
      'terms'    => $data['categories']
    );
  endif;

  if( $tax_query ) :
    $args['tax_query'] = $tax_query;
  endif;


  $vacant_jobs = get_vacant_jobs( $args );

  ob_start();

    if( $vacant_jobs['jobs'] ) : 
      
      global $post;
      foreach( $vacant_jobs['jobs'] as $post ) : setup_postdata( $post );

        global $post;
        $city = wp_get_post_terms( get_the_ID(), 'job_city' );
        $city_name = ( isset($city[0]) ) ? $city[0]->name : '';
        $city_slug = ( isset($city[0]) ) ? $city[0]->slug : '';
        $post_slug = $post->post_name;
        $permalink = vacant_job_permalink( $post );


        $args = array(
          'permalink'   => $permalink,
          'image_sizes' => array(
            array(
              'url' => get_field( 'logo' ),
            ),
          ),
          'title'     => get_the_title(),
          'text'      => $city_name,
        );
        output_item_card( $args );

      endforeach;
      wp_reset_postdata();
    endif;
  $output = ob_get_clean();

  echo json_encode([ 'output' => $output, 'has_more' => $vacant_jobs['has_more'] ]);
  die();
}
add_action("wp_ajax_ajax_load_jobs", "ajax_load_jobs");
add_action("wp_ajax_nopriv_ajax_load_jobs", "ajax_load_jobs");


/*==============================================================================
  # ajax_filter_jobs()
==============================================================================*/

function ajax_filter_jobs(){

  $data = $_POST['data'];
  $tax_query = array();

  //Update/Set $_COOKIE['job_cat']
  $job_cat = ($data['categories']) ? implode(',',$data['categories']) : '';
  setcookie( 'job_cat', $job_cat, time()+num_of_weeks(26), '/' );

  //Prepare query
  $args = array(
    'offset' => 0,
  );

  if( isset($data['searchQuery']) && $data['searchQuery'] ) : 
    $args['s'] = $data['searchQuery'];
  endif;

  if( isset($data['serviceTypes']) && $data['serviceTypes'] ) :
    $tax_query[] = array(
      'taxonomy' => 'job_servicetype',
      'field'    => 'slug',
      'terms'    => $data['serviceTypes']
    );
  endif;

  if( isset($data['cities']) && $data['cities'] ) : 
    $tax_query[] = array(
      'taxonomy' => 'job_city',
      'field'    => 'slug',
      'terms'    => $data['cities']
    );
  endif;

  if( isset($data['categories']) && $data['categories'] ) : 
    $data['categories'] = array_filter($data['categories'], function($value) { return $value !== ''; });
    if( $data['categories'] ) :
      $tax_query[] = array(
        'taxonomy' => 'job_category',
        'field'    => 'slug',
        'terms'    => $data['categories']
      );
    endif;
  endif;

  if( $tax_query ) :
    $args['tax_query'] = $tax_query;
  endif;

  //Get query
  $vacant_jobs = get_vacant_jobs( $args );

  //Get all posts so it is possible to count total
  $args['posts_per_page'] = -1;
  $args['post_type'] = 'vacant-job';
  $total_results = count( get_posts( $args ) );

  //Output query
  ob_start();

    if( $vacant_jobs['jobs'] ) : 
      
      global $post;
      foreach( $vacant_jobs['jobs'] as $post ) : setup_postdata( $post );

        global $post;
        $city = wp_get_post_terms( get_the_ID(), 'job_city' );
        $city_name = ( isset($city[0]) ) ? $city[0]->name : '';
        $city_slug = ( isset($city[0]) ) ? $city[0]->slug : '';
        $post_slug = $post->post_name;
        $permalink = vacant_job_permalink( $post );


        $args = array(
          'permalink'   => $permalink,
          'image_sizes' => array(
            array(
              'url' => get_field( 'logo' ),
            ),
          ),
          'title'     => get_the_title(),
          'text'      => $city_name,
        );
        output_item_card( $args );

      endforeach;
      wp_reset_postdata();

    else :

      echo '<div class="col-12">';
        output_no_jobs_found();
      echo '</div>';

    endif;
  $output = ob_get_clean();

  echo json_encode([ 'output' => $output, 'has_more' => $vacant_jobs['has_more'], 'total_results' => $total_results ]);
  die();
}
add_action("wp_ajax_ajax_filter_jobs", "ajax_filter_jobs");
add_action("wp_ajax_nopriv_ajax_filter_jobs", "ajax_filter_jobs");



/*==============================================================================
  # ajax_load_organic()
==============================================================================*/

function ajax_load_organic(){

  $data = $_POST['data'];

  if( function_exists( 'relevanssi_do_query' )) :

    global $wp_query;

    $s = $_POST['data']['searchQuery'];
    $paged = (float)$data['organicPage'];

    $args = array (
      's' => $s,
      'paged' => $paged
    );

    $ajax_search_query = new WP_Query( $args );
    $temp_query = $wp_query;
    $wp_query   = NULL;
    $wp_query   = $ajax_search_query;

    relevanssi_do_query( $ajax_search_query );
    
    $has_more = ( $ajax_search_query->max_num_pages > $paged ) ? true : false;

    ob_start();

      if ( $ajax_search_query->have_posts() ) :

          while ( $ajax_search_query->have_posts() ) : $ajax_search_query->the_post();
            
            global $post;
            output_site_search_content( $post );

          endwhile;

          wp_reset_postdata();

      endif;

    $output = ob_get_clean();

    $wp_query = NULL;
    $wp_query = $temp_query;

    echo json_encode([ 'output' => $output, 'has_more' => $has_more ]);

  endif;

  die();
}
add_action("wp_ajax_ajax_load_organic", "ajax_load_organic");
add_action("wp_ajax_nopriv_ajax_load_organic", "ajax_load_organic");


/*==============================================================================
  # ajax_load_posts()
==============================================================================*/

function ajax_load_posts(){

  $data = $_POST['data'];

  $args = array(
    'posts_per_page'   => 11,
    'offset'           => $data['postsOffset'],
    'exclude'          => $data['exclude'],
  );

  if ( isset($data['searchQuery']) && $data['searchQuery'] ) : 

    $args['s'] = $data['searchQuery'];

  endif; 

  if ( isset($data['category']) && $data['category'] ) :

    $args['category_name'] = $data['category'];

  endif;

  $blog_items = get_posts( $args );
  $has_more = ( count( $blog_items ) > 10 ) ? true : false;

  ob_start();

    $clearfix_key = 2;
    $add_two = false;
    foreach( $blog_items as $key => $blog_item ) :

      if ( $key < 10 ) :

        $large = ( $key === 0 || $key === 6 ) ? true : false;

        output_blog_item_card( $blog_item, $large );

        if ( (($key+1)%2) === 0 ) :
          echo '<div class="clearfix d-md-none"></div>';
        endif;

        if( (($key+1)%$clearfix_key) === 0 ) : 

          echo '<div class="clearfix d-none d-md-block"></div>';

          $clearfix_key += ( $add_two ) ? 2: 3;
          $add_two = ( $add_two ) ? false : true;

        endif;

      endif;

    endforeach;

  $output = ob_get_clean();

  echo json_encode([ 'output' => $output, 'has_more' => $has_more ]);
  die();
}
add_action("wp_ajax_ajax_load_posts", "ajax_load_posts");
add_action("wp_ajax_nopriv_ajax_load_posts", "ajax_load_posts");



/*==============================================================================
  # ajax_load_news()
==============================================================================*/

function ajax_load_news(){

  $data = $_POST['data'];
  $tax_query = array();

  $args = array(
    'posts_per_page'   => 11,
    'post_type'        => 'news',
    'offset'           => $data['postsOffset'],
    'exclude'          => $data['exclude'],
  );

  if ( isset($data['searchQuery']) && $data['searchQuery'] ) : 

    $args['s'] = $data['searchQuery'];

  endif; 

  if ( isset($data['categories']) && $data['categories'] ) :
    $tax_query[] = array(
      'taxonomy' => 'news_category',
      'field'    => 'slug',
      'terms'    => $data['categories']
    );
  endif;

  if ( isset($data['cities']) && $data['cities'] ) :
    $tax_query[] = array(
      'taxonomy' => 'news_city',
      'field'    => 'slug',
      'terms'    => $data['cities']
    );
  endif;

  if ( $tax_query ) :
    
    $args['tax_query'] = $tax_query;

  endif;



  $blog_items = get_posts( $args );
  $has_more = ( count( $blog_items ) > 10 ) ? true : false;

  ob_start();

    $clearfix_key = 2;
    $add_two = false;
    foreach( $blog_items as $key => $blog_item ) :

      if ( $key < 10 ) :

        $large = ( $key === 0 || $key === 6 ) ? true : false;

        output_blog_item_card( $blog_item, $large );

        if ( (($key+1)%2) === 0 ) :
          echo '<div class="clearfix d-md-none"></div>';
        endif;

        if( (($key+1)%$clearfix_key) === 0 ) : 

          echo '<div class="clearfix d-none d-md-block"></div>';

          $clearfix_key += ( $add_two ) ? 2: 3;
          $add_two = ( $add_two ) ? false : true;

        endif;

      endif;

    endforeach;

  $output = ob_get_clean();

  echo json_encode([ 'output' => $output, 'has_more' => $has_more ]);
  die();
}
add_action("wp_ajax_ajax_load_news", "ajax_load_news");
add_action("wp_ajax_nopriv_ajax_load_news", "ajax_load_news");
<?php 
/*==============================================================================

  # Set cookie
  # Lang
  # Redirect user to office_page if...
  # Add img sizes
  # Clean up the admin menu
  # Sanitize uploaded file name
  # Handle user roles
  # Move Yoast meta to bottom
  # Change excerpt
  # Wrap string with phone- or mail-link
  # Custom admin columns for vacant-jobs
  # Add new term
  # Get dynamic pages
  # Rewrites for office-main-pages
  # Rewrites for vacant-jobs and office-sub-pages
  # Redirects for rewrited pages
  # Add custom post-state for english page and office sub-pages
  # get_home_link()
  # posts_change_post_label()
  # lang_text()
  # Dynamic links for vacant-jobs, coworkers and office sub-pages
  # get_dynamic_archive_info()
  # get_vacant_jobs()
  # send_quick_application()
  # wp_cf7 custom form tag
  # wpcf7 handle sent mail
  # coworker_single_redirect
  # get_default_post_image()
  # custom sort functions
  # hide_quick_application()
  # Push notification on published post

==============================================================================*/

/*==============================================================================
  # Set cookie
==============================================================================*/

function num_of_weeks( $weeks = 4 ){
  return 60*60*24*7*(int)$weeks;
}

//Tries to remove "Admin blocked by cookie" error
setcookie( TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN);
if ( SITECOOKIEPATH != COOKIEPATH ){
  setcookie( TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN);
}

/*==============================================================================
  # Add img sizes
==============================================================================*/

add_action( 'init', 'custom_img_sizes' );
function custom_img_sizes(){

  add_image_size( 'post_thumbnail',  410, 328, true );
  add_image_size( 'post_thumbnail_retina',  820, 656, true );

  add_image_size( 'post_thumbnail_wide',  850, 328, true );
  add_image_size( 'post_thumbnail_wide_retina',  1700, 656, true );

  add_image_size( 'post_main',  1040, 832, true );
  add_image_size( 'post_main_retina',  2080, 1664, true );

  add_image_size( 'search_thumbnail', 200, 130, true );
  add_image_size( 'search_thumbnail_retina', 400, 260, true );

  add_image_size( 'coworker_thumbnail', 200, 240, array( 'center', 'top' ) );
  add_image_size( 'coworker_thumbnail_retina', 400, 480, array( 'center', 'top' ) );

  add_image_size( 'icon',  50, 50 );
  add_image_size( 'icon_retina',  100, 100 );

  add_image_size( 'partner',  110, 9999 );
  add_image_size( 'partner_retina',  200, 9999 );

  add_image_size( 'entry',  415, 230, true );
  add_image_size( 'entry_retina',  830, 460, true );


  add_image_size( 'intro_image',  650, 9999 );
  add_image_size( 'intro_image_retina',  1300, 9999 );
  add_image_size( 'intro_image_sm',  800, 9999 );
  add_image_size( 'intro_image_sm_retina',  1600, 9999 );
  add_image_size( 'intro_image_lg',  1000, 9999 );
  add_image_size( 'intro_image_lg_retina',  2000, 9999 );
  add_image_size( 'intro_image_xl',  1600, 9999 );
  add_image_size( 'intro_image_xl_retina',  2000, 9999 );

  add_image_size( 'block_bg',  600, 500 );
  add_image_size( 'block_bg_retina',  1200, 1000 );
  add_image_size( 'block_bg_sm',  1000, 500 );
  add_image_size( 'block_bg_sm_retina',  2000, 100 );
  add_image_size( 'block_bg_lg',  1200, 550 );
  add_image_size( 'block_bg_lg_retina',  2400, 1100 );
  add_image_size( 'block_bg_xl',  1600, 550 );
  add_image_size( 'block_bg_xl_retina',  2000, 550 );

  //Used in header for shared link on fb
  add_image_size( 'fb_image',  1200, 630, true );

}
add_filter('jpeg_quality', function($arg){ return 100; });


/*==============================================================================
  # Clean up the admin menu
==============================================================================*/

//Remove these pages from the menu so that the user has a easier time finding whats important
add_action( 'admin_menu', 'custom_menu_page_removing' );
function custom_menu_page_removing() { 
  remove_menu_page( 'edit-comments.php' );          //Comments
  //remove_menu_page( 'edit.php' );                   //Posts
}

add_action( 'admin_menu', 'adjust_the_wp_menu', 999 );
function adjust_the_wp_menu() {
  remove_submenu_page( 'themes.php', 'theme-editor.php' );
  remove_submenu_page( 'themes.php', 'widgets.php' );
  //remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
  remove_submenu_page( 'options-general.php', 'options-discussion.php' );
}


/*==============================================================================
  # Sanitize uploaded file name
==============================================================================*/

function safe_file_name( $filename ) {
  if ( $filename ){
    $filename = esc_attr( $filename );
    $filename = str_replace([' ','/',','],'-',strtolower( $filename ));
    $filename = str_replace(['ö'],'o', $filename);
    $filename = str_replace(['å','ä'],'a', $filename);
    $filename = preg_replace( '/-{2,}/', '-', $filename );
  }
  return $filename;
}
add_filter( 'sanitize_file_name', 'safe_file_name', 10 );


/*==============================================================================
  # Handle user roles
==============================================================================*/

remove_role( 'subscriber' );
remove_role( 'editor' );
remove_role( 'author' );

$caps_application_manager = array(
  'read'                 => true,
  'create_posts'         => false,
  'edit_posts'           => false,
  'edit_others_posts'    => false,
  'edit_published_posts' => false,
  'publish_posts'        => false,
  'delete_posts'         => false,
  'edit_pages'           => false,
  'edit_dashboard'       => false,
  'upload_files'         => false,
  'publish_pages'        => false,
);
add_role( 'application_manager', 'Applikation', $caps_application_manager );


/*==============================================================================
  # Move Yoast meta to bottom
==============================================================================*/
global $og_image;
$og_image = false;

add_action( 'wpseo_opengraph', 'change_yoast_seo_og_meta' );

function change_yoast_seo_og_meta() {
   add_filter( 'wpseo_opengraph_image', 'check_og_image' );
}

function check_og_image( $image ) {

  global $og_image;
  $og_image = true;

  return $image;
}

function yoasttobottom() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


/*==============================================================================
  # Change excerpt
==============================================================================*/

/**
 * apply_custom_excerpt()
 * 
 * Makes it possible to make any text into an excerpt with a custom link
 *
 * @param $content full text that will become the excerpt
 * @param $link if set adds a link at the end of the excerpt
 * @param $target  if added the link gets target="_blank"
 * 
 * @return returns the excerpt
 *
 * @version 1.0
 */ 

global $excerpt_link;
global $excerpt_link_target;

function new_excerpt_more( $more ) {
  global $post;
  global $excerpt_link;
  global $excerpt_link_target;
  
  $more_tag  = '... ';
  
  if ( $excerpt_link !== false ){
    $target = '';
    $read_more = __( 'Läs mer' );
    if ( $excerpt_link_target !== false ){
      $target = 'target="_blank"';
    }
    $read_more = '<a class="text-link" href="'.$excerpt_link.'" '.$target.'>'.$read_more.'</a>';
    $more_tag .= $read_more;
  }

  return $more_tag;
}
function new_excerpt_length( $length ) {
  return 20;
}
function rw_trim_excerpt( $text='' )
{
  $text = strip_shortcodes( $text );
  $text = apply_filters('the_content', $text);
  $text = str_replace(']]>', ']]&gt;', $text);
  $excerpt_length = apply_filters('excerpt_length', 'new_excerpt_more' );
  $excerpt_more = apply_filters('excerpt_more', 'new_excerpt_more' );
  return wp_trim_words( $text, $excerpt_length, $excerpt_more );
}

add_filter( 'excerpt_more', 'new_excerpt_more' );
add_filter( 'excerpt_length', 'new_excerpt_length' );
add_filter( 'wp_trim_excerpt', 'rw_trim_excerpt' );

function apply_custom_excerpt( $content, $link = false, $target = false ){
  global $excerpt_link;
  global $excerpt_link_target;
  $excerpt_link = $link;
  $excerpt_link_target = $target;
  return apply_filters( 'get_the_excerpt', $content ); 
}

/*==============================================================================
  # Dynamic links for vacant-jobs, coworkers and office sub-pages
==============================================================================*/

function vacant_job_permalink( $post ){
  $city = wp_get_post_terms( $post->ID, 'job_city' );
  $city_slug = ( isset($city[0]) ) ? $city[0]->slug : '';
  $post_slug = $post->post_name;
  return esc_url(home_url('/')).'lediga-jobb/'.$city_slug.'/'.$post_slug;
}

function get_dynamic_link( $page_slug, $cookie_city = false ){
  $url = explode('/', $_SERVER[REQUEST_URI]);
  $url = array_filter($url, function($value) { return $value !== ''; });

  if ( isset($url[1]) && $url[1] ) :

    $office_slug = esc_attr( $url[1] );
    $args = array(
      'name'            => esc_attr($url[1]),
      'post_type'       => 'office',
      'post_status'     => 'publish',
      'posts_per_page'  => 1,
    );
    $office = get_posts( $args );

    if ( $office[0] ) :

      $page_slug = '/'.$office[0]->post_name.$page_slug;

    endif;

  elseif ( $cookie_city ) :
    $page_slug = '/'.$cookie_city.$page_slug;
    
  endif;

  return esc_url(home_url()).$page_slug;
}



/*==============================================================================
  # Wrap string with phone- or mail-link
==============================================================================*/

/**
 * convert_string_to_link()
 * 
 * Finds phonenumbers and emailadresses inside a given string and
 * wraps them with a tel: or mailto: link
 *
 * @param $string search for phonenumbers and emailadresses inside this string
 * @param $linkclass add a class to the wraped link
 * 
 * @return string with wrapped links
 * 
 * @version 2.0
 */ 

function convert_string_to_link( $string, $linkclass = '' ){
  settype( $string, 'string' );

  //Match phone
  $phone_matches = array(
    '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{2,}[\s][0-9]{2,}[\s][0-9]{2,}[\s][0-9]{2,}',
    '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{2,}[\s][0-9]{2,}[\s][0-9]{2,}',
    '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{2,}[\s][0-9]{2,}',
    '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{5,}',
    '[+0-9]{5,}',
  );
  $phone_reg = '/';
  $phone_reg .= implode( '|', $phone_matches );
  $phone_reg .= '/';

  $string = preg_replace_callback( $phone_reg, function($match) use ($linkclass){
    $class = ( $linkclass ) ? ' class="'.$linkclass.'"' : '';
    $tel = str_replace( [' ','-','–'], '', $match[0] );
    $replacement = '<a href="tel:'.$tel.'"'.$class.'>'.$match[0].'</a>';
    return $replacement;
  }, $string );

  //Match mail
  $mail_matches = array(
    '[a-zA-ZÅÄÖåäö–0-9-_.]{2,}[\@][a-zA-ZÅÄÖåäö–0-9-_.]{2,}[\.][a-zA-ZÅÄÖåäö–0-9-_.]{2,8}',
  );
  $mail_reg = '/';
  $mail_reg .= implode( '|' , $mail_matches );
  $mail_reg .= '/';

  $string = preg_replace_callback( $mail_reg, function($match) use ($linkclass){
    $class = ( $linkclass ) ? ' class="'.$linkclass.'"' : '';
    $replacement = '<a href="mailto:'.$match[0].'"'.$class.'>'.$match[0].'</a>';
    return $replacement;
  }, $string );  

  return $string;
}

/*==============================================================================
  # Add new term
==============================================================================*/

function add_new_term( $term, $tax, $slug, $parent_id = '' ){
  if ( !term_exists( $term, $tax ) ) :
    $args = array(
      'slug' => $slug,
    );
    if ( $parent_id ){
      $args['parent'] = $parent_id;
    }
    $wp_term = wp_insert_term(
      $term,
      $tax,
      $args
    );
    return $wp_term['term_id'];
  endif;

  return null;
}



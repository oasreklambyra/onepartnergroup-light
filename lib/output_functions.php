<?php 
/*==============================================================================

  # output_divider()
  # output_item_card()
  # output_blog_item_card()
  # output_coworker_card()
  # output_no_jobs_found()
  # output_no_courses_found()
  # output_no_documents_found()
  # output_cta_container()
  # output_newsletter_form()
  # output_background_image_style()
  # output_intro_element()
  # output_site_search_content()
  # output_block_jobs_education_documents()
  # output_block_blog_posts()
  # output_block_testimonials()
  # output_block_cta()
  # output_block_text()
  # output_block_text_video_image()
  # output_block_text_large_image()
  # output_block_entires()
  # output_block_info_block()
  # output_block_accordion()
  # output_block_map()
  # output_block_partners()
  # output_block_contactform()
  # output_block_coworkers()

==============================================================================*/

/*==============================================================================
  # output_divider()
==============================================================================*/

function output_divider( $height = 0 ){
	echo '<div class="clearfix" style="height:'.$height.'px;"></div>';
}


/*==============================================================================
  # output_item_card()
==============================================================================*/

/**
 * output_item_card()
 * 
 * outputs html for easy reuse
 *
 * @param $args
 * @param $args['type'] = sets a class that defines styleing for different types
 * @param $args['id'] = define a ID, needed if images should be a bg_img
 * @param $args['image_sizes'] = define image sizes, use depends on type
 * @param $args['permalink'] = link
 * @param $args['target'] = defines target _blank
 * @param $args['title'] = Heading
 * @param $args['text'] = Sub-heading
 *
 * @return outputs a html element of the class .main-item with a wrapped col-12
 * 
 * @version 1.0
 */ 

function output_item_card( $args ){

	if( !$args ) { return ''; }

	$target = ( $args['target'] ) ? ' target="_blank"' : '';
	?>
	<div class="main-item-col <?= ( $args['type'] === 'entry' ) ? 'col-12 col-md-6 col-xl-4' : 'col-12 col-sm-6 col-lg-4' ; ?>">


		<?php if( $args['use_bg'] && $args['image_sizes'] && $args['id'] ) : ?>

			<style type="text/css">

			#<?= $args['id'] ?>{
				background-image: url('<?= $args['image_sizes'][0]['url'] ?>');

			}

			/*Retina*/
			@media all and (-webkit-min-device-pixel-ratio : 1.5),
			all and (-o-min-device-pixel-ratio: 3/2),
			all and (min--moz-device-pixel-ratio: 1.5),
			all and (min-device-pixel-ratio: 1.5) {

				#<?= $args['id'] ?>{
					background-image: url('<?= $args['image_sizes'][0]['retina'] ?>');
				}
			}

		</style>

	<?php endif; ?>


	<div<?= (isset($args['id'])) ? ' id="'.$args['id'].'"': ''; ?> class="main-item box-shadow<?= ( isset($args['type']) ) ? ' '.$args['type'] : ''; ?><?= ( $args['use_bg'] ) ? ' use-bg' : ''; ?>">

	<?= ( isset($args['permalink']) ) ? '<a href="'.$args['permalink'].'"'.$target.'>': '' ; ?>

	<?php if ( !$args['use_bg'] ) : ?>

		<div class="img-container">
			<?php if( $args['image_sizes'] ) : ?>
				<picture>

					<?php if( isset($args['image_sizes'][0]['retina']) ) : ?>
						<source media="(min-width: 0px)" srcset="<?= $args['image_sizes'][0]['url'] ?>, <?= $args['image_sizes'][0]['retina'] ?> 2x">
						<?php endif; ?>

						<img src="<?= $args['image_sizes'][0]['url'] ?>">

					</picture>
				<?php endif; ?>
			</div>

			<?php else : ?>

				<div class="bright-overlay--hover"></div>

			<?php endif; ?>


			<div class="content-container">
				<div>
					<?= ($args['title']) ? '<h3 class="title">'.$args['title'].'</h3>': ''; ?>
					<?= ($args['text']) ? '<p class="text">'.$args['text'].'</p>': ''; ?>

					<?php if ( $args['type'] === 'entry' && $args['text'] && $args['permalink'] ) : ?>

						<div class="text-center marg-top-40">
							<div class="btn btn--blue">Läs mer</div>
						</div>

					<?php endif; ?>
				</div>
			</div>

			<?= ( isset($args['permalink']) ) ? '</a>': '' ; ?>
		</div>
	</div>

<?php }



/*==============================================================================
  # output_blog_item_card()
==============================================================================*/

function output_blog_item_card( $post, $large = false ){

	$permalink = get_the_permalink( $post );
	$title = get_the_title( $post );
	$excerpt = apply_custom_excerpt( $post->post_content );
	$image = ( $temp_img = get_field( 'main_image', $post->ID ) ) ? $temp_img : get_default_post_image();

	$today_date = getdate();
	$pub_date = get_the_date( 'Y-m-d', $post->ID );

	$pub_date_display = $pub_date;
	if( date('Y-m-d',$today_date[0]) === $pub_date ) : 
		$pub_date_display = 'Idag';

	else :
		$time_since_pub = $today_date[0] - strtotime( $pub_date );
		$day = 60*60*24;
		$days_ago = (int)($time_since_pub/$day);

		if( $days_ago === 1 ) : 
			$pub_date_display = '1 dag sedan';

		elseif ( $days_ago <= 7 ) :
			$pub_date_display = $days_ago.' dagar sedan';

		endif;
	endif;
	?>
	<li class="blog-item<?= ( $large ) ? ' blog-item--large' : ''; ?>">
		<a href="<?= $permalink ?>">

			<div class="img-container">
				<picture>
					<?php if( $large ) : ?>
						<source media="(min-width: 1200px)" data-srcset="<?= $image['sizes']['post_thumbnail_wide'] ?>, <?= $image['sizes']['post_thumbnail_wide_retina'] ?> 2x">
						<?php endif; ?>

						<source media="(min-width: 0px)" data-srcset="<?= $image['sizes']['post_thumbnail'] ?>, <?= $image['sizes']['post_thumbnail_retina'] ?> 2x">

							<img class="lazyload" data-src="<?= $image['sizes']['post_thumbnail'] ?>" alt="<?= $title; ?>">
						</picture>
					</div>

					<div class="content-container">

						<h3 class="heading"><?= $title; ?></h3>

						<?php if ( get_post_type( $post ) === 'news' ) : ?>

							<?php 
							$terms = wp_get_post_terms( $post->ID, 'news_city' );
							$news_cities = array();

							foreach ( $terms as $term ) : 

								$news_cities[] = $term->name;

							endforeach;
							?>

							<?php if ( $news_cities ) : ?>

								<div class="categories"><?= implode(' - ', $news_cities ) ?></div>

							<?php endif; ?>

						<?php endif; ?>

						<div class="excerpt"><?= $excerpt ?></div>

						<div class="pub-date"><?= $pub_date_display ?></div>

					</div>

				</a>
			</li>
		<?php }


/*==============================================================================
  # output_coworker_card()
==============================================================================*/

function output_coworker_card( $coworker, $overwrite = array() ){

	$image = get_field( 'image', $coworker->ID );
	$permalink = get_the_permalink( $coworker->ID );
	$name = ( isset($overwrite['name']) ) ? $overwrite['name'] : get_field( 'name', $coworker->ID );
	$surname =  ( isset($overwrite['surname']) ) ? $overwrite['surname'] : get_field( 'surname', $coworker->ID );
	$title = get_field( 'title', $coworker->ID );
	$phone_1 = ( isset($overwrite['phone_1']) ) ? $overwrite['phone_1'] : get_field( 'phone_1', $coworker->ID );
	$phone_2  = ( isset($overwrite['phone_2']) ) ? $overwrite['phone_2'] : get_field( 'phone_2', $coworker->ID );
	$mail = get_field( 'mail', $coworker->ID );
	$mail_br = preg_replace('/(@)/', '<br/>$1', $mail );
	$linkedin = get_field( 'linkedin', $coworker->ID );
	?>
	<div class="coworker-card">

		<a class="permalink" href="<?= $permalink ?>">
			<div class="img-container-outer">
				<div class="img-container">
					<picture>
						<source media="(min-width: 0px)" data-srcset="<?= $image['sizes']['coworker_thumbnail'] ?>, <?= $image['sizes']['coworker_thumbnail_retina'] ?> 2x">
							<img class="lazyload" data-src="<?= $image['sizes']['coworker_thumbnail_retina'] ?>" alt="<?= $title; ?>">
						</picture>
					</div>
				</div>
			</a>

			<?= ( $name ) ? '<h3 class="heading name"><a href="'.$permalink.'">'.$name.'</a></h3>' : '' ; ?>
			<?= ( $surname ) ? '<h3 class="heading surname"><a href="'.$permalink.'">'.$surname.'</a></h3>' : '' ; ?>

			<?= ( $title ) ? '<p class="title">'.$title.'</p>' : '' ; ?>

			<?php if( $phone_1 || $phone_2 ) : ?>
				<div class="phone-container">
					<?= ( $phone_1 ) ? '<p class="phone"><a href="tel:'.$phone_1.'">'.$phone_1.'</a></p>' : '' ; ?>
					<?= ( $phone_2 ) ? '<p class="phone"><a href="tel:'.$phone_2.'">'.$phone_2.'</a></p>' : '' ; ?>
				</div>
			<?php endif; ?>

			<?= ( $mail ) ? '<p class="mail"><a href="mailto:'.$mail.'">'.$mail_br.'</a></p>' : '' ; ?>

			<?= ( $linkedin ) ? '<a href="'.$linkedin.'" target="_blank"><img class="linkedin" src="'.get_template_directory_uri().'/dist/images/linkedin.svg" alt="LinkedIn"></a>' : '' ; ?>

		</div>
	<?php }

/*==============================================================================
  # output_no_jobs_found()
==============================================================================*/

function output_no_jobs_found(){
	?>
	<div class="text-container marg-top-30 marg-bot-50">

		<h3 class="heading"><?= lang_text( "Tyvärr verkar det inte finnas några lediga jobb i denna kategorin", "Unfortunately, there are no vacancies in this category" ); ?></h3>

		<p class="text marg-top-10 marg-bot-30"><?= lang_text( 'Passa på att registrera ditt CV hos oss så kommer vi att kontakta dig när det dyker upp ett passande jobb för just dig!', 'Be sure to register your resume with us and we will contact you when an appropriate job for you is available!' ); ?></p>
		
		<div class="clearfix"></div>

		<a class="btn btn--blue btn--large" href="/registrera-cv"><?= lang_text( 'Registrera CV', 'Register CV' ); ?></a>

	</div>
<?php }


/*==============================================================================
  # output_no_courses_found()
==============================================================================*/

function output_no_courses_found(){
	?>
	<div class="text-container marg-top-30 marg-bot-50">

		<h3 class="heading"><?= lang_text( "Tyvärr så gick det inte att hitta några utbildningar", "Unfortunately there are no courses for this category" ); ?></h3>

	</div>
<?php }


/*==============================================================================
  # output_no_documents_found()
==============================================================================*/

function output_no_documents_found(){
	?>
	<div class="text-container marg-top-30 marg-bot-50">

		<h3 class="heading"><?= lang_text( "Det gick tvärr inte att hitta några dokumentmallar.", "There was no way to find any document templates" ); ?></h3>

	</div>
<?php }


/*==============================================================================
  # output_no_posts_found()
==============================================================================*/

function output_no_posts_found(){
	?>
	<div class="text-container marg-top-30 marg-bot-50">

		<h3 class="heading"><?= lang_text( "Det gick tvärr inte att hitta några inlägg i den valda kategorin.", "There was no way to find any document templates" ); ?></h3>

		<?php /* <p class="text marg-top-10"><?= lang_text( '', '' ); ?></p> */ ?>

	</div>
<?php }

/*==============================================================================
  # output_cta_container()
==============================================================================*/

/**
 * output_cta_container()
 * 
 * outputs html for easy reuse
 *
 * @param $args
 * @param $args['position'] = text-first or cta-first
 * @param $args['content_type'] = newsletter or link
 * @param $args['content'] = form-code or array with links
 * @param $args['heading'] = heading
 * @param $args['text'] = text
 *
 * @return outputs a html element of a cta-block with text, buttons and varying height
 * 
 * @version 1.0
 */ 

function output_cta_container( $args ){

	$heading = ( $args['heading'] ) ? $args['heading'] : '';
	$text = ( $args['text'] ) ? $args['text'] : '';
	$position = ( $args['position'] ) ? $args['position'] : 'text-first';
	$content_type = ( $args['content_type'] ) ? $args['content_type'] : 'link';
	$is_newsletter = ( $content_type === 'newsletter' ) ? true : false;
	?>
	<div class="container-fluid container__call-to-action">		
		<div class="row justify-content-center">
			<div class="d-flex col-12 col-md-10 col-xl-8 <?= ( !$is_newsletter ) ? ' link_block '.$position : '' ; ?>">

				<?= ( $is_newsletter ) ? '<div class="newsletter_cta">' : '' ; ?>

				<?php if ( $heading || $text ) : ?>

					<?php $position_newsletter = ( $position === 'text-first' ) ? 'order-lg-1' : 'order-lg-2' ; ?>
					<?= ( $is_newsletter ) ? '<div class="col-12 col-lg-7 d-flex align-items-end '.$position_newsletter.'">' : '' ; ?>

					<div class="text-container">
						<?= ( $heading ) ? '<h2 class="heading">'.$heading.'</h2><div class="clearfix"></div>' : ''; ?>
						<?= ( $text ) ? '<p class="text">'.$text.'</p>' : ''; ?>
					</div>

					<?= ( $is_newsletter ) ? '</div>' : '' ; ?>
				<?php endif; ?>


				<?php if ( $is_newsletter && $args['content'] ) : ?>


					<div class="newsletter-container col-12 col-lg-5 d-flex align-items-end <?= ( $position === 'cta-first' ) ? 'order-lg-1' : 'order-lg-2' ; ?>">
						<?php output_newsletter_form( array( 'form'=>$args['content'] ) ) ?>
					</div>


					<?php elseif ( $content_type === 'link' && $args['content'] ) : ?>
						
						<div class="cta-block-btn-container">
							
							<?php foreach( $args['content'] as $link ) : ?>

								<?php if( $link['link_type'] === 'office' || $link['link_type'] === 'office_contact' ) : ?>

									<?php 
									$main_office = get_field( 'main_office', 'options' );
									$class = ( $link['link_type'] === 'office' ) ? 'selectize--contact-office': 'selectize--contactpage-office';

									$args = array(
										'posts_per_page'   => -1,
										'orderby'          => 'title',
										'order'            => 'ASC',
										'post_type'        => 'office',
										'post_status'      => 'publish',
										'suppress_filters' => false
									);
									$offices = get_posts( $args );

									if ( $offices && $main_office ) :
										
										$offices[] = (object) [
											'post_name'    => 'main_office',
											'post_title'   => 'Huvudkontor'
										];

										usort( $offices, 'sort_offices' );

									endif;
									?>
									<?php if( $offices ) : ?>


										<select class="selectize <?= $class ?>" placeholder="<?= lang_text( 'Välj lokalkontor' , 'Choose local office' ); ?>">
											<option value=""><?= lang_text( 'Välj lokalkontor' , 'Choose local office' ); ?></option>
											<?php 
											foreach ( $offices as $office ) :
												echo '<option value="'.$office->post_name.'">'.$office->post_title.'</option>';
											endforeach; 
											?>
										</select>

									<?php endif; ?>
									

									<?php elseif( $link['link_type'] === 'no_link' ) : ?>

										<?php else : ?>

											<?php 
											$link_title = $link['title'];
											$link_url = $link['link'];
											?>

											<?php if( $link_url) : ?>
												
												<a class="btn btn--x-large btn--white" href="<?= $link_url ?>"><?= $link_title ?></a>
												<div class="clearfix"></div>

											<?php endif; ?>

										<?php endif; ?>

									<?php endforeach; ?>	

								</div>

							<?php endif; ?>

							<?= ( $is_newsletter ) ? '</div>' : '' ; ?>

						</div>
					</div>
				</div>

			<?php }

/*==============================================================================
  # output_newsletter_form()
==============================================================================*/

function output_newsletter_form( $args = array() ){

	$args['text_before'] = (isset($args['text_before'])&&$args['text_before']) ? $args['text_before'] : '';
	$args['form'] = (isset($args['form'])&&$args['form']) ? $args['form'] : get_field( 'newsletter', 'options' ); 
	?>
	<div class="newsletter">
		<?= ($args['text_before']) ? '<span>'.$args['text_before'].'</span>' : ''; ?>
		<?= $args['form'] ?>
	</div>

<?php }


/*==============================================================================
  # output_background_image_style()
==============================================================================*/

function output_background_image_style( $ele_id, $img_size, $img_array, $align = false ){
	$size_xs = $img_size;
	$size_xs_retina = $img_size.'_retina';
	$size_sm = $img_size.'_sm';
	$size_sm_retina = $img_size.'_sm_retina';
	$size_lg = $img_size.'_lg';
	$size_lg_retina = $img_size.'_lg_retina';
	$size_xl = $img_size.'_xl';
	$size_xl_retina = $img_size.'_xl_retina';
	?>

	<style type="text/css">
	/*Size for xs: <?= $size_xs ?>*/
	#<?= $ele_id; ?>{
		background-image: url( '<?= $img_array['sizes'][$size_xs]; ?>' );
		<?= ( $align ) ? 'background-position: '.$align['x'].' '.$align['y'] : ''; ?> ; ?>
	}
	@media all and (-webkit-min-device-pixel-ratio : 1.5),
	all and (-o-min-device-pixel-ratio: 3/2),
	all and (min--moz-device-pixel-ratio: 1.5),
	all and (min-device-pixel-ratio: 1.5) {
		#<?= $ele_id; ?>{
			background-image: url( '<?= $img_array['sizes'][$size_xs_retina]; ?>' );
		}
	}

	/*Size for sm,md: <?= $size_sm ?>*/
	@media(min-width: 580px ){
		#<?= $ele_id; ?>{
			background-image: url( '<?= $img_array['sizes'][$size_sm]; ?>' );
		}
	}
	@media all and (-webkit-min-device-pixel-ratio : 1.5),
	all and (-o-min-device-pixel-ratio: 3/2),
	all and (min--moz-device-pixel-ratio: 1.5),
	all and (min-device-pixel-ratio: 1.5),
	and (min-width: 580px ){
		#<?= $ele_id; ?>{
			background-image: url( '<?= $img_array['sizes'][$size_sm_retina]; ?>' );
		}
	}

	/*Size for lg: <?= $size_lg ?>*/
	@media(min-width: 992px ){
		#<?= $ele_id; ?>{
			background-image: url( '<?= $img_array['sizes'][$size_lg]; ?>' );
		}
	}
	@media all and (-webkit-min-device-pixel-ratio : 1.5),
	all and (-o-min-device-pixel-ratio: 3/2),
	all and (min--moz-device-pixel-ratio: 1.5),
	all and (min-device-pixel-ratio: 1.5),
	and (min-width: 992px ){
		#<?= $ele_id; ?>{
			background-image: url( '<?= $img_array['sizes'][$size_lg_retina]; ?>' );
		}
	}

	/*Size for xl: <?= $size_xl ?>*/
	@media(min-width: 1200px ){
		#<?= $ele_id; ?>{
			background-image: url( '<?= $img_array['sizes'][$size_xl]; ?>' );
		}
	}
	@media all and (-webkit-min-device-pixel-ratio : 1.5),
	all and (-o-min-device-pixel-ratio: 3/2),
	all and (min--moz-device-pixel-ratio: 1.5),
	all and (min-device-pixel-ratio: 1.5),
	and (min-width: 1200px ){
		#<?= $ele_id; ?>{
			background-image: url( '<?= $img_array['sizes'][$size_xl_retina]; ?>' );
		}
	}

</style>
<?php }


/*==============================================================================
  # output_intro_element()
==============================================================================*/

function output_intro_element( $args = array() ){

	$intro_element_array = array(
		'/dist/images/header-element-1.svg',
		'/dist/images/header-element-2.svg',
		'/dist/images/header-element-3.svg',
		'/dist/images/header-element-4.svg',
		'/dist/images/header-element-5.svg',
	); 
	$rand_ele = rand(0,( count($intro_element_array)-1) );
	$intro_element_url = get_template_directory().$intro_element_array[$rand_ele];

	$defaults = array(
		'heading'   => '',		//Heading for block
		'text'      => '',		//Textfield for block
	);
	$params = wp_parse_args( $args, $defaults );

	?>

	<section class="section section__intro section__intro__element<?= ( is_archive() ) ? ' archive' : '';?>">
		<div class="background-element-container">

			<div class="background-element"><?= file_get_contents($intro_element_url); ?></div>

			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-12 col-md-10 col-xl-8">

						<?php if ( isset($params['heading']) || isset($params['text']) ) : ?> 
						<div class="text-container">
							<?= ( isset($params['heading']) ) ? '<h1 class="heading">'.$params['heading'].'</h1>' : ''; ?>
							<?= ( isset($params['text']) ) ? '<p class="text">'.$params['text'].'</p>' : ''; ?>
						</div>
					<?php endif; ?>

				</div>
			</div>
		</div>

	</div>
</section>

<?php }


/*==============================================================================
  # output_site_search_content()
==============================================================================*/


function output_site_search_content( $post ){

	$title = $post->post_title;
	$info = '';
	$excerpt = '';
	$image = get_template_directory_uri().'/dist/images/search_thumbnail.jpg';
	$image_retina = get_template_directory_uri().'/dist/images/search_thumbnail_retina.jpg';
	$permalink = get_the_permalink( $post->ID );
	$post_type = get_post_type( $post );


//Bloggpost
	if ( $post_type === 'post' ) : 

		$info = lang_text( 'Blogginlägg', 'Blogpost' );
		$excerpt = get_the_excerpt();
		$temp_image = get_field( 'main_image', $post->ID );
		$image = $temp_image['sizes']['search_thumbnail'];
		$image_retina = $temp_image['sizes']['search_thumbnail_retina'];

//Nyheter
	elseif ( $post_type === 'news' ) : 

		$info = lang_text( 'Nyhet', 'News' );
		$excerpt = get_the_excerpt();
		$temp_image = get_field( 'main_image', $post->ID );
		$image = $temp_image['sizes']['search_thumbnail'];
		$image_retina = $temp_image['sizes']['search_thumbnail_retina'];
		

//Regular page
	elseif ( $post_type === 'page' ) :

		$info = lang_text( 'Sida', 'Page - Swedish' );
		$excerpt = ( $temp = get_field( 'intro_text', $post->ID ) ) ? apply_custom_excerpt( $temp ) : '';

		if( $temp_image = get_field( 'intro_image', $post->ID ) ) : 

			$image = $temp_image['sizes']['search_thumbnail'];	
			$image_retina = $temp_image['sizes']['search_thumbnail_retina'];	

		endif;

		$dynamic_pages = get_dynamic_pages();

		foreach( $dynamic_pages as $dynamic_page ) : 
			if ( $dynamic_page && $dynamic_page->ID === $post->ID ) :

				$standard_slug = str_replace( get_home_url(), '', $permalink);
				$permalink = get_dynamic_link( $standard_slug );

			endif; 
		endforeach;


//English page
	elseif ( $post_type === 'english-page' ) :

		$info = lang_text( 'Sida - Engelska', 'Page' );
		$excerpt = ( $temp = get_field( 'intro_text', $post->ID ) ) ? apply_custom_excerpt( $temp ) : '';
        //$permalink = '';

		if( $temp_image = get_field( 'intro_image', $post->ID ) ) : 

			$image = $temp_image['sizes']['search_thumbnail'];	
			$image_retina = $temp_image['sizes']['search_thumbnail_retina'];	

		endif;


	elseif ( $post_type === 'office' ) :

		$info = lang_text( 'Kontor', 'Office' );
		$excerpt = ( $temp = get_field( 'intro_text', $post->ID ) ) ? apply_custom_excerpt( $temp ) : '';
		$permalink = get_home_url().'/'.$post->post_name;

		if( $temp_image = get_field( 'intro_image', $post->ID ) ) : 

			$image = $temp_image['sizes']['search_thumbnail'];	
			$image_retina = $temp_image['sizes']['search_thumbnail_retina'];	

		endif;


	elseif ( $post_type === 'coworker' ) :

		$info = lang_text( 'Medarbetare', 'Coworker' );
		$temp_image = get_field( 'image', $post->ID );
		$image = ( $temp_image ) ? $temp_image['sizes']['medium'] : $image;
		$image_retina = ( $temp_image ) ? $temp_image['sizes']['medium'] : $image_retina;


	elseif ( $post_type === 'vacant-job' ) :

		$temp_city = wp_get_post_terms( $post->ID, 'job_city' );
		$city = ( $temp_city ) ? ' - '.$temp_city[0]->name : '';
		$info = lang_text( 'Ledigt jobb', 'Vacant job' ).$city;
		$excerpt = get_the_excerpt();
		$permalink_slug = end(array_filter(explode('/',get_the_permalink($post->ID)),create_function('$value','return $value !== "";')));
		$permalink = get_home_url().'/lediga-jobb/'.$temp_city[0]->slug.'/'.$permalink_slug;
		$image = get_field( 'logo', $post->ID );
		$image_retina = $image;

    	//PERMALINK De sidor som har kontor har problem med länkar


	elseif ( $post_type === 'course' ) :

		$info = lang_text( 'Utbildning', 'Course' );
		$excerpt = get_the_excerpt();
		$terms = wp_get_post_terms( $post->ID, 'course_category', $args );

		foreach ( $terms as $term ) :
			if ( $term->parent  == 0 ) :

				$category_image = get_field( 'category_image', $term );

				if ( $category_image ) :

					$image = $category_image['url'];
					$image_retina = $category_image['url'];

					break;
				endif;
				
			endif;
		endforeach;


	elseif ( $post_type === 'document' ) :

		$info = lang_text( 'Dokumentmall', 'Document' );
		$excerpt = get_the_excerpt();
		$temp_image = get_field( 'icon', $post->ID );
		$image = $temp_image['sizes']['icon'];
		$image_retina = $temp_image['sizes']['icon_retina'];


	endif;
	?>

	<div class="search-item <?= $post_type ?>">
		<a href="<?= $permalink ?>">
			
			<div class="img-container">		
				<picture>
					<source media="(min-width: 0px)" data-srcset="<?= $image ?>, <?= $image_retina ?> 2x">
						<img class="lazyload" data-src="<?= $image ?>" alt="<?= $title; ?>">
					</picture>
				</div>

				<div class="content-container">

					<h2 class="heading"><?= $title ?></h2>
					<?= ( $info ) ? '<p class="sub-heading">'.$info.'</p>' : ''; ?>
					<?= ( $excerpt ) ? '<p class="excerpt">'.$excerpt.'</p>' : ''; ?>

				</div>

			</a>
		</div>

	<?php }


/*==============================================================================
  # output_block_blog_posts()
==============================================================================*/

function output_block_blog_posts( $args = array() ){

	$defaults = array(
		'id'  					=> '',					//ID for direct links
		'row_number'  			=> '1',					//unique number for each blog block
		'marg_bot'  			=> '200',				//Margin bottom
		'heading' 				=> '',					//Heading
		'text' 					=> '',					//Textfield
		'post_type' 			=> 'post',				//Post or news
		'show_posts' 			=> 'latest',			//Choose which posts to display
		'preset_posts' 			=> array(),				//If show posts equals custom, this should be and array with post objects
		'current_cat' 			=> false,				//If show posts equals categories, this should contain a term object
		'hide_archive_link' 	=> false,				//Hide archve link
		'custom_archive_link' 	=> false,				//Add a custom archive link
	);

	$params = wp_parse_args( $args, $defaults );
	
	$posts_array = array();
	
	if( $params['show_posts'] === 'custom' ) :
		
		if ( $params['preset_posts'] ) :

			foreach( $params['preset_posts'] as $item ) :
				$posts_array[] = $item;
			endforeach;

		endif;

	else : 

		$args = array(
			'posts_per_page'   	=> 3,
			'post_type'			=> $params['post_type']
		);

		if( $params['show_posts'] === 'categories' && $params['current_cat'] ) :
			$args['tax_query'] = array(
				array(
					'taxonomy' => $params['current_cat']->taxonomy,
					'field'    => 'term_id',
					'terms'    => $params['current_cat']->term_id,
				),
			);
		endif; 

		$posts_array = get_posts( $args );

	endif; // ( $params['show_posts'] === 'custom' )


	$terms = array();
	if( $params['post_type'] === 'post' && $params['show_posts'] === 'categories' ) :

		$terms = get_terms( 'category', array( 'hide_empty' => true ) );

	endif; // ( $params['show_posts'] === 'category' )

	
	if ( $posts_array ) :
		?>
		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="container-fluid section section__blog_posts marg-bot-<?= $params['marg_bot'] ?>">

		<div class="row justify-content-center">
			<div class="col-12 col-md-10">

				<div class="row justify-content-center">
					
					<div class="col-12 col-md-6 col-xl-custom">
						<?php if( $params['heading'] || $params['text'] ) : ?>
							<div class="text-container">
								<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>': ''; ?>
								<?= ( $params['text'] ) ? '<p class="text">'.$params['text'].'</p>': ''; ?>
							</div>
						<?php endif; ?>
					</div>

					<div class="col-12 col-md-6 col-xl-custom">
						<?php if( $terms ) : ?>
							<div class="category-container">
								<ul class="category-container__inner">
									<li><a class="blog-category-toggle<?= ( !$params['current_cat'] ) ? ' current': ''; ?>" href="#blog-posts-container-<?= $params['row_number'] ?>" data-value="alla"><?= lang_text( 'Alla', 'All' ); ?></a></li>
									
									<?php foreach( $terms as $term  ) : ?>

										<li><a class="blog-category-toggle<?= ( $params['current_cat']->term_id === $term->term_id ) ? ' current': ''; ?>" href="#blog-posts-container-<?= $params['row_number'] ?>" data-value="<?= $term->term_id ?>"><?= $term->name ?></a></li>

									<?php endforeach; ?>

								</ul>
							</div>
						<?php endif; ?>
					</div>

				</div>

				<div class="row">
					<div class="col-12">			
						<ul id="blog-posts-container-<?= $params['row_number'] ?>" class="blog-posts-container">
							<?php 
							foreach( $posts_array as $post ) :

								output_blog_item_card( $post );

							endforeach; 
							wp_reset_postdata();
							?>
						</ul>
					</div>
				</div>
				

				<div class="row">
					<div class="col-12 btn-container text-center">
						<a class="btn--blue" href="<?= get_post_type_archive_link( 'post' ); ?>">Alla nyheter</a>
					</div>
				</div>


			</div>
		</div>

	</section>
	
<?php endif; ?>
<?php }



/*==============================================================================
  # output_block_testimonials()
==============================================================================*/

function output_block_testimonials( $args = array() ){

	$defaults = array(
		'id'  			=> '',					//ID for direct links
		'marg_bot'  	=> '200',				//Margin bottom
		'show_bg' 		=> true,				//If the background element should be visible
		'testimonials' 	=> array()				//Array with testimonial objects
	);

	$params = wp_parse_args( $args, $defaults );
	$background_element_url = get_template_directory_uri().'/dist/images/testimonials-element.svg';

	if( $params['testimonials'] ) : 
		?>

		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__testimonials marg-bot-<?= $params['marg_bot'] ?><?= ( $params['show_bg'] ) ? ' show-bg': ''; ?>">

		<?php if( $params['show_bg'] ) : ?>

			<div class="background-element">
				<img class="lazyload image" data-src="<?= $background_element_url ?>" alt="OnePartnerGroup element"></div>

			<?php endif; ?>

			<div class="container-fluid">		
				<div class="row justify-content-center">
					<div class="col-12 col-md-10 col-xl-8">

						<div class="owl-carousel">
							<?php foreach ( $params['testimonials'] as $testimonial ) : ?>
								
								<?php 
								$name = get_field( 'name', $testimonial->ID );
								$title = get_field( 'title', $testimonial->ID );
								$text = get_field( 'text', $testimonial->ID );
								$image = get_field( 'image', $testimonial->ID );
								?>

								<div class="item">

									<div class="image-container">
										<img class="image box-shadow-pink" src="<?= $image['url'] ?>" alt="<?= ( $name ) ? $name : 'Rekommendation' ?>">
									</div>

									<div class="content-container">
										<?= ( $name ) ? '<h3 class="heading">'.$name.'</h3><div class="clearfix"></div>' : '' ?>
										<?= ( $title ) ? '<div class="title">'.$title.'</div><div class="clearfix"></div>' : '' ?>
										<?= ( $text ) ? '<p class="text">'.$text.'</p>' : '' ?>
									</div>

								</div>

							<?php endforeach; ?>
						</div>

					</div>
				</div>
			</div>


		</section>

	<?php endif; }



/*==============================================================================
  # output_block_cta()
==============================================================================*/

function output_block_cta( $args = array() ){

	$defaults = array(
		'id'  				=> '',					//ID for direct links
		'row_number'  		=> '1',					//Unique number for each blog block
		'marg_bot'  		=> '200',				//Margin bottom
		'pad_bot' 			=> 'sm',				//Padding bottom; sm, md or lg
		'pad_top' 			=> 'sm',				//Padding top; sm, md or lg
		'bg_type' 			=> 'color',				//Color or image background
		'bg_image' 			=> array(),				//If bg_type equals image, this should be a image object
		'bg_image_url' 		=> '',					//If bg_type equals image, this can be a direct link to the bg_image
		'bg_color' 			=> 'blue',				//White, blue or grey
		'heading' 			=> '',					//Heading
		'text' 				=> '',					//Textfield
		'content_type' 		=> 'newsletter',		//Link or newsletter
		'position' 			=> 'cta-first',			//cta-first or text-first
		'use_unique_code' 	=> false,				//True lets the user type in her own form-tag, false gets the standard from options
		'unique_code'		=> '',					//Html element of the form to use
		'links'				=> array()				//Array with link info
	);

	$params = wp_parse_args( $args, $defaults );



	$content = array();

	if( $params['content_type'] === 'newsletter' ) :

		$content = ( $params['use_unique_code'] && $params['unique_code'] ) ? $params['unique_code'] : get_field('newsletter','options');

	elseif( $params['content_type'] === 'link' && $params['links'] ) :

		$content = $params['links'];

	endif;

	$cta_args = array(
		'position' 		=> $params['position'],
		'content_type' 	=> $params['content_type'],
		'content' 		=> $content,
		'heading' 		=> $params['heading'],
		'text' 			=> $params['text'],
	);

	?>

	<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__call-to-action marg-bot-<?= $params['marg_bot'] ?>">

	<?php 
	if( $params['bg_type'] === 'image' && $params['bg_image'] && !$params['bg_image_url'] ) :

		output_background_image_style( 'cta-block-'.$params['row_number'], 'block_bg', $params['bg_image'], array( 'x'=>'center','y'=>'top') );

	endif;
	?>

	<div<?= ( $params['bg_type'] === 'image' && $params['bg_image'] ) ? ' id="cta-block-'.$params['row_number'].'"' : ''; ?> class="bg-image pad-top-<?= $params['pad_top'] ?> pad-bot-<?= $params['pad_bot'] ?><?= ( $params['bg_color'] ) ? ' '.$params['bg_color'].'-bg': ''; ?>"<?= ( !$params['bg_image'] && $params['bg_image_url'] ) ? ' style="background-image: url('.$params['bg_image_url'].');': ''; ?>>

	<?php output_cta_container( $cta_args ); ?>

</div>

</section>

<?php }


/*==============================================================================
  # output_block_text()
==============================================================================*/

function output_block_text( $args = array() ){

	$defaults = array(
		'id'  				=> '',					//ID for direct links
		'marg_bot'  		=> '200',				//Margin bottom
		'marg_heading'  	=> 'md',				//Margin from heading to text. sm, md or lg
		'padding'  			=> 'sm',				//padding on the block. sm, md or lg
		'bg_color'  		=> 'none',				//grey or none
		'heading' 			=> '',					//Heading
		'text' 				=> '',					//Text
		'content_placement' => 'left',				//Left, center or right
		'display_type' 		=> 'columns',			//columns,fullwidth or standard
	);

	$params = wp_parse_args( $args, $defaults );

	?>
	<?php if ( $params['heading'] || $params['text'] ) : ?>

		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__text marg-bot-<?= $params['marg_bot'] ?>">

		<?= ( $params['bg_color'] !== 'none' ) ? '<div class="background-color pad-'.$params['padding'].'">' : '' ; ?>

		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10 col-xl-8 text-<?= $params['content_placement'] ?>">

					<?php if( $params['heading'] || $params['text'] ) : ?>

						<div class="text-container<?= ( $params['display_type'] !== 'standard' ) ? ' '.$params['display_type'] : ''; ?>">
							<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>' : ''; ?>
							<?= ( $params['text'] ) ? '<div class="text marg-top-'.$params['marg_heading'].'"">'.$params['text'].'</div>': ''; ?>
						</div>

					<?php endif; ?>

				</div>
			</div>
		</div>

		<?= ( $params['bg_color'] !== 'none' ) ? '</div>' : '' ; ?>

	</section>

<?php endif; ?>

<?php }


/*==============================================================================
  # output_block_text_video_image()
==============================================================================*/

function output_block_text_video_image( $args = array() ){

	$defaults = array(
		'id'  				=> '',					//ID for direct links
		'marg_bot'  		=> '200',				//Margin bottom
		'media_placement' 	=> 'left',				//Left or right
		'media_type' 		=> 'image',				//Video or image
		'youtube_id' 		=> '',					//If media_type equals video, this should be a youtube video id
		'image' 			=> array(),				//If media_type equals image,	this should be a image object
		'heading' 			=> '',					//Heading
		'text' 				=> '',					//Text
	);

	$params = wp_parse_args( $args, $defaults );

	?>
	<?php if ( $params['youtube_id'] || $params['image'] || $params['heading'] || $params['text'] ) : ?>

		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__text_video_image marg-bot-<?= $params['marg_bot'] ?>">

		<div class="container-fluid">		
			<div class="row justify-content-center">
				<div class="col-12 col-md-10">

					<div class="row flex-wrap">
						
						<?php if( $params['heading'] || $params['text'] ) : ?>
							<div class="content-center-md col-12 col-md-6 <?= ($params['media_placement']==='right') ? 'order-md-1' : 'order-md-2' ?>">
								<div class="text-container-outer">
									<div class="text-container">
										<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>': ''; ?>
										<?= ( $params['text'] ) ? '<p class="text">'.$params['text'].'</p>': ''; ?>
									</div>
								</div>
							</div>
						<?php endif; ?>

						<?php if( $params['media_type'] === 'video' ) : ?>

							<?php if( $params['youtube_id'] ) : ?>

								<div class="content-center-md col-12 col-md-6 <?= ($params['media_placement']==='right') ? 'order-md-2' : 'order-md-1' ?>">
									<div class="video-container-outer">
										<div class="video-container">
											<div class="video player" data-property="
											{
												videoURL	    : '<?= $params['youtube_id'] ?>',
												containment     : 'self',
												showYTLogo      : false,
												showControls    : true,
												autoPlay	    : false,
												stopMovieOnBlur : false,
												loop		    : false,
												mute		    : false,
												opacity		    : 1,
												gaTrack		    : false,
												quality		    : 'highres',
											}">
											<img class="video-loader" src="<?= get_template_directory_uri().'/dist/images/loading.svg' ?>">

											<div class="video-overlay">
												<img class="video-player-btn" src="<?= get_template_directory_uri().'/dist/images/player.svg' ?>">
											</div>

										</div>
									</div>
								</div>
							</div>

						<?php endif; ?>

						<?php else : ?>

							<?php if( $params['image'] ) : ?>

								<div class="content-center-md col-12 col-md-6 <?= ($params['media_placement']==='right') ? 'order-md-2' : 'order-md-1' ?>">
									
									<div class="image-container">
										<picture>
											<source media="(min-width: 0px)" data-srcset="<?= $params['image']['sizes']['intro_image'] ?>, <?= $params['image']['sizes']['intro_image_retina'] ?> 2x">
												<img class="lazyload image" data-src="<?= $params['image']['sizes']['intro_image'] ?>" alt="<?= $params['image']['alt'] ?>">
											</picture>
										</div>

									</div>

								<?php endif; ?>

							<?php endif; ?>

						</div>

					</div>
				</div>
			</div>
		</section>

	<?php endif; ?>

<?php }


/*==============================================================================
  # output_block_text_large_image()
==============================================================================*/

function output_block_text_large_image( $args = array() ){

	$defaults = array(
		'id'  				=> '',					//ID for direct links
		'marg_bot'  		=> '200',				//Margin bottom
		'media_placement' 	=> 'left',				//Left or right
		'image' 			=> array(),				//Image object
		'heading' 			=> '',					//Heading
		'text' 				=> '',					//Text
	);

	$params = wp_parse_args( $args, $defaults );
	?>
	
	<?php if ( $params['heading'] && $params['text'] ) : ?>

		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__text_large_image marg-bot-<?= $params['marg_bot'] ?>">

		<div class="container-fluid">		
			<div class="row justify-content-center">
				<div class="pos-static col-12 col-md-10 col-lg-8">
					<div class="row flex-wrap <?= ( $params['media_placement'] === 'left' ) ? 'justify-content-md-end' : 'justify-content-md-start' ?>">
						

						<?php if( $params['heading'] || $params['text'] ) : ?>

							<div class="text-col col-12 col-md-6">
								<div class="text-container-outer">
									<div class="text-container">
										<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>': ''; ?>
										<?= ( $params['text'] ) ? '<p class="text">'.$params['text'].'</p>': ''; ?>
									</div>
								</div>
							</div>

						<?php endif; ?>


						<?php if( $params['image'] ) : ?>

							<div class="image-col col-12 col-md-6">
								<div class="image-container">
									<picture>
										
										<source media="(min-width: 1200px)" data-srcset="<?= $params['image']['sizes']['intro_image_sm'] ?>, <?= $params['image']['sizes']['intro_image_sm_retina'] ?> 2x">

											<source media="(min-width: 768px)" data-srcset="<?= $params['image']['sizes']['intro_image'] ?>, <?= $params['image']['sizes']['intro_image_retina'] ?> 2x">

												<source media="(min-width: 0px)" data-srcset="<?= $params['image']['sizes']['icon'] ?>, <?= $params['image']['sizes']['icon'] ?> 2x">

													<img class="lazyload image" data-src="<?= $params['image']['sizes']['intro_image'] ?>" alt="<?= $params['image']['alt'] ?>">

												</picture>
											</div>
										</div>

									<?php endif; ?>


								</div>
							</div>
						</div>
					</div>

				</section>

			<?php endif; ?>

		<?php }


/*==============================================================================
  # output_block_entires()
==============================================================================*/

function output_block_entires( $args = array() ){

	$defaults = array(
		'id'  				=> '',					//ID for direct links
		'row_number'  		=> '1',					//Unique number for each blog block
		'marg_bot'  		=> '200',				//Margin bottom
		'heading' 			=> '',					//Heading
		'text' 				=> '',					//Text
		'links'				=> array()				//Array with link info
	);

	$params = wp_parse_args( $args, $defaults );
	?>

	<?php if ( $params['links'] ) : ?>

		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__entries marg-bot-<?= $params['marg_bot'] ?>">

		<div class="container-fluid">		
			<div class="row justify-content-center">
				<div class="col-12 col-md-10">

					<div class="row flex-wrap">
						
						<?php if( $params['heading'] || $params['text'] ) : ?>
							<div class="col-12 col-lg-custom">
								<div class="text-container">
									<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>': ''; ?>
									<?= ( $params['text'] ) ? '<p class="text">'.$params['text'].'</p>': ''; ?>
								</div>
							</div>
						<?php endif; ?>

						<div class="col-12">
							<div class="row flex-wrap">
								<?php 
								foreach( $params['links'] as $key => $link ) :

									$uniqe_id = 'entry-'.$params['row_number'].'-'.$key;
									$link['id'] = $uniqe_id;

									output_item_card( $link );

								endforeach;
								wp_reset_postdata();
								?>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>

	</section>

<?php endif; ?>

<?php }


/*==============================================================================
  # output_block_info_block()
==============================================================================*/

function output_block_info_block( $args = array() ){

	$defaults = array(
		'id'  				=> '',					//ID for direct links
		'marg_bot'  		=> '200',				//Margin bottom
		'heading' 			=> '',					//Heading
		'text' 				=> '',					//Text
		'show_bg' 			=> true,				//Show or hide element background
		'items' 			=> array()				//Array with item information
	);

	$params = wp_parse_args( $args, $defaults );
	$background_element_url = get_template_directory_uri().'/dist/images/information-block-element.svg';

	?>

	<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__information_block marg-bot-<?= $params['marg_bot'] ?>">

	<?= ( $params['show_bg'] ) ? '<div class="background-element" style="background-image: url('.$background_element_url.');">': ''; ?>

	<div class="container-fluid">
		<div class="row justify-content-center">

			<?php if( $params['heading'] || $params['text'] ) : ?>
				<div class="col-12 col-md-10">
					<div class="main-text-container text-container">
						<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>' : ''; ?>
						<?= ( $params['text'] ) ? '<p class="text">'.$params['text'].'</p>' : ''; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if( $params['items'] ) : ?> 
				<div class="col-12 col-md-10">
					<div class="row content">

						<div class="col-12">
							<div class="row info-row">

								<?php foreach( $params['items'] as $item ) : ?>

									<div class="info-col col-12 col-sm-6 col-lg-4">
										<div class="info-container text-container">
											<?= ( $item['heading'] ) ? '<h3 class="heading">'.$item['heading'].'</h3>' : ''; ?>
											<?= ( $item['text'] ) ? '<p class="text">'.$item['text'].'</p>' : ''; ?>
										</div>
									</div>

								<?php endforeach; ?>

							</div>
						</div>

					</div>
				</div>
			<?php endif; ?>

		</div>
	</div>

	<?= ( $params['show_bg'] ) ? '</div>': ''; ?>

</section>

<?php }




/*==============================================================================
  # output_block_partners()
==============================================================================*/

function output_block_partners( $args = array() ){

	$defaults = array(
		'id'  				=> '',					//ID for direct links
		'marg_bot'  		=> '200',				//Margin bottom
		'heading' 			=> '',					//Heading
		'text' 				=> '',					//Text
		'items' 			=> array()				//Array with partner objects
	);

	$params = wp_parse_args( $args, $defaults );
	$num_of_items = count( $params['items'] );
	?>

	<?php if( $params['items'] ) : ?>
		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__partners marg-bot-<?= $params['marg_bot'] ?>">
		
		<div class="container-fluid">
			<div class="row justify-content-center">

				<?php if( $params['heading'] || $params['text'] ) : ?>
					<div class="col-12 col-md-10 col-xl-8">
						<div class="text-container">
							<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>' : ''; ?>
							<?= ( $params['text'] ) ? '<div class="text">'.$params['text'].'</div>': ''; ?>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</div>

		<div class="container-fluid">
			<div class="row justify-content-center">

				<div class="col-12 col-md-10 col-xl-8">
					<div class="row justify-content-center">

						<?php foreach( $params['items'] as $item ) : ?>

							<?php 
							$image = get_field( 'image', $item->ID );
							$link = get_field( 'link', $item->ID );
							?>

							<div class="col-6 col-sm-4 col-md-3 col-lg-2">

								<div class="partner-image-container">

									<?= ( $link  ) ? '<a href="'.$link.'" target="_blank">' : ''; ?>

									<picture>
										<source media="(min-width: 0px)" data-srcset="<?= $image['sizes']['partner'] ?>, <?= $image['sizes']['partner_retina'] ?> 2x">
											<img class="partner-image lazyload" data-src="<?= $image['sizes']['partner'] ?>" alt="<?= $image['alt'] ?>">
										</picture>

										<?= ( $link  ) ? '</a>' : ''; ?>
										
									</div>
								</div>

							<?php endforeach; ?>

						</div>
					</div>

				</div>
			</div>

		</section>
	<?php endif; ?>

<?php }


/*==============================================================================
  # output_block_contactform()
==============================================================================*/

function output_block_contactform( $args = array() ){

	$defaults = array(
		'id'  				=> '',			//ID for direct links
		'marg_bot'  		=> '200',		//Margin bottom
		'heading' 			=> '',			//Heading
		'text' 				=> '',			//Text
		'shortcode' 		=> ''			//Contact form 7 shortcode
	);

	$params = wp_parse_args( $args, $defaults );
	?>

	<?php if( $params['shortcode'] ) : ?>
		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__partners marg-bot-<?= $params['marg_bot'] ?>">
		
		<div class="container-fluid">
			<div class="row justify-content-center">

				<?php if( $params['heading'] || $params['text'] ) : ?>
					<div class="col-12 col-md-10 col-xl-8">
						<div class="text-container">
							<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>' : ''; ?>
							<?= ( $params['text'] ) ? '<div class="text">'.$params['text'].'</div>': ''; ?>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</div>

		<div class="container-fluid">
			<div class="row justify-content-center">

				<div class="col-12 col-md-10 col-xl-8">

					<?= do_shortcode( $params['shortcode'] ) ?>

				</div>

			</div>
		</div>

	</section>
<?php endif; ?>

<?php }


/*==============================================================================
  # output_block_coworkers()
==============================================================================*/

function output_block_coworkers( $args = array() ){

	$defaults = array(
		'id'  				=> '',			//ID for direct links
		'marg_bot'  		=> '200',		//Margin bottom
		'heading' 			=> '',			//Heading
		'text' 				=> '',			//Text
		'content_placement' => 'left',		//Left, center or right
		'coworker_areas' 	=> array(		//Array with information about coworker areas
			array(
				'area'			=> '',		//Heading for area
				'coworkers'		=> array(),	//Array with coworker objects
			),
		),
	);

	$params = wp_parse_args( $args, $defaults );

	$params['content_placement'] = ( $params['content_placement'] === 'left' ) ? 'start' : $params['content_placement'];
	$params['content_placement'] = ( $params['content_placement'] === 'right' ) ? 'end' : $params['content_placement'];
	?>
	
	<?php if ( isset($params['coworker_areas'][0]['coworkers']) && $params['coworker_areas'][0]['coworkers'] ) : ?>

		<section<?= ( $params['id'] ) ? ' id="'.$params['id'].'"': '' ; ?> class="section section__coworkers marg-bot-<?= $params['marg_bot'] ?>">

		<div class="container-fluid">
			<div class="row justify-content-center">

				<?php if( $params['heading'] || $params['text'] ) : ?>
					<div class="col-12 col-md-10 col-xl-8">
						<div class="text-container main-text-container">
							<?= ( $params['heading'] ) ? '<h2 class="heading">'.$params['heading'].'</h2>' : ''; ?>
							<?= ( $params['text'] ) ? '<div class="text">'.$params['text'].'</div>': ''; ?>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</div>

		<div class="container-fluid">
			<div class="row justify-content-center">

				<?php foreach ( $params['coworker_areas'] as $coworker_areas ) : ?>
					<div class="col-12 col-md-10">

						<div class="text-container">
							<?= ( $coworker_areas['area'] ) ? '<h3 class="heading area-heading font-size-2">'.$coworker_areas['area'].'</h3>' : ''; ?>
						</div>

						<div class="row justify-content-<?= $params['content_placement'] ?>">
							<?php if ( $coworker_areas['coworkers'] ) : ?>
								<?php foreach ( $coworker_areas['coworkers'] as $coworker ) : ?>

									<div class="col-12 col-sm-6 col-md-4 col-xl-4">
										<?php output_coworker_card( $coworker ); ?>
									</div>

								<?php endforeach; ?>
							<?php endif; ?>
						</div>

					</div>
				<?php endforeach; ?>

			</div>
		</div>

	</section>

<?php endif; ?>

<?php }

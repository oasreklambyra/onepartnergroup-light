<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primär meny', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  ///add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */

function assets() {
/**CSS**/
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, '1.5');

/**JAVASCRIPT**/
  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], '1.5', true);
  wp_enqueue_script('ytplayer', get_template_directory_uri().'/assets/scripts/jquery.mb.YTPlayer.min.js', ['jquery'], null, true);
  wp_enqueue_script('owlcarousel', get_template_directory_uri().'/assets/scripts/owl.carousel.min.js', ['jquery'], null, true);

/**AJAX**/
  wp_localize_script( 'sage/js', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


/*==============================================================================
  # Add new term
==============================================================================*/

if( !function_exists( 'add_new_term' ) ){
  function add_new_term( $term, $tax, $slug, $parent_id = '' ){
    if( !term_exists( $term, $tax ) ) :
      $args = array(
        'slug' => $slug,
      );
      if( $parent_id ){
        $args['parent'] = $parent_id;
      }
      $wp_term = wp_insert_term(
        $term,
        $tax,
        $args
      );
      return $wp_term['term_id'];
    endif;

    return null;
  }
}


/*==============================================================================
  # Register custom post_types and taxonomies
==============================================================================*/

/**
 * Register Medarbetare
 */
$labels = array(
  'name'                => 'Medarbetare', 
  'menu_name'           => 'Medarbetare',
  'singular_name'       => 'Medarbetare',
  'all_items'           => 'Alla medarbetare',
  'edit_item'           => 'Ändra medarbetare',
  'update_item'         => 'Uppdatera medarbetare',
  'add_new_item'        => 'Skapa ny medarbetare',
  'new_item'            => 'Skapa ny medarbetare',
  'view_item'           => 'Se medarbetare',
);
$args = array(
  'labels'              => $labels,
  'public'              => true,
  'show_in_nav_menus'   => true,
  'has_archive'         => false,
  'show_in_rest'        => true,
  'menu_icon'           => 'dashicons-groups',
  'rewrite'             => array( 'slug' => 'personal', 'with_front' => false ),
  'supports'            => array('title','page-attributes')
  );
register_post_type( 'coworker', $args );


// /**
//  * Register Nyheter
//  */
// $labels = array(
//   'name'                => 'Nyheter',
//   'menu_name'           => 'Nyheter',
//   'singular_name'       => 'Nyhet',
//   'search_items'        => 'Sök nyhet',
//   'all_items'           => 'Alla nyheter',
//   'edit_item'           => 'Ändra nyhet',
//   'update_item'         => 'Uppdatera nyhet',
//   'add_new_item'        => 'Skapa ny nyhet',
//   'new_item'            => 'Skapa ny nyhet',
//   'view_item'           => 'Visa nyhet',
// );
// $args = array(
//   'labels'              => $labels,
//   'public'              => true,
//   'show_in_nav_menus'   => true,
//   'has_archive'         => true,
//   'show_in_rest'        => false,
//   'menu_icon'           => 'dashicons-flag',
//   'rewrite'             => array( 'slug' => 'nyheter', 'with_front' => false ),
//   'supports'            => array( 'title', 'editor', 'page-attributes' )
//   );
// register_post_type( 'news', $args );

// /**
//  * Register Taxonomy news_category
//  */
// $labels = array(
//   'name'                => 'Kategorier',
//   'menu_name'           => 'Kategorier',
//   'singular_name'       => 'Kategori',
//   'search_items'        => 'Sök kategorier',
//   'all_items'           => 'Alla kategorier',
//   'edit_item'           => 'Ändra kategorier',
//   'update_item'         => 'Uppdatera kategori',
//   'add_new_item'        => 'Skapa ny kategori',
//   'new_item'            => 'Skapa ny kategori',
//   'view_item'           => 'Visa kategori',
// );
// $args = array(
//   'public'              => true,
//   'publicly_queryable'  => true,
//   'show_ui'             => true,
//   'show_in_menu'        => true,
//   'show_in_nav_menus'   => false,
//   'show_admin_column'   => true,
//   'hierarchical'        => true,
//   'query_var'           => true,
//   'has_archive'         => true,
//   'show_in_rest'        => false,
//   'rewrite'             => array( 'slug' => 'nyhetskategori', 'with_front' => false ),
//   'labels'              => $labels,
// );
// register_taxonomy( 'news_category', array( 'news' ), $args );

/**
 * Register Dokumentmallar
 */
$labels = array(
  'name'                => 'Dokumentmallar',
  'menu_name'           => 'Dokumentmallar',
  'singular_name'       => 'Dokumentmall',
  'search_items'        => 'Sök dokumentmall',
  'all_items'           => 'Alla dokumentmallar',
  'edit_item'           => 'Ändra dokumentmall',
  'update_item'         => 'Uppdatera dokumentmall',
  'add_new_item'        => 'Skapa ny dokumentmall',
  'new_item'            => 'Skapa ny dokumentmall',
  'view_item'           => 'Visa dokumentmall',
);
$args = array(
  'labels'              => $labels,
  'public'              => true,
  'show_in_nav_menus'   => true,
  'has_archive'         => true,
  'show_in_rest'        => false,
  'menu_icon'           => 'dashicons-welcome-write-blog',
  'rewrite'             => array( 'slug' => 'dokumentmallar', 'with_front' => false ),
  'supports'            => array( 'title', 'editor', 'page-attributes' )
  );
register_post_type( 'document', $args );

/**
 * Register Taxonomy document_category
 */
$labels = array(
  'name'                => 'Dokumentkategorier',
  'menu_name'           => 'Dokumentkategorier',
  'singular_name'       => 'Dokumentkategori',
  'search_items'        => 'Sök dokumentkategorier',
  'all_items'           => 'Alla dokumentkategorier',
  'edit_item'           => 'Ändra dokumentkategorier',
  'update_item'         => 'Uppdatera dokumentkategori',
  'add_new_item'        => 'Skapa ny dokumentkategori',
  'new_item'            => 'Skapa ny dokumentkategori',
  'view_item'           => 'Visa dokumentkategori',
);
$args = array(
  'public'              => false,
  'publicly_queryable'  => false,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'show_in_nav_menus'   => false,
  'show_admin_column'   => true,
  'hierarchical'        => true,
  'query_var'           => true,
  'has_archive'         => false,
  'show_in_rest'        => false,
  'labels'              => $labels,
);
register_taxonomy( 'document_category', array( 'document' ), $args );

/**
 * Register Partners/Certifikat
 */
$labels = array(
  'name'                => 'Partners/Certifikat',
  'menu_name'           => 'Partners/Certifikat',
  'singular_name'       => 'Partner/Certifikat',
  'search_items'        => 'Sök partner',
  'all_items'           => 'Alla partners',
  'edit_item'           => 'Ändra partner',
  'update_item'         => 'Uppdatera partner',
  'add_new_item'        => 'Skapa ny partner',
  'new_item'            => 'Skapa ny partner',
  'view_item'           => 'Visa partner',
);
$args = array(
  'labels'              => $labels,
  'public'              => true,
  'show_in_nav_menus'   => true,
  'has_archive'         => false,
  'show_in_rest'        => false,
  'publicly_queryable'  => false,
  'menu_icon'           => 'dashicons-id',
  'supports'            => array( 'title', 'page-attributes' )
  );
register_post_type( 'partner', $args );
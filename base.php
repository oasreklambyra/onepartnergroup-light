<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>

<?php get_template_part('templates/head'); ?>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MKWZFTL"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php
        do_action('get_header');
        get_template_part('templates/content/content','header');
    ?>

    <div class="wrap" role="document">
        
        <div class="main">
            <?php include Wrapper\template_path(); ?>
        </div>

        <?php
            do_action('get_footer');
            get_template_part('templates/content/content','footer');

            $google_maps_key = get_field( 'google_maps_key', 'options' ); 
            if( $google_maps_key ) :
                echo '<script src="https://maps.googleapis.com/maps/api/js?key='.$google_maps_key.'"></script>';
            endif;

            wp_footer();
        ?>

    </div><!-- /.wrap -->

    <?php get_template_part('templates/includes/inc','cookie-disclaimer'); ?>

    <div class="change-page-load">
        <img class="lazyload" data-src="<?= get_template_directory_uri().'/dist/images/loading.svg' ?>" alt="Laddar...">
    </div>
    
    <!-- Conditional Lazyload -->
    <script type="text/javascript">
    (function(w, d){
    var b = d.getElementsByTagName('body')[0];
    var s = d.createElement("script"); s.async = true;
    var v = !("IntersectionObserver" in w) ? "8.6.0" : "10.4.1";
    s.src = "<?= get_template_directory_uri(); ?>/assets/scripts/lazyload."+v+".min.js";
    w.lazyLoadOptions = {
        threshold: 500,
        elements_selector: ".lazyload"
    }; // Your options here. See "recipes" for more information about async.
    b.appendChild(s);
    }(window, document));
    </script>
    <!-- End Conditional Lazyload -->

</body>
</html>
(function($) {
    //Use custom marker
    var marker_url = '/wp-content/themes/onepartnergroup/dist/images/marker-blue.png';

    //Use custom style
    var style= [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
];


/*==============================================================================
  ##1.    Google maps      
==============================================================================*/

    /**
    * Add markers
    */
    function add_marker( $marker, map ) {

        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

        var iconObj = {
            url         : marker_url, //<--marker_url = variable
            size        : new google.maps.Size(45, 45),
            scaledSize  : new google.maps.Size(45, 45),
        };
        var marker = new google.maps.Marker({
            icon        : iconObj,
            position    : latlng,
            map         : map,
            url         : $marker.attr('data-url')
        });

        map.markers.push( marker );
        
        if ( marker.url ) {
            google.maps.event.addListener(marker, 'click', function() {
                window.location.href = this.url;
            });
        }
    }

    /**
    * Center map
    */
    function center_map( map ) {
        var bounds = new google.maps.LatLngBounds();
    
        $.each( map.markers, function( i, marker ){
            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
            bounds.extend( latlng );
        });

        if( map.markers.length === 1 )
        {
            map.setCenter( bounds.getCenter() );
            map.setZoom( 12 );
      
        }else{
            map.fitBounds( bounds );
        }
    }

    /**
    * Create map
    */
    function new_map( $el ) {
        var $markers = $el.find('.marker');
        var args = {
            zoom                : 12,
            disableDefaultUI    : true,
            draggable           : true,
            fullscreenControl   : true,
            zoomControl         : true,
            scrollwheel         : false,
            mapTypeControl      : false,
            panControl          : false,
            signInControl       : false,
            streetViewControl   : false,
            styles              : style,
            center              : new google.maps.LatLng(0, 0),
            mapTypeId           : google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map( $el[0], args);
        map.markers = [];

        $markers.each(function(){ 
            add_marker( $(this), map );
        });

        center_map( map );

        return map;
    }

    /**
    * Create map
    */
    var map = null;
    $('.acf-map').each(function(){
        map = new_map( $(this) );
    });

})(jQuery);
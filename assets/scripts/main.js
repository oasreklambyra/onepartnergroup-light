/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

/*==============================================================================

  # YoutubePlayer
  # Smashballoon Facebook feed changes
  # Accordion open-class
  # OwlCarousel
  # Local scroll 
  # Header menu functions
  # Single vacant-job function
  # Cookie-text
  # Intro image sizer
  # Loader for new page
  # Scroll and resize listener
  # Sort blog posts
  # Handle loader
  # Load more vacant-jobs/courses
  # Filter vacant-jobs/courses
  # Vacant jobs radio btns
  # Posts filter category buttons
  # Selectize
  # Selectize for wpcf7
  # Disabled MC-signup if GDPR

==============================================================================*/

/*==============================================================================
  # YoutubePlayer
==============================================================================*/

YTPlayer = $('.video');
YTPlayer.YTPlayer();


$('.video-container').on('click','.video-overlay',function(e){
  var currentVideo = $( e.target).closest('.video');
  currentVideo.toggleClass('play');

  if( currentVideo.hasClass('play') ){
    currentVideo.YTPPlay();

  }else{
    currentVideo.YTPPause();

  }

});


/*==============================================================================
  # Smashballoon Facebook feed changes
==============================================================================*/

$('.cff-item').each(function(){
  var $self = $(this),
  $textWrapper = $self.find('.cff-post-text'),
  $sharedLink = $self.find('.cff-shared-link');
  $textWrapper.before( $self.find('.cff-shared-link') );
  if( $textWrapper.length < 1 ) { 
    $sharedLink.remove();
  }
});



/*==============================================================================
  # Accordion open-class
==============================================================================*/

  // The accordion-block uses the basic bootstrap-4 accordion
  // This function adds a class to the top-level element .card for design purposes

$('.card-heading').on('click','.no-scroll',function(){
  var accordion = $( this ).closest('.accordion'),
      parentOpen = $( this ).closest('.card').hasClass('show');

  accordion.find('.card').removeClass('show');
  if( !parentOpen ){
    $( this ).closest('.card').addClass('show');
  }
});

/*==============================================================================
  # OwlCarousel
==============================================================================*/

$('.owl-carousel').owlCarousel({
  loop: true,
  center: true,
  items: 1,
  margin: 60,
  URLhashListener: true,
  startPosition: 'URLHash',
  autoHeight: true,
  lazyLoad: true,
});


/*==============================================================================
  # Local scroll 
==============================================================================*/

/**
 * scrollTo()
 * 
 * ScrollTo scrolls to a given element in a given time
 * ScrollTo handlers calls the function onload if a hash is present and 
 * when a linked is clicked if the hash exists on the same page
 * 
 * @param $ele Element too scroll to 
 * @param $time Scroll time in ms
 * 
 * @return scrolls to element
 *
 * @version  1.1
 */

function scrollTo( $ele, $time ){
  if ( $ele.length > 0) {
    var $offset = $ele.offset().top,
        $extra_offset = 120;
        $offset = $offset - $extra_offset;
        
    $("html, body").animate({
      scrollTop: $offset+'px' 
    }, $time );
  }
}
//ScrollTo handlers
var $hash = window.location.hash;
//If a link contains a hash and it exists on the same page, scroll to it
$( 'a' ).on( 'click', function( e ){
  var $target = $( this );
  //Do nothing if the link is a part of a no-scroll element
  if( !($target.parents( '.no-scroll' ).length) && !($target.hasClass('no-scroll')) ){
    //Get the a-tag if target is a child to the a-tag (This seams to be necessary if the link contains an svg-obj)
    if( !( $target.is( 'a' )) ){
      $target = $target.closest( 'a' );
    }
    //Check for hash, prevent default if found  
    var $href = $target.attr( 'href' ),
      $hrefIndex = $href.indexOf('#');
    //If link contains hash
    if( $hrefIndex >= 0 ){
      var $hrefLength = $href.length,
        $hash = $href.substring( $hrefIndex, $hrefLength );
      //If hash exists on this page
      if( $( $hash ).length ){
        e.preventDefault();
        scrollTo( $( $hash ), 500 );
      }
    }
  }
});

//Scrolls to hash on page load if it exists
if( $hash.length > 0 ){
  scrollTo( $( $hash ), 1500 );
}

//<!--ScrollTo handlers End-->


/*==============================================================================
  # Header menu functions
==============================================================================*/

var $menuBtn = $( '.menu-btn' ),
    $searchBtn = $( '#search-btn' ),
    $prevScrollValue = $(window).scrollTop(),
    $banner = $( '#banner');

$menuBtn.on('click',function(e){
  $menuBtn.toggleClass('open');
  $banner.addClass('show-banner');
  $('body').toggleClass('show-menu');
});

$searchBtn.on('click',function(e){

  var $headerNav = $searchBtn.closest('.header-nav');

  if( $headerNav.hasClass('show-search') ){
    $headerNav.find('.search-box').submit();

  } else {
    $headerNav.addClass('show-search');
    $searchBtn.closest('.header-nav').find('input[name="s"]').focus();
    
  }

});

$( '.my-pages-toggle' ).on('click',function(e){
  e.preventDefault();
  $( '.my-pages__dropdown' ).toggleClass('open');
});

function handleHeaderScroll(){
  var $scrollValue = $(window).scrollTop();

  if( $scrollValue === 0 ){
    $banner.removeClass( 'fixed' );
  }else{
    $banner.addClass( 'fixed' );    
  }

  //headerscroll
  if( $scrollValue !== $prevScrollValue ){
    if( ($scrollValue<$prevScrollValue) ){
      $banner.addClass( 'show-banner' );
    }else{
      $banner.removeClass( 'show-banner' );
      $banner.find( '.header-nav' ).removeClass( 'show-search' );
    }
  }

  $prevScrollValue = $scrollValue;
}


/*==============================================================================
  # Single vacant-job function
==============================================================================*/

$( '.apply-popup' ).on('click','.apply-btn',function(e){
  e.preventDefault();
  
  $( this ).siblings('.apply-popup__dropdown').toggleClass( 'open' );

});

$( '.close-application' ).on('click',function(e){
  $('.apply-popup__dropdown.open').removeClass( 'open' );
});

$( '.post-content strong' ).each(function(e){
  if ( $(this).text().length <= 1  ){

    $(this).next( 'br' ).remove();
    $(this).remove();

  }
});

/*==============================================================================
  # Cookie-text
==============================================================================*/

$( '#cookie-btn' ).on('click',function(){
  $( '.cookie-container' ).addClass( 'hide' );

  $.ajax({
      type: 'POST',
      dataType : 'json',
      url: my_ajax_object.ajax_url,
      data: {action: 'set_seen_cookie_box' }
  });
});


/*==============================================================================
  # Intro image sizer
==============================================================================*/

var introImg = $( '#intro-image' ),
    mainIntroImgEle = $( '#intro-image .placeholder' ),
    containerProportions,
    natrualProportions;

  

function introImageSizer(){
  if ( introImg.length > 0 ) {

    containerProportions = introImg.width() / introImg.height();
  
    if ( natrualProportions ) {

      if ( natrualProportions > containerProportions ) {
        introImg.addClass( 'tall-box' );
        introImg.removeClass( 'wide-box' );

      } else {
        introImg.addClass( 'wide-box' );
        introImg.removeClass( 'tall-box' );

      }

    }
  }
}

var image = new Image(),
    bg;

if ( mainIntroImgEle.length > 0 ) {
  bg = mainIntroImgEle.css('background-image');
  image.src = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
}

image.onload = function() {
  natrualProportions = this.width / this.height;
  introImageSizer();
}; 

introImageSizer();


/*==============================================================================
  # Loader for new page
==============================================================================*/

function loadNewPage() {
  $( '.change-page-load' ).addClass( 'load-new' );
}


/*==============================================================================
  # Sort blog posts
==============================================================================*/

var sortBlogBtn = $('.blog-category-toggle');

sortBlogBtn.on('click',function(e){
  e.preventDefault();

  var container = $(this).attr('href'),
      value = $(this).attr('data-value');

  $('.blog-category-toggle.current').removeClass('current');
  $(this).addClass('current');

  $.ajax({
      type: 'POST',
      dataType : 'json',
      url: my_ajax_object.ajax_url,
      data: {action: 'ajax_sort_blog_posts', term_id: value },
      success: function( data ){
        if( data ){

          $( container ).empty();
          $( container ).append( data.output );

          //myLazyLoad.update(); No need for these anymore

        }
      }
  });
});


/*==============================================================================
  # Handle loader
==============================================================================*/

var lmContainer = $( '#lm-container' );

function handleLoader( action ){
  
  if( action === 'loading' ) {
    lmContainer.addClass('loading');
    lmContainer.removeClass('hide');

  } else if ( action === 'loaded' ) {
    lmContainer.removeClass('loading');
    lmContainer.removeClass('hide');
 
  } else if ( action === 'hide' ){
    lmContainer.addClass('hide');
    lmContainer.removeClass('loading');

  }
}


/*==============================================================================
  # Load more vacant-jobs/courses
==============================================================================*/


var itemFeed = $( '#item-feed' ),
    lmBtn = $( '#lm-btn' ),
    organicPage = 2,
    postsOffset = 10,
    lmOffset = 18;


function getFilterDataJobs(){

  var serviceTypes = [];
  $('#selectize-container-category').find('.item').each(function(){
    serviceTypes.push( $(this).attr('data-value') );
  });

  var cities = [];
  $('#selectize-container-city').find('.item').each(function(){
    cities.push( $(this).attr('data-value') );
  });

  var data = {
    searchQuery   : $('input[name="job-s"]').attr('value'),
    serviceTypes  : serviceTypes,
    cities        : cities,
    categories    : $('input[name="radio-categories"]').attr('value').split(',')
  };

  return data;
}


function getFilterDataCourses(){

  var categories = [];
  $('#selectize-container-category').find('.item').each(function(){
    categories.push( $(this).attr('data-value') );
  });

  var data = {
    searchQuery   : $('input[name="course-s"]').attr('value'),
    categories    : categories
  };

  return data;
}


function getFilterDataDocuments(){

  var categories = [];
  $('#selectize-container-document').find('.item').each(function(){
    categories.push( $(this).attr('data-value') );
  });

  var data = {
    searchQuery   : $('input[name="document-s"]').attr('value'),
    categories    : categories
  };

  return data;
}


function getFilterDataPosts(){

  var data = {
    searchQuery   : $('input[name="posts-s"]').attr('value'),
    category      : $('input[name="posts-category"]').attr('value'),
    exclude       : lmBtn.attr('data-exclude'),
    postsOffset   : postsOffset
  };

  return data;
}

function getFilterDataNews(){

  var categories = [];
  $('#selectize-container-news-category').find('.item').each(function(){
    categories.push( $(this).attr('data-value') );
  });

  var cities = [];
  $('#selectize-container-news-city').find('.item').each(function(){
    cities.push( $(this).attr('data-value') );
  });

  var data = {
    searchQuery   : $('input[name="posts-s"]').attr('value'),
    categories    : categories,
    cities        : cities,
    exclude       : lmBtn.attr('data-exclude'),
    postsOffset   : postsOffset
  };

  return data;
}


lmBtn.on('click',function(e){
  e.preventDefault();

  handleLoader( 'loading' );
  
  var type = $( this ).attr( 'data-type' ),
      action = '',
      data = [];

  if ( type === 'job' ) {

    action = 'ajax_load_jobs';
    data = getFilterDataJobs();

  } else if ( type === 'course' ) {

    action = 'ajax_load_courses';
    data = getFilterDataCourses();

  } else if ( type === 'document' ) {

    action = 'ajax_load_documents';
    data = getFilterDataDocuments();


  } else if ( type === 'posts' ) {

    action = 'ajax_load_posts';
    data = getFilterDataPosts();

  } else if ( type === 'news' ) {

    action = 'ajax_load_news';
    data = getFilterDataNews();

  } else if ( type === 'organic' ) {

    action = 'ajax_load_organic';
    data = {
      searchQuery : $( this ).attr( 'data-search' ),
      organicPage : organicPage
    };

  }

  data.lmOffset = lmOffset;

  $.ajax({
      type: 'POST',
      dataType : 'json',
      url: my_ajax_object.ajax_url,
      data: {action: action, data: data },
      success: function(data){
        if( data ){

          if ( type === 'organic' ) {

            $( '#organic-search-content' ).append( data.output );

          } else {

            itemFeed.append( data.output );
            
          }

          //myLazyLoad.update(); No need for these anymore

          if( data.has_more ){
            handleLoader( 'loaded' );
            lmOffset += 18;
            postsOffset += 10;
            organicPage++;

          } else {
            handleLoader( 'hide' );

          }
        }
      }
  });
});

/*==============================================================================
  # Filter vacant-jobs/courses
==============================================================================*/

function filterSearchItems(){

  handleLoader( 'loading' );
  itemFeed.html('');

  var type = lmBtn.attr( 'data-type' ),
      action = '',
      data = [];

  if ( type === 'job' ) {

    action = 'ajax_filter_jobs';
    data = getFilterDataJobs();

  } else if ( type === 'course' ) {

    action = 'ajax_load_courses';
    data = getFilterDataCourses();

  } else if ( type === 'document' ) {

    action = 'ajax_load_documents';
    data = getFilterDataDocuments();

  } else if ( type === 'posts' ) {

    action = 'ajax_load_posts';
    data = getFilterDataPosts();
    data.postsOffset = 0;

  } else if ( type === 'news' ) {

    action = 'ajax_load_news';
    data = getFilterDataNews();
    data.postsOffset = 0;

  }

  $.ajax({
      type: 'POST',
      dataType : 'json',
      url: my_ajax_object.ajax_url,
      data: {action: action, data: data },
      success: function(data){

        if( data ){

          itemFeed.html( data.output );
          $( '#total-results' ).text( data.total_results );

          //myLazyLoad.update(); No need for these anymore

          if( data.has_more ){
            handleLoader( 'loaded' );
            lmOffset = 18;
            postsOffset = 10;

          } else {
            handleLoader( 'hide' );

          }
        }   
      }
  });
}

$('.search-form').on('submit',function(e){
  e.preventDefault();
  filterSearchItems();
});

$( '#settings-btn' ).on('click',function(){
  $( this ).toggleClass( 'open' );
  $( '#settings-container' ).slideToggle( '250' );
});


/*==============================================================================
  # Vacant jobs radio btns
==============================================================================*/

$( '.radio-container' ).on('click','.radio-value',function(e){
  var input = $( '#radio-cat-values' ),
      inputVal = input.attr('value').split(',');

  $( this ).toggleClass( 'choosen' );

  if( $( this ).hasClass('choosen') ){
    inputVal.push( $( this ).attr('data-input') );        

  }else{
    var index = inputVal.indexOf( $( this ).attr('data-input') );      
    if (index > -1) {
        inputVal.splice(index, 1);
    }

  }

  input.attr('value', inputVal.join(',') );

  filterSearchItems();
});


/*==============================================================================
  # Posts filter category buttons
==============================================================================*/

$( '.filter-posts-categories' ).on('click','.filter-item',function(e){

  $( '.filter-item.selected' ).removeClass( 'selected' );
  $( this ).addClass( 'selected' );

  $( 'input[name="posts-category"]' ).attr( 'value', $( this ).attr('data-value') );

  filterSearchItems();
});


/*==============================================================================
  # Selectize
==============================================================================*/

//Sorry for this part, all the selectize stuff got kinda messy in the end

function updatePreferenceCookie( action, value ){
  var data = {
    action: action,
    value: value,
  };

  $.ajax({
      type: 'POST',
      dataType : 'json',
      url: my_ajax_object.ajax_url,
      data: {action: 'update_preference_cookie', data: data },
      success: function(data){

        if( data.location ){
          
          document.location=data.location;

        }

      }
  });
}

$('#select-category').selectize({
    create: false,
    onChange: function(value) {

      loadNewPage();
      updatePreferenceCookie( 'preference', value );

    }
});

$('#select-office').selectize({
    create: false,
    onChange: function(value) {

      loadNewPage();
      updatePreferenceCookie( 'office', value );

    }
});

$('#select-nav-office').selectize({
    create: false,
    onChange: function(value) {

      loadNewPage();
      updatePreferenceCookie( 'office', value );

    }
});

$('.selectize--contact-office').selectize({
    create: false,
    onChange: function(value) {

      loadNewPage();
      updatePreferenceCookie( 'office', value );

    }
});

$('.selectize--contactpage-office').selectize({
    create: false,
    onChange: function(value) {

      loadNewPage();
      updatePreferenceCookie( 'office-contact', value );

    }
});


/*
 * Add tags for serach-filter selectize
 */
var filterValues = $( '#filter-values' );

function selectizeFilter( values, category ){
  var itemsToKeep,
      showVal,
      name,
      ele,
      i;

  if( category === 'city' ){

    var filterEleCity = $( '#filter-values .filter-item.city:not(.hide)' ),
        filterValuesCity = [];
  
    //Get the value from current elements
    if( filterEleCity.length !== -1 ){
      for( i = 0; i < filterEleCity.length; i++ ){
        filterValuesCity.push( $( filterEleCity[i] ).attr('data-val') );
      }
    }

    //Check if there is a new element to add
    if( values && values.length !== -1 ){
      for( i = 0; i < values.length; i++ ){
        if( filterValuesCity.indexOf( values[i] ) === -1 ){
          
          name = $( '#selectize-container-'+category+' [data-value='+values[i]+']' ).text();

          if ( lmBtn.attr( 'data-type' ) === "news" ) {
            name = $( '#selectize-container-news-'+category+' [data-value='+values[i]+']' ).text();

          }

          ele = '<div class="filter-item '+category+' hide" data-cat="'+category+'" data-val="'+values[i]+'">'+name+'</div>';
          filterValues.prepend( ele );
          showVal = values[i];        
        }
      }
    }


  } else if ( category === 'category' ) {

    var filterEleCat = $( '#filter-values .filter-item.category:not(.hide)' ),
        filterValuesCat = [];
  
    //Get the value from current elements
    if( filterEleCat.length !== -1 ){
      for( i = 0; i < filterEleCat.length; i++ ){
        filterValuesCat.push( $( filterEleCat[i] ).attr('data-val') );
      }
    }

    //Check if there is a new element to add
    if( values && values.length !== -1 ){
      for( i = 0; i < values.length; i++ ){
        if( filterValuesCat.indexOf( values[i] ) === -1 ){
          
          name = $( '#selectize-container-'+category+' [data-value='+values[i]+']' ).text();

          if ( lmBtn.attr( 'data-type' ) === "news" ) {
            name = $( '#selectize-container-news-'+category+' [data-value='+values[i]+']' ).text();

          }

          ele = '<div class="filter-item '+category+' hide" data-cat="'+category+'" data-val="'+values[i]+'">'+name+'</div>';
          filterValues.append( ele );
          showVal = values[i];
        }
      }
    }

  } else if ( category === 'document' ) {

    var filterEleDoc = $( '#filter-values .filter-item.document:not(.hide)' ),
        filterValuesDoc = [];
  
    //Get the value from current elements
    if( filterEleDoc.length !== -1 ){
      for( i = 0; i < filterEleDoc.length; i++ ){
        filterValuesDoc.push( $( filterEleDoc[i] ).attr('data-val') );
      }
    }

    //Check if there is a new element to add
    if( values && values.length !== -1 ){
      for( i = 0; i < values.length; i++ ){
        if( filterValuesDoc.indexOf( values[i] ) === -1 ){
          
          name = $( '#selectize-container-'+category+' [data-value='+values[i]+']' ).text();
          ele = '<div class="filter-item '+category+' hide" data-cat="'+category+'" data-val="'+values[i]+'">'+name+'</div>';
          filterValues.append( ele );
          showVal = values[i];
        }
      }
    }

  }

  setTimeout(function(){
    $( '.filter-item.hide[data-val="'+showVal+'"]' ).removeClass('hide');
  }, 25 );
  filterSearchItems();
}


/*
 * Activate serach-filter selectize
 */
var selectCategoryPlaceholder = $('#selectize-container-category').attr( 'data-placeholder' );
var selectJobCategory = $('#select-job-category').selectize({
    create: false,
    maxItems: null,
    onChange: function(value){
      $( '#select-job-category-selectized' ).attr( 'placeholder', selectCategoryPlaceholder );
      selectizeFilter(value,'category');
    }
});

var selectCityPlaceholder = $('#selectize-container-city').attr( 'data-placeholder' );
var selectJobCity = $('#select-job-city').selectize({
    create: false,
    maxItems: null,
    onChange: function(value){
      $( '#select-job-city-selectized' ).attr( 'placeholder', selectCityPlaceholder );
      selectizeFilter(value,'city');
    }
});

var selectCoursePlaceholder = $('#selectize-container-category').attr( 'data-placeholder' );
var selectCourseCategory = $('#select-course-category').selectize({
    create: false,
    maxItems: null,
    onChange: function(value){
      $( '#select-course-category-selectized' ).attr( 'placeholder', selectCoursePlaceholder );
      selectizeFilter(value,'category');
    }
});

var selectDocumentPlaceholder = $('#selectize-container-document').attr( 'data-placeholder' );
var selectDocumentCategory = $('#select-document-category').selectize({
    create: false,
    maxItems: null,
    onChange: function(value){
      $( '#select-document-category-selectized' ).attr( 'placeholder', selectDocumentPlaceholder );
      selectizeFilter(value,'document');
    }
});

var selectNewsCategoryPlaceholder = $('#selectize-container-news-category').attr( 'data-placeholder' );
var selectNewsCategory = $('#select-news-category').selectize({
    create: false,
    maxItems: null,
    onChange: function(value){
      $( '#select-news-category-selectized' ).attr( 'placeholder', selectNewsCategoryPlaceholder );
      selectizeFilter(value,'category');
    }
});

var selectNewsCityPlaceholder = $('#selectize-container-news-city').attr( 'data-placeholder' );
var selectNewsCity = $('#select-news-city').selectize({
    create: false,
    maxItems: null,
    onChange: function(value){
      $( '#select-news-city-selectized' ).attr( 'placeholder', selectNewsCityPlaceholder );
      selectizeFilter(value,'city');
    }
});


/*
 * Set default value for serach-filter selectize
 */

filterValues.find( '.filter-item' ).each(function(){
  if( $( this ).hasClass( 'city' ) ){

    if ( selectJobCity.length > 0 ) {
      selectJobCity[0].selectize.addItem( $(this).attr('data-val'), true );
    }

    if ( selectNewsCity.length > 0 ) {
      selectNewsCity[0].selectize.addItem( $(this).attr('data-val'), true );
    }

  }else if( $( this ).hasClass( 'category' ) ){

    if ( selectJobCategory.length > 0 ) {
      selectJobCategory[0].selectize.addItem( $(this).attr('data-val'), true );
    }

    if ( selectNewsCategory.length > 0 ) {
      selectNewsCategory[0].selectize.addItem( $(this).attr('data-val'), true );
    }

  }
});

$( '#select-job-category-selectized' ).attr( 'placeholder', selectCategoryPlaceholder );
$( '#select-job-city-selectized' ).attr( 'placeholder', selectCityPlaceholder );

$( '#select-news-category-selectized' ).attr( 'placeholder', selectNewsCategoryPlaceholder );
$( '#select-news-city-selectized' ).attr( 'placeholder', selectNewsCityPlaceholder );


/*
 * Remove values from serach-filter selectize when clicking on tags
 */
filterValues.on('click','.filter-item',function(){
  var ele = $(this),
      type = lmBtn.attr( 'data-type' ),
      cat = ele.attr('data-cat'),
      val = ele.attr('data-val'),
      control;


  if( cat === 'document')  {
    control = selectDocumentCategory[0].selectize;

  } else if( cat === 'city')  {

    if ( type === 'job' ) {
      control = selectJobCity[0].selectize; 
      
    } else if ( type === 'news' ) {
      control = selectNewsCity[0].selectize; 

    }

  } else {

    if ( type === 'job' ) {
      control = selectJobCategory[0].selectize; 
    
    } else if ( type === 'news' ) {
      control = selectNewsCategory[0].selectize; 

    } else {
      control = selectCourseCategory[0].selectize; 
      
    }
  }

  ele.addClass('hide');
  control.removeItem( val );
  setTimeout(function() {
    ele.remove();
  }, 250);
});


/*==============================================================================
  # Selectize for wpcf7
==============================================================================*/

$('.selectize--form').selectize({
    create: false,
    onChange: function(value) {
      
      $( '.selectize-container input[name="choosen_office"]' ).attr('value', value );

    }
});

var wpcf7SelectControl = [];
var wpcf7Select = $( '.wpcf7-select' ).selectize({
    onChange: function(value) {

      //Used to close a opend dropdown in quick application modal
      wpcf7SelectControl = this;
    }
});



/*==============================================================================
  # Disabled MC-signup if GDPR
==============================================================================*/

function handleDisabled( form ){
  var gdpr = form.find('input[type="checkbox"]'),
      submit = form.find('input[type="submit"]');

  if ( gdpr.length > 0 ) {

    if ( gdpr.is(':checked') ) {

      submit.removeClass('disabled');

    } else {

      submit.addClass('disabled');

    }
  }
}

$('form[name="mc-embedded-subscribe-form"]' ).each(function(e){
  handleDisabled( $(this) );
});

$('form[name="mc-embedded-subscribe-form"]' ).on('click', 'input[type="checkbox"]', function(e){
  handleDisabled( $(this).closest('form[name="mc-embedded-subscribe-form"]') );
});

$( 'input[type="submit"]' ).on('click',function(e){
  if ( $( this ).hasClass('disabled') ) {
    e.preventDefault();
  }
});


/*==============================================================================
  # Scroll and resize listener
==============================================================================*/

$( window ).resize(function(){
  introImageSizer();
});

$( window ).scroll(function(){
  handleHeaderScroll();
});
$( document ).click(function(e){

  if( !$(e.target).closest('.header-nav').length && $('.header-nav').hasClass('show-search') ){
    $('.header-nav').removeClass('show-search');
  }

  if( !$(e.target).closest('.my-pages').length && $('.my-pages__dropdown').hasClass('open') ){
    $( '.my-pages__dropdown' ).removeClass('open');
  }

  if( !$(e.target).closest('.apply-popup').length && $( '.apply-popup__dropdown.open' ).length ){
    $( '.apply-popup__dropdown.open' ).removeClass('open');
  }

  if( $(e.target).closest('.fancy-modal').length > 0 && !$(e.target).closest('.selectize-container').length && wpcf7SelectControl.order ){
    wpcf7SelectControl.blur();
  }

});


/*==============================================================================
  <!-- End of custom javascript -->  
==============================================================================*/

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.

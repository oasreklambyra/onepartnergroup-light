<?php
	global $post;

	$intro_element_array = array(
		'/dist/images/header-element-1.svg',
		'/dist/images/header-element-2.svg',
		'/dist/images/header-element-3.svg',
		'/dist/images/header-element-4.svg',
	); 
	$rand_ele = rand(0,(count($intro_element_array)-1));
	$intro_element_url = get_template_directory().$intro_element_array[1];

	$title = get_the_title();
	$text = get_the_content();
	$icon = get_field( 'icon' );
	$file = get_field( 'document' );

	$terms = wp_get_post_terms( $post->ID , 'document_category' );
	$terms_name = array();

	foreach ( $terms as $term ) : 
		$terms_name[] = $term->name;
	endforeach;
?>
<section class="section section__intro section__intro__element single-post">
	<div class="background-element-container marg-bot-100">

		<div class="background-element"><?= file_get_contents($intro_element_url); ?></div>

		<div class="container-fluid">
			<div class="row justify-content-center">

				<div class="col-12 col-md-5 col-xl-4">

					<div class="text-container">

						<h1 class="heading"><?= $title ?></h1>
						<?= ( $terms_name ) ? '<h2 class="sub-title">'.implode(' - ', $terms_name ).'</h2>' : ''; ?>
						<?= ( $text ) ? '<p class="text post-content">'.$text.'</p>' : ''; ?>

					</div>

				</div>

				<div class="col-12 col-md-5 col-xl-4">

					<div class="download-container">

						<a class="download-link" href="<?= $file ?>" target="_blank">

							<?php if( $icon ) : ?>
								<picture>
									<source media="(min-width: 0px)" data-srcset="<?= $icon['sizes']['icon'] ?>, <?= $icon['sizes']['icon_retina'] ?> 2x">
									<img class="lazyload" data-src="<?= $icon['sizes']['icon'] ?>" alt="<?= $args['alt'] ?>">

								</picture>
							<?php endif; ?>

							<span><?= lang_text( 'Ladda ner', 'Download' ) ?></span>
						</a>

					</div>

				</div>

			</div>
		</div>

	</div>
</section>


<?php 
	$args = array(
		'heading' 		=> lang_text( 'Kontakta oss', 'Contact us' ),
		'row_number'	=> 'cta-1',
		'marg_bot' 		=> '150',
		'bg_type' 		=> 'color',
		'bg_color' 		=> 'white',
		'content_type' 	=> 'link',
		'position' 		=> 'text-first',
		'links'			=> array(
			array(
				'link_type' => 'office'
			)
		)
	);
	output_block_cta( $args );
?>
<?php 
	$args = array(
		'heading' 	=> lang_text( 'Här hittar du alla</br> våra dokumentmallar.', 'You can find all our</br> document templates here.' )
	);
	output_intro_element( $args );


	get_template_part('templates/content/content', 'document-archive');


	$args = array(
		'heading' 		=> lang_text( 'Kontakta oss', 'Contact us' ),
		'row_number'	=> 'cta-1',
		'marg_bot' 		=> '100',
		'bg_type' 		=> 'image',
		'bg_image_url'	=> get_template_directory_uri().'/dist/images/contact-element.svg',
		'content_type' 	=> 'link',
		'position' 		=> 'text-first',
		'links'			=> array(
			array(
				'link_type' => 'office'
			)
		)
	);
	output_block_cta( $args );
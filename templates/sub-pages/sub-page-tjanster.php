<?php 
	//Get the url so it is possible to check current location
  	$url = explode('/', $_SERVER[REQUEST_URI]);
  	$url = array_filter($url, create_function('$value', 'return $value !== "";')); 

	$current_office = get_posts( 
		array(
			'name'            => esc_attr($url[1]),
			'post_type'       => 'office',
			'post_status'     => 'publish',
			'posts_per_page'  => 1,
		) 
	);
	$current_office = $current_office[0];



/*==============================================================================
  # Intro element block
==============================================================================*/

	$intro_heading = ( $current_office && $temp = get_field( 'service_sub_intro_heading', $current_office->ID ) ) ? $temp : get_field( 'intro_heading' );
	$intro_text = ( $current_office && $temp = get_field( 'service_sub_intro_text', $current_office->ID ) ) ? $temp : get_field( 'intro_text' );

	$args = array(
		'heading' 	=> $intro_heading,
		'text' 		=> $intro_text
	);
	output_intro_element( $args );



/*==============================================================================
  # Entries block
==============================================================================*/

	$entries = array();

	$entries_heading = ( $current_office && $temp = get_field( 'service_sub_entries_heading', $current_office->ID ) ) ? $temp : get_field('area_entries_heading');
	$entries_text = ( $current_office && $temp = get_field( 'service_sub_entries_text', $current_office->ID ) ) ? $temp : get_field('area_entries_text');

	$entries_field = ( $current_office && have_rows( 'service_sub_entries', $current_office->ID ) ) ? 'service_sub_entries' : 'area_entries';
	$entries_id = ( $current_office && have_rows( 'service_sub_entries', $current_office->ID ) ) ? $current_office->ID : get_the_id();


	if ( have_rows( $entries_field, $entries_id ) ) :
		while( have_rows( $entries_field, $entries_id ) ) : the_row();

			$bg_image = get_sub_field('bg_image');

			$link_type = get_sub_field('link_type' );
			$permalink = get_sub_field($link_type);

			if ( $link_type === 'dynamic' ) : 

				$standard_slug = str_replace( get_home_url(), '', $permalink);
				$permalink = get_dynamic_link( $standard_slug );

			endif; 

			$entries[] = array(
				'type'		  => 'entry',
				'use_bg'	  => true,
				'permalink'   => $permalink,
				'title' 	  => get_sub_field('heading'),
				'text' 	  	  => get_sub_field('text'),
				'target'	  => get_sub_field('target'),
				'image_sizes' => array(
					array(
						'url' 	 => $bg_image['sizes']['entry'],
						'retina' => $bg_image['sizes']['entry_retina'],
					),
				),
			);

		endwhile;
	endif;

	$args = array(
		'id' 			=> 'tjanster',
		'row_number' 	=> 'tjanster',
		'heading'		=> $entries_heading,
		'text'			=> $entries_text,
		'links'			=> $entries
	);

	output_block_entires( $args );



/*==============================================================================
  # Large image with text block
==============================================================================*/


	$text_image_heading = ( $current_office && $temp = get_field( 'service_sub_text_image_heading', $current_office->ID ) ) ? $temp : get_field('area_large_image_heading');
	$text_image_text = ( $current_office && $temp = get_field( 'service_sub_text_image_text', $current_office->ID ) ) ? $temp : get_field('area_large_image_text');

	$args = array(
		'id'  				=> 'varfor-opg',
		'marg_bot'  		=> '200',
		'media_placement' 	=> get_field('area_large_image_placement'),
		'image' 			=> get_field('area_large_image'),
		'heading' 			=> $text_image_heading,
		'text' 				=> $text_image_text,
	);

	output_block_text_large_image( $args );



/*==============================================================================
  # Infoblock dedicated to branches
==============================================================================*/

	$info_heading = ( $current_office && $temp = get_field( 'service_sub_info_heading', $current_office->ID ) ) ? $temp : get_field('area_info_heading');
	$info_text = ( $current_office && $temp = get_field( 'service_sub_info_text', $current_office->ID ) ) ? $temp : get_field('area_info_text');

	$args = array(
		'id'  				=> 'branscher',
		'heading' 			=> $info_heading,
		'text' 				=> $info_text,
		'show_bg' 			=> get_field('area_info_show_background')
	);

	if ( $current_office && have_rows( 'service_sub_info_content', $current_office->ID ) ) : 

		while( have_rows( 'service_sub_info_content', $current_office->ID ) ) : the_row();

			$type = get_sub_field( 'info_type' );

			if( $type === 'branch' ) :

				$branch = get_sub_field( 'branch' );
				$title = get_the_title( $branch->ID );
				$text = get_field( 'text', $branch->ID );

				$args['items'][] = array(
					'heading' 	=> $title,
					'text'		=> $text
				);

			elseif ( $type === 'custom_text' ) :

				$args['items'][] = array(
					'heading' 	=> get_sub_field( 'custom_heading' ),
					'text'		=> get_sub_field( 'custom_text' )
				);
				
			endif; 

		endwhile;

	else : 

		if( have_rows( 'area_info_information' ) ) :
			while( have_rows( 'area_info_information' ) ) : the_row();

				$type = get_sub_field( 'info_type' );

				if( $type === 'branch' ) :

					$branch = get_sub_field( 'branch' );
					$title = get_the_title( $branch->ID );
					$text = get_field( 'text', $branch->ID );

					$args['items'][] = array(
						'heading' 	=> $title,
						'text'		=> $text
					);
				elseif ( $type === 'custom_text' ) :

					$args['items'][] = array(
						'heading' 	=> get_sub_field( 'custom_heading' ),
						'text'		=> get_sub_field( 'custom_text' )
					);
					
				endif; 

			endwhile;
		endif;

	endif;
	
	output_block_info_block( $args );



/*==============================================================================
  # FAQ
==============================================================================*/

	$faq_heading = ( $current_office && $temp = get_field( 'service_sub_faq_heading', $current_office->ID ) ) ? $temp : get_field('area_faq_heading');
	$faq_text = ( $current_office && $temp = get_field( 'service_sub_faq_text', $current_office->ID ) ) ? $temp : get_field('area_faq_text');

	$args = array(
		'id'  				=> 'faq',
		'heading' 			=> $faq_heading,
		'text' 				=> $faq_text,
		'first_open' 		=> get_field('area_faq_first_open')
	);

	if ( have_rows('area_faq_part') ) :
		while ( have_rows('area_faq_part') ) : the_row();

			$args['items'][] = get_sub_field('faq');

		endwhile;
	endif;

	if( $current_office && have_rows('service_sub_faq', $current_office->ID) ) :
		while ( have_rows('service_sub_faq', $current_office->ID) ) : the_row();

			$args['items'][] = get_sub_field('faq');

		endwhile;
	endif;


/*==============================================================================
  # Simple text block
==============================================================================*/

	$text_block_heading = ( $current_office && $temp = get_field( 'service_sub_bot_text', $current_office->ID ) ) ? $temp : get_field('area_text_heading');
	$text_block_text = ( $current_office && $temp = get_field( 'service_sub_bot_text', $current_office->ID ) ) ? $temp : get_field('area_text');

	$args = array(
		'id'  				=> 'arbetsprocesser',
		'marg_bot'  		=> '20',
		'marg_heading'  	=> 'md',
		'padding'  			=> 'lg',
		'bg_color'  		=> 'grey',
		'heading' 			=> $text_block_heading,
		'text' 				=> $text_block_text,
	);

	output_block_text( $args );


/*==============================================================================
  # Video/image block
==============================================================================*/

	$video_heading = ( $current_office && $temp = get_field( 'service_sub_work_at_opg_heading', $current_office->ID ) ) ? $temp : get_field('block_1_heading');
	$video_text = ( $current_office && $temp = get_field( 'service_sub_work_at_opg_text', $current_office->ID ) ) ? $temp : get_field('block_1_text');	

	$youtube_id = ( $temp_ytp = get_field('block_1_youtube_id') ) ? 'http://youtu.be/'.$temp_ytp : '';
	$args = array(
		'marg_bot' 			=> '100',
		'media_placement' 	=> get_field('block_1_media_placement'),
		'media_type' 		=> get_field('block_1_media_type'),
		'youtube_id'		=> $youtube_id,
		'image'				=> get_field('block_1_image'),
		'heading' 			=> $video_heading,
		'text' 				=> $video_text,
	);

	output_block_text_video_image( $args );


/*==============================================================================
  # Contactform
==============================================================================*/


$form_heading = ( $current_office && $temp = get_field( 'service_sub_form_heading', $current_office->ID ) ) ? $temp : get_field('area_form_heading');

$form_text = ( $current_office && $temp = get_field( 'service_sub_form_text', $current_office->ID ) ) ? $temp : get_field('area_form_text');

$args = array(
	'id' 		=> 'kontaktformular',
	'marg_bot' 	=> '150',
	'heading' 	=> $form_heading,
	'text' 		=> $form_text,
	'shortcode' => get_field('area_form_shortcode'),
);

output_block_contactform( $args );


/*==============================================================================
  # Contactpersons
==============================================================================*/

if ( $current_office && have_rows( 'service_sub_contact_persons', $current_office->ID ) ) :

	$args = array(
		'id'  				=> 'kontakt-personer',
		'heading'  			=> 'Kontaktpersoner',
		'marg_bot'  		=> '150',
	);

	while ( have_rows( 'service_sub_contact_persons', $current_office->ID ) ) : the_row();

		$coworker_area = array();
		$coworker_area['area'] = get_sub_field( 'area' );

		while ( have_rows( 'coworkers' ) ) : the_row();

			$coworker_area['coworkers'][] = get_sub_field( 'coworker' );

		endwhile;

		$args['coworker_areas'][] = $coworker_area;

	endwhile;

	output_block_coworkers( $args );

endif;


/*==============================================================================
  # Call-to-action blocks
==============================================================================*/

	if( have_rows('area_cta') ) :

		$cnt = 1;

		while( have_rows('area_cta') ) : the_row();

			$bg_type = get_sub_field('bg_type');

			$args = array(
				'id' 				=> get_sub_field('id'),
				'row_number' 		=> $cnt,
				'marg_bot' 			=> '20',
				'pad_top' 			=> get_sub_field('padding_top'),
				'pad_bot' 			=> get_sub_field('padding_bot'),
				'bg_type' 			=> $bg_type,
				'heading' 			=> get_sub_field('heading'),
				'text' 				=> get_sub_field('text'),
				'content_type' 		=> 'link',
				'position' 			=> get_sub_field('positioning'),
			);


			if ( $bg_type === 'image' ) : 

				$args['bg_image'] = get_sub_field('bg_image');

			elseif ( $bg_type === 'color' ) :

				$args['bg_color'] = get_sub_field('bg_color');

			endif;


			if( have_rows('links') ) :

				while( have_rows('links') ) : the_row();

					$link_type = get_sub_field('link_type');

					$link = array(
						'link_type' => $link_type
					);

					if( $link_type !== 'office' && $link_type !== 'office_contact' ) :

						$permalink = get_sub_field($link_type);

						if ( $link_type === 'dynamic' ) : 
							$standard_slug = str_replace( get_home_url(), '', $permalink);
							$permalink = get_dynamic_link( $standard_slug );
						endif; 

						$link['title'] = get_sub_field('link_title');
						$link['link'] = $permalink;

					endif; 

					$args['links'][] = $link;

				endwhile;

			endif;


			output_block_cta( $args );
			$cnt++;

		endwhile;
	endif;

	output_divider( 130 );


/*==============================================================================
  # Contactinformation
==============================================================================*/

if ( $current_office ) :

	$args = array(
		'id'  				=> 'kontaktuppgifter',
		'marg_bot'  		=> '150',
		'show_bg' 			=> false,
	);
	
	$contact = '';
	$contact .= ( $temp = get_field('office_main_mail', $current_office->ID ) ) ? convert_string_to_link( $temp, 'mail' ).'</br>' : '';
	$contact .= ( $temp = get_field('office_main_phone', $current_office->ID ) ) ? convert_string_to_link( $temp, 'phone' ).'</br>' : '';
	
	if( $contact ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Kontaktuppgifter', 'Contact' ),
			'text'		=> $contact
		);
	endif;

	$adress = '';
	$adress .= ( $temp = get_field('office_visitors_address', $current_office->ID ) ) ? $temp.'</br>' : '';
	$adress .= ( $temp = get_field('office_zip_code', $current_office->ID ) ) ? $temp.'</br>' : '';

	if( $adress ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Besöksadress', 'Address' ),
			'text'		=> $adress
		);
	endif;

	$billing = '';
	$billing .= ( $temp = get_field('office_inovice_mail', $current_office->ID ) ) ? convert_string_to_link( $temp, 'mail' ).'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_name', $current_office->ID ) ) ? $temp.'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_address', $current_office->ID ) ) ? $temp.'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_zip', $current_office->ID ) ) ? $temp.'</br>' : '';

	if( $billing ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Faktureringsadress', 'Billing information' ),
			'text'		=> $billing
		);
	endif;

	output_block_info_block( $args );

endif;

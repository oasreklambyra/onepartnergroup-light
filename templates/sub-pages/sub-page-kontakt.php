<?php 
	//Get the url so it is possible to check current location
		$url = explode('/', $_SERVER[REQUEST_URI]);
		$url = array_filter($url, create_function('$value', 'return $value !== "";')); 

		$current_office = false;
		$current_office = get_posts( 
			array(
				'name'            => esc_attr($url[1]),
				'post_type'       => 'office',
				'post_status'     => 'publish',
				'posts_per_page'  => 1,
			) 
		);
		$current_office = $current_office[0];

		$page_id = ( $current_office ) ? $current_office->ID : get_the_ID(); 



/*==============================================================================
  # Intro element block
==============================================================================*/

	$intro_heading = ( $current_office ) ? 'Kontakta oss i '.$current_office->post_title : get_field( 'intro_heading' );
	$intro_text = ( $current_office ) ? get_field('contact_sub_text', $current_office->ID ) : get_field( 'intro_text' );

	$args = array(
		'heading' 	=> $intro_heading,
		'text' 		=> $intro_text
	);
	output_intro_element( $args );


/*==============================================================================
  # Coworker list, only for office pages
==============================================================================*/

	if ( $current_office ) :
		$args = array(
			'id'  				=> 'personal',
			'marg_bot'  		=> '200',
		);

		if ( have_rows( 'office_coworkers', $current_office->ID ) ) : 

			while ( have_rows( 'office_coworkers', $current_office->ID ) ) : the_row();

				$coworker_area = array();
				$coworker_area['area'] = get_sub_field( 'area' );

				while ( have_rows( 'coworkers' ) ) : the_row();

					$coworker_area['coworkers'][] = get_sub_field( 'coworker' );

				endwhile;

				$args['coworker_areas'][] = $coworker_area;

			endwhile;
		endif;

		output_block_coworkers( $args );
	endif;



/*==============================================================================
  # Contactform
==============================================================================*/

	$form_heading = ( $current_office ) ? get_field('contact_sub_form_heading',$current_office->ID) : get_field('area_form_heading');
	$form_text = ( $current_office ) ? get_field('contact_sub_form_text',$current_office->ID) : get_field('area_form_text');

	$args = array(
		'id' 		=> 'kontaktformular',
		'marg_bot' 	=> '200',
		'heading' 	=> $form_heading,
		'text' 		=> $form_text,
		'shortcode' => get_field('area_form_shortcode'),
	);

	output_block_contactform( $args );



/*==============================================================================
  # Map, main page shows all, office page show only it self
==============================================================================*/
 
	// $args = array(
	// 	'id'  				=> 'karta',
	// 	'marg_bot'  		=> '200',
	// 	'heading' 			=> get_sub_field('heading'),
	// 	'text' 				=> get_sub_field('text')
	// );

	// if ( $current_office ) :

	// 	$args['markers'][] = get_field('map',$current_office->ID);
	// 	$args['map_heading'] = get_field('office_visitors_address', $current_office->ID );
	// 	$args['map_text'] = get_field('office_zip_code', $current_office->ID );

	// else : 

	// 	$offices = get_posts( 
	// 		array(
	// 			'post_type'       => 'office',
	// 			'post_status'     => 'publish',
	// 			'posts_per_page'  => -1,
	// 		) 
	// 	);

	// 	foreach( $offices as $office ):

	// 		$marker = get_field('map',$office->ID);
	// 		$marker['url'] = get_the_permalink($office->ID);
	// 		$args['markers'][] = $marker;

	// 	endforeach;
	// endif;

	// output_block_map( $args );



/*==============================================================================
  # Contactdetails
==============================================================================*/
 
	$args = array(
		'id'  				=> 'kontaktuppgifter',
		'marg_bot'  		=> '150',
		'show_bg' 			=> false,
	);
	
	$contact = '';
	$contact .= ( $temp = get_field('office_main_mail', $page_id ) ) ? convert_string_to_link( $temp, 'mail' ).'</br>' : '';
	$contact .= ( $temp = get_field('office_main_phone', $page_id ) ) ? convert_string_to_link( $temp, 'phone' ).'</br>' : '';
	
	if( $contact ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Kontaktuppgifter', 'Contact' ),
			'text'		=> $contact
		);
	endif;

	$adress = '';
	$adress .= ( $temp = get_field('office_visitors_address', $page_id ) ) ? $temp.'</br>' : '';
	$adress .= ( $temp = get_field('office_zip_code', $page_id ) ) ? $temp.'</br>' : '';

	if( $adress ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Besöksadress', 'Address' ),
			'text'		=> $adress
		);
	endif;

	$billing = '';
	$billing .= ( $temp = get_field('office_inovice_mail', $page_id ) ) ? convert_string_to_link( $temp, 'mail' ).'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_name', $page_id ) ) ? $temp.'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_address', $page_id ) ) ? $temp.'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_zip', $page_id ) ) ? $temp.'</br>' : '';

	if( $billing ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Faktureringsadress', 'Billing information' ),
			'text'		=> $billing
		);
	endif;

	output_block_info_block( $args );
?>
<?php 


/*==============================================================================
  # THIS SUB PAGE IS INACTIVE - CONTENT MOVED TO ARCHIVE PAGE
==============================================================================*/


	//Get the url so it is possible to check current location
		$url = explode('/', $_SERVER[REQUEST_URI]);
		$url = array_filter($url, create_function('$value', 'return $value !== "";'));

		$current_office = false;
		$current_office = get_posts( 
			array(
				'name'            => esc_attr($url[1]),
				'post_type'       => 'office',
				'post_status'     => 'publish',
				'posts_per_page'  => 1,
			) 
		);
		$current_office = $current_office[0];



/*==============================================================================
  # Intro element block
==============================================================================*/

	$intro_heading = ( $current_office && $temp = get_field( 'education_sub_intro_heading', $current_office->ID ) ) ? $temp : get_field( 'intro_heading' );
	$intro_text = ( $current_office && $temp = get_field( 'education_sub_intro_text', $current_office->ID ) ) ? $temp : get_field( 'intro_text' );

	$args = array(
		'heading' 	=> $intro_heading,
		'text' 		=> $intro_text
	);
	output_intro_element( $args );



/*==============================================================================
  # Textblock
==============================================================================*/

	$textblock_heading = ( $current_office && $temp = get_field( 'education_sub_main_heading', $current_office->ID ) ) ? $temp : get_field('area_text_heading');
	$textblock_text = ( $current_office && $temp = get_field( 'education_sub_main_text', $current_office->ID ) ) ? $temp : get_field('area_text');

	$args = array(
		'id'  				=> '',
		'marg_heading'  	=> 'sm',
		'bg_color'  		=> 'none',
		'heading' 			=> $textblock_heading,
		'text' 				=> $textblock_text,
	);

	output_block_text( $args );


/*==============================================================================
  # Education archive
==============================================================================*/

	get_template_part('templates/content/content', 'education-archive');


/*==============================================================================
  # Call-to-action
==============================================================================*/

	if( have_rows('area_cta') ) :

		$cnt = 1;

		while( have_rows('area_cta') ) : the_row();

			$bg_type = get_sub_field('bg_type');

			$args = array(
				'id' 				=> get_sub_field('id'),
				'row_number' 		=> $cnt,
				'marg_bot' 			=> '20',
				'pad_top' 			=> get_sub_field('padding_top'),
				'pad_bot' 			=> get_sub_field('padding_bot'),
				'bg_type' 			=> $bg_type,
				'heading' 			=> get_sub_field('heading'),
				'text' 				=> get_sub_field('text'),
				'content_type' 		=> 'link',
				'position' 			=> get_sub_field('positioning'),
			);

			if ( $bg_type === 'image' ) : 

				$args['bg_image'] = get_sub_field('bg_image');

			elseif ( $bg_type === 'color' ) :

				$args['bg_color'] = get_sub_field('bg_color');

			endif;

			if( have_rows('links') ) :

				while( have_rows('links') ) : the_row();

					$link_type = get_sub_field('link_type');

					$link = array(
						'link_type' => $link_type
					);

					if( $link_type !== 'office' && $link_type !== 'office_contact' ) :

						$permalink = get_sub_field($link_type);

						if ( $link_type === 'dynamic' ) : 
							$standard_slug = str_replace( get_home_url(), '', $permalink);
							$permalink = get_dynamic_link( $standard_slug );
						endif; 

						$link['title'] = get_sub_field('link_title');
						$link['link'] = $permalink;

					endif; 

					$args['links'][] = $link;

				endwhile;

			endif;

			output_block_cta( $args );
			$cnt++;

		endwhile;
	endif;
?>


<?php output_divider( 130 ); ?>
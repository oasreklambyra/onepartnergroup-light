<?php 
	$intro_image = get_field( 'intro_image' );
	$heading_intro_image = get_field( 'intro_heading' );
	$text_intro_image = get_field( 'intro_text' );
	$font_size = ( is_front_page() ) ? 'font-size-intro': 'font-size-intro-office';

	!$hide_btn_container = get_field( 'hide_btn_container' );
	$heading_contact = get_field( 'heading_contact' );
	$text_contact = get_field( 'text_contact' );
	$contact_element_url = get_template_directory().'/dist/images/intro-image-contact-element.svg';

	$link_contact = array();
	$link_type = get_field( 'link_type' );
	
	if( $link_type === 'office' ){

		$link_contact['link_type'] = $link_type;

	}else{

		$permalink = get_field( $link_type );


		if ( $link_type === 'dynamic' ) : 
			$standard_slug = str_replace( get_home_url(), '', $permalink);
			$permalink = get_dynamic_link( $standard_slug );
		endif; 

		$link_contact['link_type']  = $link_type;
		$link_contact['title'] = get_field( 'link_title' );
		$link_contact['link']  = $permalink;
	}


	$link_job = '';
	if ( !$hide_btn_container && $template = get_field( 'template_jobbsokande', 'options' ) ) :

		$link = get_permalink( $template->ID );
		$slug = str_replace( get_home_url(), '', $link);
		$link_job = get_dynamic_link( $slug );

	endif;

	$education_link = '';
	if ( !$hide_btn_container /*&& $template = get_field( 'template_utbildning', 'options' )*/ ) :

		/*$link = get_permalink( $template->ID );
		$slug = str_replace( get_home_url(), '', $link);
		$link_education = get_dynamic_link( $slug );*/

		$link_education = get_post_type_archive_link( 'course' );

	endif; 

	$link_recuit = '';
	if ( !$hide_btn_container && $template = get_field( 'template_tjanster', 'options' ) ) :

		$link = get_permalink( $template->ID );
		$slug = str_replace( get_home_url(), '', $link);
		$link_recuit = get_dynamic_link( $slug );

	endif;
?>

<section class="section section__intro section__intro__image<?= ( is_front_page() ) ? ' front-page': ''; ?>">

	<?php /* output_background_image_style( 'intro-image', 'intro_image', $intro_image ) ?>

	<?php if ( have_rows('align_intro_image') ) : ?>
		<style type="text/css">
			<?php while( have_rows('align_intro_image') ) : the_row(); ?>

				@media screen and (min-width: <?= ( $screen = get_sub_field('device_width') ) ? $screen.'px' : '0px'; ?> ) {
					#intro-image{
    					background-position: <?= get_sub_field('x_axis') ?> <?= get_sub_field('y_axis') ?> !important;
					}
				}

			<?php endwhile; ?>
		</style>
	<?php endif; */ ?>

	<div class="background-image-container">

		<div id="intro-image" class="bg-image">

			<div class="placeholder" style="background-image: url('<?= $intro_image['sizes']['partner'] ?>');"></div>

			<picture>
			
				<source media="(min-width: 1200px)" data-srcset="<?= $intro_image['sizes']['intro_image_xl'] ?>, <?= $intro_image['sizes']['intro_image_xl_retina'] ?> 2x">
			
				<source media="(min-width: 992px)" data-srcset="<?= $intro_image['sizes']['intro_image_lg'] ?>, <?= $intro_image['sizes']['intro_image_lg_retina'] ?> 2x">
			
				<source media="(min-width: 580px)" data-srcset="<?= $intro_image['sizes']['intro_image_sm'] ?>, <?= $intro_image['sizes']['intro_image_sm_retina'] ?> 2x">
			
				<source media="(min-width: 0px)" data-srcset="<?= $intro_image['sizes']['intro_image'] ?>, <?= $intro_image['sizes']['intro_image_retina'] ?> 2x">
			
				<img class="lazyload main-intro-image" data-src="<?= $intro_image['sizes']['intro_image'] ?>" alt="<?= $title; ?>">
			
			</picture>



			<img class="lazyload filter-image" data-src="<?= get_template_directory_uri(); ?>/dist/images/opg_hemsida_fade.png">

		</div>

		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10">
					
					<?php if ( $heading_intro_image || $text_intro_image ) : ?> 
						<div class="text-container">
							<?= ( $heading_intro_image ) ? '<h1 class="heading '.$font_size.'">'.$heading_intro_image.'</h1>' : ''; ?>
							<?= ( $text_intro_image ) ? '<p class="text">'.$text_intro_image.'</p>' : ''; ?>
						</div>
					<?php endif; ?>

					<ul class="cta-btns-container">

						<?php if ( !$hide_btn_container ) : ?>

							<?= ( $link_job ) ? '<li><a class="cta-btn btn--large btn--white box-shadow" href="'.$link_job.'">'.lang_text( 'Söker du jobb?', 'Looking for a job?'  ).'</a></li>' : ''; ?>
							<?= ( $link_recuit ) ? '<li><a class="cta-btn btn--large btn--white box-shadow" href="'.$link_recuit.'">'.lang_text( 'Söker du personal?', 'Want to recuit?' ).'</a></li>' : ''; ?>
							<?= ( $link_education ) ? '<li><a class="cta-btn btn--large btn--white box-shadow" href="'.$link_education.'">'.lang_text( 'Utbilda din personal?', 'Want to educate your staff?' ).'</a></li>' : ''; ?>

						<?php endif; ?>
					</ul>

				</div>
			</div>
		</div>

	</div>


	<div class="section__call-to-action">
		<div class="contact-background">

			<div class="contact-background-element"><?= file_get_contents( $contact_element_url ); ?></div>
			
			<?php 
				$content[] = $link_contact;
				$args = array(
					'content' 		=> $content,
					'heading' 		=> $heading_contact,
					'text' 			=> $text_contact,
				);
				output_cta_container( $args ); 
			?>

		</div>
	</div>

</section>
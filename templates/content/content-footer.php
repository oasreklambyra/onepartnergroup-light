<?php 
global $lang; 
$logo = get_field( 'main_logo', 'options' );
$home_url = get_home_url();
?>

<footer id="footer" class="footer">
	<div class="container-fluid">

		<div class="row justify-content-center">
			<div class="col-12 col-md-10">
				<div class="row">
					
					<div class="col-12 col-md-4 col-lg-3">

						<?php if ( $logo ) : ?>
							<a class="logo-link" href="<?= $home_url ?>">
								<?php // $logo['url'] is expected to be a svg, thats why ['url'] is used ?>
								<img class="logo lazyload" data-src="<?= $logo['url'] ?>" alt="<?php bloginfo('name'); ?>">
							</a>
						<?php endif; ?>

						<?php if ( $footer_text ) : ?>
							<div class="footer-text text-container">
								<p class="text">
									<?= $footer_text ?>
								</p>
							</div>
						<?php endif; ?>

					</div>

					<div class="col-12 col-md-8 col-lg-9">
						<div class="link-groups">

							<?php if ( have_rows( 'footer_small_nav', 'options' ) ) : ?>

											<?php $footer_small_nav ='footer_small_nav' ?>

											<?php while ( have_rows( $footer_small_nav, 'options' ) ) : the_row(); ?>

												<?php 
												$type_of = get_sub_field( 'type_of' );
												$title = get_sub_field( 'title' );
												$link = get_sub_field( $type_of );
												$target = get_sub_field( 'target' );

												if ( $type_of === 'dynamic' ) : 

													$slug = str_replace( get_home_url(), '', $link );
													$link = get_dynamic_link( $slug );

												endif; 
												?>

												<li class="link"><a href="<?= $link ?>"<?= ( $target ) ? ' target="_blank"': ''; ?>><?= $title ?></a></li>

											<?php endwhile; ?>

							<?php endif; ?>

						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="col-12">
			<div class="divider"></div>
		</div>


	</div>
</footer>
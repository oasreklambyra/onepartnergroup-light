<?php 

	// is_archive() is to check if the flexible content for courses archive should be used
	$id = ( is_archive() ) ? 'options' : get_the_ID();

	if( have_rows( 'blocks', $id ) ):

	    global $row_number;
	    $row_number = 0;

	    while ( have_rows( 'blocks', $id ) ) : the_row();

	    	$row_layout = get_row_layout();

	        if( $row_layout ) : 
	        	
	        	get_template_part( 'templates/blocks/block', $row_layout ); 

	        endif;

	        $row_number++;

	    endwhile;
	endif;
?>
<?php
	global $wp_query;
	$latest_post = $wp_query->posts[0];
	
	$pre_set_category = false;
	$queried_obj = get_queried_object();

	if ( $queried_obj && $queried_obj->term_id ) :
		
		$pre_set_category = $queried_obj;

	endif; 
?>

<section class="section section__search-posts marg-bot-50">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">

				<form class="search-form" method="POST">

					<div class="main-search">
						<input type="text" name="posts-s" value="" placeholder="Sök...">
						<span class="instructions">Tryck enter för att söka</span>
					</div>

					<div class="search-filter">

						<div id="settings-btn" class="settings-btn">Filtreringsinställningar</div>
						

						<ul class="filter-posts-categories">
							
							<li class="filter-item<?= ( $pre_set_category ) ? '': ' selected';?>" data-value="">Alla</li>

							<?php 
								$terms = get_terms( array(
								    'taxonomy' => 'category',
								    'hide_empty' => true,
								) );

								foreach ( $terms as $term ) : 

									$selected = ( $pre_set_category && $pre_set_category->slug === $term->slug ) ? ' selected': '';
									echo '<li class="filter-item'.$selected.'" data-value="'.$term->slug.'">'.$term->name.'</li>';

								endforeach;
							?>
						</ul>

						<input type="hidden" name="posts-category" value="<?= ( $pre_set_category ) ? $pre_set_category->slug: '';?>">

						</div>


					<input type="submit" class="hide-submit" tabindex="-1" />

				</form>

			</div>
		</div>
	</div>
</section>


<?php 
	$args = array(
		'posts_per_page'   => 10,
		'exclude'          => $latest_post->ID,
	);

	if ( $pre_set_category ) :

		$args['category'] = $pre_set_category->term_id;

	endif;

	$blog_items = get_posts( $args );

	$args['posts_per_page'] = -1; 
	$total_results = count( get_posts( $args ) );
?>

<section class="section section__posts marg-bot-200">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">
				<div class="row">
					
				<?php if( $blog_items ) : ?> 

					<div class="col-12">
						<ul id="item-feed" class="item-feed">
						
							<?php
								$clearfix_key = 2;
								$add_two = false;
								foreach( $blog_items as $key => $blog_item ) :

									$large = ( $key === 0 || $key === 6 ) ? true : false;

									output_blog_item_card( $blog_item, $large );

									if ( (($key+1)%2) === 0 ) :
										echo '<div class="clearfix d-md-none"></div>';
									endif;

									if( (($key+1)%$clearfix_key) === 0 ) : 

										echo '<div class="clearfix d-none d-md-block"></div>';

										$clearfix_key += ( $add_two ) ? 2: 3;
										$add_two = ( $add_two ) ? false : true;


									endif;

								endforeach;
							?>

						</ul>
					</div>


				<?php else : ?>
					
					<div class="col-12">
						
						<?php output_no_posts_found(); ?>

					</div>

				<?php endif; ?>	


				<div id="lm-container" class="col-12 load-more-container text-center<?= ( $total_results > 10 ) ? '': ' hide'; ?>">

					<div id="lm-animation" class="loading-animation">
						<img class="ele" src="<?= get_template_directory_uri().'/dist/images/loading.svg'; ?>" alt="Laddar...">
					</div>

					<br>

					<button id="lm-btn" data-type="posts" data-exclude="<?= $latest_post->ID ?>" class="btn--large btn--blue">Visa fler inlägg</button>
				</div>


				</div>
			</div>
		</div>
	</div>
</section>
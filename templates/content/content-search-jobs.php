<section class="section section__serach-jobs">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">

				<form class="search-form">

					<div class="search-jobs">
						<input type="text" name="s" value="" placeholder="<?= lang_text( 'Sök...', 'Search...' ) ?>">
						<span class="instructions"><?= lang_text( 'Tryck enter för att söka', 'Press enter to search' ) ?></span>
					</div>

					<div class="search-filter">

						<div class="dropdown-container">
							
						</div>

						<div class="num-of-results">
							
						</div>

						<div class="select-container">

						</div>

					</div>

				</form>

			</div>
		</div>
	</div>
</section>
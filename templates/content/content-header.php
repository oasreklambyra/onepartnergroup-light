<?php 
global $lang;
global $post;
$opg_logo = get_field( 'main_logo', 'options' );
$external_logo = get_field( 'external_logo', $post->ID );
$home_url = get_home_url();
?>
<header id="banner" class="banner">

    <!--[if IE]>
        <div class="alert alert-warning">
        Du använder en <strong>utdaterad</strong> webbläsare som inte längre stöds. För bästa upplevelse <a href="http://browsehappy.com/" target="_blank">uppdatera din webbläsare</a>.
        </div>
      <![endif]-->

      <div class="row justify-content-center">
        <div class="col-12">

          <div class="logo-container">
            <a class="logo-link" href="<?= $home_url ?>">

              <?php if($external_logo) : ?>

                <img class="logo" src="<?= $external_logo['url'] ?>" alt="<?php bloginfo('name'); ?>">

                <?php else : ?>

                  <img class="logo" src="<?= $opg_logo['url'] ?>" alt="<?php bloginfo('name'); ?>">

                <?php endif; ?>

              </a>
            </div>

            <nav class="nav header-nav">

              <div class="menu-btn">
                <div class="menu-row top"></div>
                <div class="menu-row mid"></div>
                <div class="menu-row bot"></div>                        
              </div>

            </nav>
          </div>

        </div>
      </div>
    </header>

    <nav class="nav main-nav">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="menu-content col-12 col-md-10 col-xl-8">

            <nav class="menu">
              <div class="menu-inner primary">

                <?php $menu_main = 'menu_main_swe' ?>

                <?php while ( have_rows( $menu_main, 'options' ) ) : the_row(); ?>

                  <?php 
                  $type_of = get_sub_field( 'type_of' );
                  $title = get_sub_field( 'title' );
                  $link = get_sub_field( $type_of );
                  $target = get_sub_field( 'target' );

                  if ( $type_of === 'dynamic' ) : 

                    $slug = str_replace( get_home_url(), '', $link );
                    $link = get_dynamic_link( $slug );

                  endif; 
                  ?>

                  <div class="menu-item"><a href="<?= $link ?>"<?= ( $target ) ? ' target="_blank"': ''; ?>><?= $title ?></a></div>

                <?php endwhile; ?>

              </div>
            </nav>

            <?php if ( have_rows( 'menu_swe', 'options' ) ) : ?>

              <nav class="menu">
                <div class="menu-inner secondary">

                  <?php while ( have_rows( 'menu_swe', 'options' ) ) : the_row(); ?>

                    <?php 
                    $type_of = get_sub_field( 'type_of' );
                    $title = get_sub_field( 'title' );
                    $link = get_sub_field( $type_of );
                    $target = get_sub_field( 'target' );

                    if ( $type_of === 'dynamic' ) : 

                      $slug = str_replace( get_home_url(), '', $link );
                      $link = get_dynamic_link( $slug );

                    endif; 
                    ?>

                    <div class="menu-item"><a href="<?= $link ?>"<?= ( $target ) ? ' target="_blank"': ''; ?>><?= $title ?></a></div>

                  <?php endwhile; ?>

                </div>
              </nav>

            <?php endif; ?>


          </div>
        </div>
      </nav>

      <?php // ACF ändra färg på sidans a-taggar ?>
      <?php $link_color = get_field('link-color'); ?>
      <?php if ($link_color) : ?>
        <style>

        a {color: <?php the_field('link-color'); ?>!important;}
        
        .btn--white, .container__call-to-action .newsletter_cta .newsletter button, 
        .container__call-to-action .newsletter_cta .newsletter input[type=submit], 
        .section__call-to-action .newsletter_cta .newsletter button, 
        .section__call-to-action .newsletter_cta .newsletter input[type=submit] {
          color: <?php the_field('link-color'); ?>!important;
        }

      </style>
      <?php endif; ?>
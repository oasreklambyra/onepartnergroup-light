<?php 

//Get query
	$args = array(
		'posts_per_page'   => 18,
		'offset'           => 0,
		'post_type'        => 'document',
		'orderby'          => 'menu_order title',
		'order'            => 'ASC',
		'post_status'      => 'publish',
		'suppress_filters' => false 
	);
	$documents = get_posts( $args );

//Get all posts so it is possible to count total
	$args['posts_per_page'] = -1;
	$total_results = count( get_posts( $args ) );

//Get values for filters
	$raw_document_categories = get_terms('document_category',array('parent'=>0, 'hide_empty'=>true));
	$all_document_categories = array();

	if ( $raw_document_categories ) :

		usort( $raw_document_categories, 'sort_term_by_name' );

		foreach( $raw_document_categories as $key => $parent ) :
			
			$all_document_categories[] = $parent;

			$child_document_categories = get_terms('document_category',array('parent'=>$parent->term_id, 'hide_empty'=>true));

			if ( $child_document_categories ) : 

				usort( $child_document_categories, 'sort_term_by_name' );

				foreach( $child_document_categories as $key => $child ) :

					$child->name = '- '.$child->name;
					$all_document_categories[] = $child;

				endforeach;

			endif;

		endforeach;

	endif;
?>

<section class="section section__search-documents">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">

				<form class="search-form" method="POST">

					<div class="main-search">
						<input type="text" name="document-s" value="" placeholder="<?= lang_text( 'Sök...', 'Search...' ) ?>">
						<span class="instructions"><?= lang_text( 'Tryck enter för att söka', 'Press enter to search' ) ?></span>
					</div>

					<div class="search-filter">

						<div class="num-of-results">
							<?= lang_text( 'Hittade <span id="total-results">'.$total_results.'</span> dokumentmallar', 'Found <span id="total-results">'.$total_results.'</span> document templates' ); ?>
						</div>


						<?php if ( $all_document_categories ) : ?> 

							<div id="settings-btn" class="settings-btn"><?= lang_text( 'Filtreringsinställningar', 'Filter settings' ); ?></div>
							
							<div id="settings-container" class="settings-container">
								
								<div class="dropdown-container">

									<div id="selectize-container-document" class="selectize-container" data-placeholder="<?= lang_text( 'Kategorier', 'Category' ) ?>">
				                        <select id="select-document-category" class="selectize selectize--filter" placeholder="<?= lang_text( 'Kategorier', 'Category' ) ?>">
				                            <option value="">Kategorier</option>

				                            <?php 
				                                foreach( $all_document_categories as $key => $type ) :
				                                    echo '<option value="'.$type->slug.'">'.$type->name.'</option>';
				                                endforeach; 
				                            ?>

				                        </select>
				                    </div>

								</div>

								<div id="filter-values" class="filter-values"></div>

							</div>

						<?php endif; ?>

					</div>

					<input type="submit" class="hide-submit" tabindex="-1" />

				</form>

			</div>
		</div>
	</div>
</section>


<section class="section section__documents">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">
				<div class="row">
					
				<?php if( $documents ) : ?> 

					<div class="col-12">
						<div id="item-feed" class="row">
						
							<?php
								foreach( $documents as $document ) :

									$icon = get_field( 'icon', $document->ID );
									$args = array(
										'type'		  => 'document',
										'permalink'   => get_field( 'document', $document->ID ),
										'target'   	  => '_blank',
										'image_sizes' => array(
											array(
												'url' 	=> $icon['sizes']['icon'],
												'retina' => $icon['sizes']['icon_retina'],
											),
										),
										'title' 	  => $document->post_title,
									);

									output_item_card( $args );

								endforeach;
							?>

						</div>
					</div>


				<?php else : ?>
					
					<div class="col-12">
						
						<?php output_no_documents_found(); ?>

					</div>

				<?php endif; ?>	


				<div id="lm-container" class="col-12 load-more-container text-center<?= ( $total_results > 18 ) ? '': ' hide'; ?>">

					<div id="lm-animation" class="loading-animation">
						<img class="ele" src="<?= get_template_directory_uri().'/dist/images/loading.svg'; ?>" alt="Laddar...">
					</div>

					<br>

					<button id="lm-btn" data-type="document" class="btn--large btn--blue"><?= lang_text( "Visa fler dokument", "Show more documents" ); ?></button>
				</div>


				</div>
			</div>
		</div>
	</div>
</section>
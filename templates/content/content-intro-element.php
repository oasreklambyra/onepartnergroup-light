<?php
	$intro_element_array = array(
		'/dist/images/header-element-1.svg',
		'/dist/images/header-element-2.svg',
		'/dist/images/header-element-3.svg',
		'/dist/images/header-element-4.svg',
	); 
	$rand_ele = rand(0,(count($intro_element_array)-1));
	$intro_element_url = get_template_directory().$intro_element_array[$rand_ele];
	$heading_intro_element = ( is_archive() ) ? lang_text( 'Hitta ditt</br>nya drömjobb.', 'Find your new </br>dream job.' ) : get_field( 'intro_heading' );
	$text_intro_element = ( is_archive() ) ? '' : get_field( 'intro_text' );
?>
<section class="section section__intro section__intro__element<?= ( is_archive() ) ? ' archive' : '';?>">
	<div class="background-element-container">

		<div class="background-element"><?= file_get_contents($intro_element_url); ?></div>

		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10 col-xl-8">

					<?php if ( $heading_intro_element || $text_intro_element ) : ?> 
						<div class="text-container">
							<?= ( $heading_intro_element ) ? '<h1 class="heading">'.$heading_intro_element.'</h1>' : ''; ?>
							<?= ( $text_intro_element ) ? '<p class="text">'.$text_intro_element.'</p>' : ''; ?>
						</div>
					<?php endif; ?>

				</div>
			</div>
		</div>

	</div>
</section>
<?php 
	global $wp_query;
?>

<section class="section section__serach">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">
				
				<h1 class="heading marg-bot-40"><?= lang_text( 'Sökresultat', 'Searchresults' ) ?></h1>

				<form class="organic-search" method="POST" action="/">

					<div class="main-search">
						<input type="text" name="s" value="<?= get_search_query() ?>" placeholder="<?= lang_text( 'Sök..', 'Search...' ) ?>">
						<span class="instructions"><?= lang_text( 'Tryck enter för att söka', 'Press enter to search' ) ?></span>
					</div>

					<div class="search-filter">
						<div class="num-of-results">
							<?= lang_text( 'Hittade '.$wp_query->found_posts.' resultat för sökningen "'.get_search_query().'"', 'Found '.$wp_query->found_posts.' for your search "'.get_search_query().'"' ) ?>
						</div>
					</div>

					<input type="submit" class="hide-submit" tabindex="-1" />
				</form>

			</div>
		</div>
	</div>
</section>


<section class="section section__serach">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">		

				<div class="search-results">

					<div id="organic-search-content">
					<?php
						if ( have_posts() ) :

				          	while ( have_posts() ) : the_post();
				            
				            	global $post;
				            	output_site_search_content( $post );

				          	endwhile;

				          	wp_reset_postdata();

				      	endif;
				    ?>
					</div>

					<?php if ( have_posts() ) : ?>

						<div id="lm-container" class="marg-bot-30 load-more-container text-center<?= ( $wp_query->max_num_pages > 1 ) ? '': ' hide'; ?>">

							<div id="lm-animation" class="loading-animation">
								<img class="ele" src="<?= get_template_directory_uri().'/dist/images/loading.svg'; ?>" alt="Laddar...">
							</div>

							<br>

							<button id="lm-btn" data-type="organic" data-search="<?= get_search_query() ?>" class="btn--large btn--blue"><?= lang_text( "Visa fler resultat", "Show more results" ); ?></button>
						</div>

					<?php else : ?>

						<div class="marg-bot-30">
							<text class="text-container">
								<h3 class="heading"><?= lang_text( 'Inget resultat hittades', 'No results found' ) ?></h3>
								<p class="text"><?= lang_text( 'Det gick tyvärr inte att hitta något som matchar din sökning. Du kan alltid testa att söka efter något annat.', 'Could not find any results that match your search. You can always try antoher seach.' ) ?></p>
							</text>
						</div>

					<?php endif; ?>
				</div>

			</div>
		</div>
	</div>
</section>
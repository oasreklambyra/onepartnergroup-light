<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<section class="section section__office">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10">
					<h1>Nyhet: <?php the_title(); ?></h1>

					<div class="marg-top-50 post-content">
						<?php the_content(); ?>
					</div>

				</div>
			</div>
		</div>
	</section>

<?php endwhile; endif; ?>
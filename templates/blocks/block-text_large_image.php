<?php 
	$args = array(
		'id'  				=> get_sub_field('id'),
		'marg_bot'  		=> get_sub_field('marg_bot'),
		'media_placement' 	=> get_sub_field('media_placement'),
		'image' 			=> get_sub_field('image'),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
	);

	output_block_text_large_image( $args );
?>
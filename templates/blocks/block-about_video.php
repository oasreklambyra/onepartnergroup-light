<?php 
	$ele_id = ( get_sub_field('id') ) ? ' id="'.get_sub_field('id').'"': '';
	$marg_bot = get_sub_field('marg_bot');
	$heading = get_sub_field('heading');
	$text = get_sub_field('text');
	$show_link = get_sub_field('show_link');
	$background_element_url = get_template_directory_uri().'/dist/images/video-element.svg';
	$media_type = get_sub_field( 'media_type' );
?>
<section<?= $ele_id ?> class="section section__about_video marg-bot-<?= $marg_bot ?>">

	<?php /* <div class="background-element"><?= file_get_contents($background_element_url); ?></div> */ ?>
	<div class="background-element"><img src="<?= $background_element_url ?>"></div>

	<div class="container-fluid">		
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">


				<div class="row flex-wrap">
					
					<?php if( $heading || $text ) : ?>
						<div class="col-12 col-md-6 order-md-2">
							<div class="text-container-outer">
								<div class="text-container">
									<?= ( $heading ) ? '<h2 class="heading">'.$heading.'</h2>': ''; ?>
									<?= ( $text ) ? '<p class="text">'.$text.'</p>': ''; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<div class="col-12 col-md-6 order-md-1">
						
						<?php if( $media_type === 'video' ) : ?>

							<?php $youtube_id = ( $temp_ytp = get_sub_field('youtube_id') ) ? 'http://youtu.be/'.$temp_ytp : ''; ?>

							<?php if( $youtube_id ) : ?>


								<div class="video-container-outer">
									<div class="video-container">
										<div class="video player" data-property="
										{
											videoURL	    : '<?= $youtube_id ?>',
											containment     : 'self',
											showYTLogo      : false,
											showControls    : true,
											autoPlay	    : false,
											stopMovieOnBlur : false,
											loop		    : false,
											mute		    : false,
											opacity		    : 1,
											gaTrack		    : false,
											quality		    : 'highres',
										}">
											<img class="video-loader" src="<?= get_template_directory_uri().'/dist/images/loading.svg' ?>">

											<div class="video-overlay">
												<img class="video-player-btn" src="<?= get_template_directory_uri().'/dist/images/player.svg' ?>">
											</div>

										</div>
									</div>
								</div>


							<?php endif; ?>

						<?php else : ?>
							
							<?php $image = get_sub_field( 'image' ); ?>

							<?php if( $image ) : ?>

								<div class="image-container">
									<picture>
										<source media="(min-width: 0px)" data-srcset="<?= $image['sizes']['intro_image'] ?>, <?= $image['sizes']['intro_image_retina'] ?> 2x">
										<img class="lazyload image" data-src="<?= $image['sizes']['intro_image'] ?>" alt="<?= $image['alt'] ?>">
									</picture>
								</div>

							<?php endif; ?>
		
						<?php endif; ?>
















					</div>

					<?php if( $show_link ) : ?>
						<?php 
							$link_title = get_sub_field('link_title');
							$link = get_sub_field(get_sub_field('link_type'));
						?>
						<div class="col-12 col-xl-custom order-3">				
							<div class="link-container">
								<a class="cta-link" href="<?= $link ?>"><?= $link_title ?></a>
							</div>
						</div>
					<?php endif; ?>

				</div>

			</div>
		</div>
	</div>
</section>
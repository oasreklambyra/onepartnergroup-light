<?php 
	$args = array(
		'id'  				=> get_sub_field('id'),
		'marg_bot'  		=> get_sub_field('marg_bot'),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
	);

	if ( get_sub_field( 'use_general_certificates' ) ) :

		if( have_rows( 'general_certificates', 'options' ) ) :
			while( have_rows( 'general_certificates', 'options' ) ) : the_row();

				$args['items'][] = get_sub_field( 'certificate' );

			endwhile;
		endif;

	else :

		if( have_rows( 'partner_rows' ) ) :
			while( have_rows( 'partner_rows' ) ) : the_row();

				$args['items'][] = get_sub_field( 'partner' );			

			endwhile;
		endif;

	endif;
	

	output_block_partners( $args );
?>
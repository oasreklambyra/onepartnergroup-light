<?php 
	global $row_number;

	$args = array(
		'id'  				=> get_sub_field('id'),
		'row_number'  		=> $row_number,
		'marg_bot'  		=> get_sub_field('marg_bot'),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
		'first_open' 		=> get_sub_field('first_open')
	);

	if ( have_rows('part') ) :
		while ( have_rows('part') ) : the_row();

			$args['items'][] = get_sub_field('faq');

		endwhile;
	endif;

	output_block_accordion( $args );
?>
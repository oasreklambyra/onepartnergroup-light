<?php 

$row_number = 1;

$id 				  = get_sub_field('id');
$marg_bot  		= get_sub_field('marg_bot');
$heading 			= get_sub_field('heading');
$text 				= get_sub_field('text');
$first_open 	= get_sub_field('first_open');


?>

<section id="<?= $id; ?>" class="section section__accordion marg-bot-<?= $marg_bot; ?>">

	<div class="container-fluid">
		<div class="row justify-content-center">


			<div class="col-12 col-md-10 col-xl-8">
				<div class="text-container main-text-container">
					<h2 class="heading"><?= $heading;  ?></h2>
					<div class="text"><?= $text; ?></div>
				</div>
			</div>


		</div>
	</div>

	<div id="accordion-1" class="accordion" role="tablist" aria-multiselectable="true">

		<?php 
		if ( have_rows('part') ) :
			while ( have_rows('part') ) : the_row();

				$part_heading  = get_sub_field('part_heading');
				$part_text 		 = get_sub_field('part_text');

				?>

				<div class="card">

					<div class="card-header" role="tab" id="heading-<?= $row_number; ?>">
						

						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 col-xl-8">

									<h3 class="heading card-heading">

										<a class="no-scroll" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?= $row_number; ?>" aria-expanded="false" aria-controls="collapse-">
											<?= $part_heading; ?>
										</a>

									</h3>

								</div>
							</div>
						</div>

					</div>

					<div id="collapse-<?= $row_number; ?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?= $row_number; ?>">

						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 col-xl-8">

									<div class="card-block">
										<div class="text-container text-two-columns">
											<?= $part_text; ?>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>


				<?php
				$row_number++;
			endwhile;
		endif;

		?>

	</div>
</section>
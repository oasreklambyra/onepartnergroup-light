<?php 
	$args = array(
		'id' 			=> get_sub_field('id'),
		'marg_bot' 		=> get_sub_field('marg_bot'),
		'show_bg'		=> get_sub_field('show_bg')
	);

	if( have_rows( 'testimonials_rows' ) ) :

		$testimonials = array();

		while( have_rows( 'testimonials_rows' ) ) : the_row();

			$testimonials[] = get_sub_field( 'testimonial' );

		endwhile;

		$args['testimonials'] = $testimonials;

	endif; 

	output_block_testimonials( $args );
?>
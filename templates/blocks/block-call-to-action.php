<?php 
	global $row_number;
	$bg_type = get_sub_field('bg_type');
	$content_type = get_sub_field('content_type');

	$args = array(
		'id' 				=> get_sub_field('id'),
		'row_number' 		=> $row_number,
		'marg_bot' 			=> get_sub_field('marg_bot'),
		'pad_top' 			=> get_sub_field('padding_top'),
		'pad_bot' 			=> get_sub_field('padding_bot'),
		'bg_type' 			=> $bg_type,
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
		'content_type' 		=> $content_type,
		'position' 			=> get_sub_field('positioning'),
	);

	if ( $bg_type === 'image' ) : 

		$args['bg_image'] = get_sub_field('bg_image');

	elseif ( $bg_type === 'color' ) :

		$args['bg_color'] = get_sub_field('bg_color');

	endif;


	if( $content_type === 'link' && have_rows('links') ) :

		while( have_rows('links') ) : the_row();

			$link_type = get_sub_field('link_type');

			$link = array(
				'link_type' => $link_type
			);

			if( $link_type !== 'office' && $link_type !== 'office_contact' ) :

				$permalink = get_sub_field($link_type);

				if ( $link_type === 'dynamic' ) : 
					$standard_slug = str_replace( get_home_url(), '', $permalink);
					$permalink = get_dynamic_link( $standard_slug );
				endif; 

				$link['title'] = get_sub_field('link_title');
				$link['link'] = $permalink;

			endif; 

			$args['links'][] = $link;

		endwhile;

	elseif ( $content_type === 'newsletter' ) : 

		$args['use_unique_code'] = get_sub_field('use_unique_code');
		$args['unique_code'] = get_sub_field('unique_code');

	endif;


	output_block_cta( $args );
?>
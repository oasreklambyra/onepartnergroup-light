<?php 
	$ele_id = ( get_sub_field('id') ) ? ' id="'.get_sub_field('id').'"': '';
	$marg_bot = get_sub_field('marg_bot');
	$heading = get_sub_field('heading');
	$text = get_sub_field('text');
	$fb_id = get_sub_field('facebook_id');
	$shortcode_id = ( $fb_id ) ? ' id="'.$fb_id.'"': '';
	$shortcode = '[custom-facebook-feed'.$shortcode_id.']';
?>

<section<?= $ele_id ?> class="container-fluid section section__facebook marg-bot-<?= $marg_bot ?>">
	<div class="row justify-content-center">
		<div class="col-12 col-md-10">

			<div class="row justify-content-center">
				
				<div class="col-12 col-xl-custom">
					<?php if( $heading || $text ) : ?>
						<div class="text-container">
							<?= ( $heading ) ? '<h2 class="heading">'.$heading.'</h2>': ''; ?>
							<?= ( $text ) ? '<p class="text">'.$text.'</p>': ''; ?>
						</div>
					<?php endif; ?>
				</div>

			</div>
			
			<div class="row">		
				<?= do_shortcode( $shortcode ) ?>
			</div>
			
			<div class="row">
				<div class="col-12 btn-container text-center">
					<a class="btn--blue" href="https://www.facebook.com/<?= ( $fb_id ) ? $fb_id : 'onepartnergroup.se'; ?>" target="_blank"><?= lang_text( 'Gå till Facebook', 'Go to Facebook' ); ?></a>
				</div>
			</div>

		</div>
	</div>
</section>
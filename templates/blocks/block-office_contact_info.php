<?php 

$office = get_sub_field( 'office' );

if ( $office ) :

	$args = array(
		'id'  				=> get_sub_field( 'id' ),
		'marg_bot'  		=> get_sub_field( 'marg_bot' ),
		'show_bg' 			=> false,
	);
	
	$contact = '';
	$contact .= ( $temp = get_field('office_main_mail', $office->ID ) ) ? convert_string_to_link( $temp, 'mail' ).'</br>' : '';
	$contact .= ( $temp = get_field('office_main_phone', $office->ID ) ) ? convert_string_to_link( $temp, 'phone' ).'</br>' : '';
	
	if( $contact ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Kontaktuppgifter', 'Contact' ),
			'text'		=> $contact
		);
	endif;

	$adress = '';
	$adress .= ( $temp = get_field('office_visitors_address', $office->ID ) ) ? $temp.'</br>' : '';
	$adress .= ( $temp = get_field('office_zip_code', $office->ID ) ) ? $temp.'</br>' : '';

	if( $adress ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Besöksadress', 'Address' ),
			'text'		=> $adress
		);
	endif;

	$billing = '';
	$billing .= ( $temp = get_field('office_inovice_mail', $office->ID ) ) ? convert_string_to_link( $temp, 'mail' ).'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_name', $office->ID ) ) ? $temp.'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_address', $office->ID ) ) ? $temp.'</br>' : '';
	$billing .= ( $temp = get_field('office_inovice_zip', $office->ID ) ) ? $temp.'</br>' : '';

	if( $billing ) :
		$args['items'][] = array(
			'heading' 	=> lang_text( 'Faktureringsadress', 'Billing information' ),
			'text'		=> $billing
		);
	endif;

	output_block_info_block( $args );

endif;
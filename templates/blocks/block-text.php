<?php 
	$args = array(
		'id'  				=> get_sub_field('id'),
		'marg_bot'  		=> get_sub_field('marg_bot'),
		'marg_heading'  	=> get_sub_field('marg_heading'),
		'padding'  			=> get_sub_field('padding'),
		'bg_color'  		=> get_sub_field('bg_color'),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
		'content_placement' => get_sub_field('content_placement'),
		'display_type' 		=> get_sub_field('display_type')
	);

	output_block_text( $args );
?>
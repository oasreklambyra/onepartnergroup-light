<?php 
	$args = array(
		'id' 		=> get_sub_field('id'),
		'marg_bot' 	=> get_sub_field('marg_bot'),
		'heading' 	=> get_sub_field('heading'),
		'text' 		=> get_sub_field('text'),
		'shortcode' => get_sub_field('shortcode'),
	);

	output_block_contactform( $args );
?>
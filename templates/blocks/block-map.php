<?php 
	$args = array(
		'id'  				=> get_sub_field('id'),
		'marg_bot'  		=> get_sub_field('marg_bot'),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
		'map_heading' 		=> get_sub_field('map_heading'),
		'map_text' 			=> get_sub_field('map_text'),
	);

	if ( have_rows('maps') ) :
		while( have_rows('maps') ) : the_row();

			if ( get_sub_field('marker_type') === 'office' ) : 

				$office = get_sub_field('office');
				$args['markers'][] = get_field('map',$office->ID);

			else :

				$args['markers'][] = get_sub_field('custom');

			endif;

		endwhile;
	endif;

	output_block_map( $args );
?>
<?php 
	$youtube_id = ( $temp_ytp = get_sub_field('youtube_id') ) ? 'http://youtu.be/'.$temp_ytp : '';

	$args = array(
		'id' 				=> get_sub_field('id'),
		'marg_bot' 			=> get_sub_field('marg_bot'),
		'media_placement' 	=> get_sub_field('media_placement'),
		'media_type' 		=> get_sub_field('media_type'),
		'youtube_id'		=> $youtube_id,
		'image'				=> get_sub_field( 'image' ),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
	);

	output_block_text_video_image( $args );
?>
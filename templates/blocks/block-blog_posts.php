<?php 
	global $row_number;
	$post_type = get_sub_field('post_type');
	$show_posts = get_sub_field('show_posts');
	
	$current_cat = ( $post_type === 'news' ) ? get_sub_field( 'preset_city_news' ) : get_sub_field( 'preset_category' );

	$args = array(
		'id'  			=> get_sub_field('id'),
		'row_number'  	=> $row_number,
		'marg_bot'  	=> get_sub_field('marg_bot'),
		'heading' 		=> get_sub_field('heading'),
		'text' 			=> get_sub_field('text'),
		'post_type' 	=> $post_type,
		'show_posts' 	=> $show_posts,
		'current_cat' 	=> $current_cat,
	);

	if( $show_posts === 'custom' ) :

		while( have_rows( 'preset_posts' ) ) : the_row();

			if ( $post_type === 'news' ) :
				$args['preset_posts'][] = get_sub_field( 'news' );

			else :
				$args['preset_posts'][] = get_sub_field( 'post' );

			endif;

		endwhile;

	elseif ( $show_posts === 'categories' && $current_cat ) :

		$args['custom_archive_link'] = get_term_link( $current_cat, $current_cat->taxonomy );

	endif;

	output_block_blog_posts( $args );
?>
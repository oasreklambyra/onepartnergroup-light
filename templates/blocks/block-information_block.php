<?php 
	$args = array(
		'id'  				=> get_sub_field('id'),
		'marg_bot'  		=> get_sub_field('marg_bot'),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
		'show_bg' 			=> get_sub_field('show_background')
	);

	if( have_rows( 'information' ) ) :

		$items_array = array();

		while( have_rows( 'information' ) ) : the_row();

			$type = get_sub_field( 'info_type' );

			if( $type === 'branch' ) :

				$branch = get_sub_field( 'branch' );
				$title = get_the_title( $branch->ID );
				$text = get_field( 'text', $branch->ID );

				$items_array[] = array(
					'heading' 	=> $title,
					'text'		=> $text
				);
			elseif ( $type === 'custom_text' ) :

				$items_array[] = array(
					'heading' 	=> get_sub_field( 'custom_heading' ),
					'text'		=> get_sub_field( 'custom_text' )
				);
				
			endif; 

		endwhile;

		$args['items'] = $items_array;

	endif;

	output_block_info_block( $args );
?>
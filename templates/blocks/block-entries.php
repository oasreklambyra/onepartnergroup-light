<?php 
	global $row_number;

	$args = array(
		'id' 			=> get_sub_field('id'),
		'row_number' 	=> $row_number,
		'marg_bot'		=> get_sub_field('marg_bot'),
		'heading'		=> get_sub_field('heading'),
		'text'			=> get_sub_field('text')
	);

	if ( have_rows('entries') ) :

		$entries = array();

		while( have_rows('entries') ) : the_row();

			$bg_image = get_sub_field('bg_image');

			$link_type = get_sub_field('link_type' );
			$permalink = get_sub_field($link_type);

			if ( $link_type === 'dynamic' ) : 

				$standard_slug = str_replace( get_home_url(), '', $permalink);
				$permalink = get_dynamic_link( $standard_slug );

			endif; 

			$entries[] = array(
				'type'		  => 'entry',
				'use_bg'	  => true,
				'permalink'   => $permalink,
				'title' 	  => get_sub_field('heading'),
				'text' 	  	  => get_sub_field('text'),
				'target' 	  => get_sub_field('target'),
				'image_sizes' => array(
					array(
						'url' 	 => $bg_image['sizes']['entry'],
						'retina' => $bg_image['sizes']['entry_retina'],
					),
				),
			);

		endwhile;

		$args['links'] = $entries;

	endif;

	output_block_entires( $args );
?>
<?php 
	$marg_bot = get_sub_field('marg_bot');
	$show_line = get_sub_field('show_line');
?>
<section class="marg-bot-<?= $marg_bot ?>">
	<div class="container-fluid">		
		<div class="row justify-content-center">
			<div class="col-12 col-md-10 col-xl-8">

				<?= ( $show_line ) ? '<div class="divider"></div>' : ''; ?>

			</div>
		</div>
	</div>
</section>
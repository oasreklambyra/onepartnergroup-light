<?php 
	$args = array(
		'id'  				=> get_sub_field('id'),
		'marg_bot'  		=> get_sub_field('marg_bot'),
		'heading' 			=> get_sub_field('heading'),
		'text' 				=> get_sub_field('text'),
		'content_placement' => get_sub_field('content_placement'),
	);

	$coworker_type = get_sub_field( 'coworker_type' );

	if ( $coworker_type === 'from_office' ) : 

		$office = get_sub_field('from_office');

		if ( have_rows( 'office_coworkers', $office->ID ) ) : 
			while ( have_rows( 'office_coworkers', $office->ID ) ) : the_row();

				$coworker_area = array();
				$coworker_area['area'] = get_sub_field( 'area' );

				while ( have_rows( 'coworkers' ) ) : the_row();

					$coworker_area['coworkers'][] = get_sub_field( 'coworker' );

				endwhile;

				$args['coworker_areas'][] = $coworker_area;

			endwhile;
		endif;

	elseif ( $coworker_type === 'custom_coworkers' ) :

		if ( have_rows( 'custom_coworkers' ) ) : 
			while ( have_rows( 'custom_coworkers' ) ) : the_row();

				$coworker_area = array();
				$coworker_area['area'] = get_sub_field( 'area' );

				while ( have_rows( 'coworkers' ) ) : the_row();

					$coworker_area['coworkers'][] = get_sub_field( 'coworker' );

				endwhile;

				$args['coworker_areas'][] = $coworker_area;

			endwhile;
		endif;

	endif;

	output_block_coworkers( $args );
?>
<?php
$args = array(
	'heading' 	=> get_field( 'missing_heading', 'options' ),
	'text' 		=> get_field( 'missing_text', 'options' )
);
output_intro_element( $args );


echo '<div class="pad-bot-100"></div>';

?>


<div class="container">
	<div class="row">
		<div class="col-12 text-center">
			<h1>404 - Du har råkat hamna lite fel!</h1>
			<br>
			<h4>Klicka vidare i menyn för att hamna rätt igen :)</h4>
		</div>
	</div>
</div>
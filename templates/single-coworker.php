<?php
	global $post;

	//Single coworker has a redirect if new_permalink is set, serach for coworker_single_redirect in theme_functions

	$intro_element_array = array(
		'/dist/images/header-element-1.svg',
		'/dist/images/header-element-2.svg',
		'/dist/images/header-element-3.svg',
		'/dist/images/header-element-4.svg',
	); 
	$rand_ele = rand(0,(count($intro_element_array)-1));
	$intro_element_url = get_template_directory().$intro_element_array[1];

	$firstname = get_field('name');
	$surname = get_field('surname');
	$content = get_field('about');
?>
<section class="section section__intro section__intro__element single-post">
	<div class="background-element-container">

		<div class="background-element"><?= file_get_contents($intro_element_url); ?></div>

		<div class="container-fluid">
			<div class="row justify-content-center">

				<div class="col-12 col-md-10 col-xl-8">

					<div class="text-container">
						<h1 class="heading"><?= $firstname.'</br>'.$surname ?></h1>
					</div>

				</div>
			</div>
		</div>

	</div>
</section>


<section class="section section__content section__content__coworker">

	<div class="container-fluid">
		<div class="row justify-content-center">

			<div class="col-12 col-md-5 col-xl-4 order-md-2">
				
				<?php output_coworker_card( $post, array( 'name' => '', 'surname' => '' ) ); ?>

			</div>

			<?php if ( $content || have_rows( 'experiences' ) || have_rows( 'educations' ) ) : ?>

				<div class="col-12 col-md-5 col-xl-4 order-md-1">
					
					<?php if ( $content ) : ?>

						<div class="post-content">
							<?= $content ?>
						</div>

					<?php endif; ?>
					
					<?php if ( have_rows( 'experiences' ) || have_rows( 'educations' ) ) : ?> 

						<div class="meta-box single-col">
						
							<?php if( have_rows( 'experiences' ) ) : ?>
								<li class="meta-item">
									<h4 class="heading"><?= lang_text( 'Tidigare erfarenheter', 'Past experiences' ) ?></h4>
									
									<?php while ( have_rows( 'experiences' ) ) : the_row(); ?>
										
										<p class="meta-info"><?php the_sub_field( 'experience' ); ?></p>

									<?php endwhile; ?>

								</li>
							<?php endif; ?>
						
							<?php if( have_rows( 'educations' ) ) : ?>
								<li class="meta-item">
									<h4 class="heading"><?= lang_text( 'Utbildningar', 'Education' ) ?></h4>
									
									<?php while ( have_rows( 'educations' ) ) : the_row(); ?>
										
										<p class="meta-info"><?php the_sub_field( 'education' ); ?></p>

									<?php endwhile; ?>

								</li>
							<?php endif; ?>

						</div>

					<?php endif; ?>

				</div>

			<?php endif; ?>

		</div>
	</div>

</section>
<?php
	$args = array(
		'heading' 	=> get_field( 'intro_heading' ),
		'text' 		=> get_field( 'intro_text' )
	);
	output_intro_element( $args );
?>

<?php get_template_part('templates/content/content', 'flexible'); ?>
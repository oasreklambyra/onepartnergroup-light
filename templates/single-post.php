<?php

global $post;

$post_type = get_post_type();

$image = get_field( 'main_image' );
$title = get_the_title();
$permalink = get_the_permalink();

$category = ( $post_type === 'news' ) ? 'news_category' : 'category' ;
$terms = wp_get_post_terms( get_the_ID(), $category );
$term_slugs = array();
$city_term_slugs = array();

$today_date = getdate();
$pub_date = get_the_date( 'Y-m-d', get_the_ID() );

$pub_date_display = $pub_date;
if( date('Y-m-d',$today_date[0]) === $pub_date ) : 
	$pub_date_display = 'Idag';

else :
	$time_since_pub = $today_date[0] - strtotime( $pub_date );
	$day = 60*60*24;
	$days_ago = (int)($time_since_pub/$day);

	if( $days_ago === 1 ) : 
		$pub_date_display = '1 dag sedan';

	elseif ( $days_ago <= 7 ) :
		$pub_date_display = $days_ago.' dagar sedan';

	endif;
endif;
?>

<?php output_intro_element(); ?>


<section class="section section__content section__content__post">

	<div class="container-fluid">
		<div class="row justify-content-center">

			<div class="col-12 col-md-10 col-xl-8">
				
				<picture>
					<source media="(min-width: 500px)" data-srcset="<?= $image['sizes']['post_main'] ?>, <?= $image['sizes']['post_main_retina'] ?> 2x">
						<source media="(min-width: 0px)" data-srcset="<?= $image['sizes']['post_thumbnail'] ?>, <?= $image['sizes']['post_thumbnail_retina'] ?> 2x">
							<img class="main-image" src="<?= $image['sizes']['post_main'] ?>" alt="<?= $image['alt'] ?>">
						</picture>

					</div>

					<div class="col-12 col-md-6 col-lg-7 col-xl-6">

						<div class="post-content">

							<h1 class="heading"><?= $title; ?></h1>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

								<?php the_content(); ?>

							<?php endwhile; else : ?>
							<p><?php esc_html_e( 'Sorry, ingen post här' ); ?></p>
						<?php endif; ?>

					</div>

				</div>

				<div class="col-12 col-md-4 col-lg-3 col-xl-2">

					<ul class="meta-box single-col">

						<li class="meta-item">
							<h4 class="heading">Publicerat</h4>
							<p class="meta-info"><?= $pub_date_display ?></p>
						</li>

						<?php if ( $terms ) : ?>
							<li class="meta-item">
								<h4 class="heading">Kategorier</h4>

								<?php foreach ( $terms as $term ) : ?>

									<p class="meta-info"><a href="<?= get_category_link( $term->term_id ); ?>"><?= $term->name; ?></a></p>

									<?php $term_slugs[] = $term->slug; ?>

								<?php endforeach; ?>
							</li>
						<?php endif; ?>

					</ul>

				</div>

			</div>
		</div>

	</section>



	<?php 
	$similar_category = ( $post_type === 'news' ) ? 'news_city' : 'category' ;
	$similar_slugs = ( $post_type === 'news' && $city_term_slugs ) ? $city_term_slugs : $term_slugs;

	$args = array(
		'posts_per_page'    => 3,
		'orderby'		    => 'rand',
		'exclude'			=> get_the_ID(),
		'tax_query'		 	=> array(
			array(
				'taxonomy'	=> $similar_category,
				'field'		=> 'slug',
				'terms'		=> $similar_slugs
			)
		)
	);

	if ( $post_type === 'news' ) :
		
		$args['post_type'] 	= 'news';
		$args['orderby'] 	= 'date';
		$args['order'] 		= 'DESC';

	endif;

	$preset_posts = get_posts( $args );

	$similar_heading = 'Liknande inlägg';

	$args = array(
		'row_number'  		=> 'row-1',
		'marg_bot'  		=> '150',
		'heading' 			=> $similar_heading,
		'show_posts' 		=> 'custom',
		'hide_archive_link'	=> true,
		'preset_posts'		=> $preset_posts
	);

	output_block_blog_posts( $args );
	?>


	<?php 

	// if( have_rows('area_cta', get_option('page_for_posts', true) ) ) :

	// 	$cnt = 1;

	// 	while( have_rows('area_cta', get_option('page_for_posts', true) ) ) : the_row();

	// 		$bg_type = get_sub_field('bg_type');

	// 		$args = array(
	// 			'id' 				=> get_sub_field('id'),
	// 			'row_number' 		=> $cnt,
	// 			'marg_bot' 			=> '100',
	// 			'pad_top' 			=> get_sub_field('padding_top'),
	// 			'pad_bot' 			=> get_sub_field('padding_bot'),
	// 			'bg_type' 			=> $bg_type,
	// 			'heading' 			=> get_sub_field('heading'),
	// 			'text' 				=> get_sub_field('text'),
	// 			'content_type' 		=> 'link',
	// 			'position' 			=> get_sub_field('positioning'),
	// 		);

	// 		if ( $bg_type === 'image' ) : 

	// 			$args['bg_image'] = get_sub_field('bg_image');

	// 		elseif ( $bg_type === 'color' ) :

	// 			$args['bg_color'] = get_sub_field('bg_color');

	// 		endif;

	// 		if( have_rows('links') ) :

	// 			while( have_rows('links') ) : the_row();

	// 				$link_type = get_sub_field('link_type');

	// 				$link = array(
	// 					'link_type' => $link_type
	// 				);

	// 				$args['links'][] = $link;

	// 			endwhile;

	// 		endif;

	// 		output_block_cta( $args );
	// 		$cnt++;

	// 	endwhile;
	// endif;
	?>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MKWZFTL');</script>
<!-- End Google Tag Manager -->

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="BjmS4CiUtCm_K7fq96BMyBrZxhWXu_NySytQT6SOCHE" />

<?php 
	/*
	 * Add wpseo_title and wpseo_canonical for dynamic pages
	 */

	//Get the url so it is possible to check current location
	$url = explode('/', $_SERVER[REQUEST_URI]);
	$url = array_filter($url, create_function('$value', 'return $value !== "";'));

	global $current_office;

	$current_office = false;
	$current_office = get_posts( 
		array(
			'name'            => esc_attr($url[1]),
			'post_type'       => 'office',
			'post_status'     => 'publish',
			'posts_per_page'  => 1,
		) 
	);
	$current_office = $current_office[0];

	if ( $current_office && $url[2] ) :
		
		function filter_dynamic_wpseo_title($title) {

			global $current_office;
			$title = htmlspecialchars_decode( $title.' | '.$current_office->post_title );

			return $title;
		}
		add_filter('wpseo_title', 'filter_dynamic_wpseo_title');

		function filter_dynamic_wpseo_canonical($canonical) {

			global $current_office;
			$pos = strlen( esc_url(home_url('/')) );
			$canonical = substr_replace( $canonical, $current_office->post_name.'/', $pos, 0);

			return $canonical;
		}
		add_filter('wpseo_canonical', 'filter_dynamic_wpseo_canonical');

	endif;
?>

<?php wp_head(); ?>

<?php 
	/*
	 * Add defualt og:image if it's missing
	 */
	global $og_image;

	if ( !$og_image ) :

		$defualt_image;

		if ( is_singular( 'post' ) || is_singular( 'news' ) ) :

		  $default = get_field( 'main_image' );
		  $defualt_image = $default['sizes']['fb_image'];

		elseif ( is_singular( 'office' ) ) :

		  $default = get_field( 'intro_image' );
		  $defualt_image = $default['sizes']['fb_image'];

		endif;

		if ( $defualt_image ) :

			echo '<meta property="og:image" content="'.$defualt_image.'" />
			<meta property="twitter:image" content="'.$defualt_image.'" />';

		endif;

	endif;
?>

<link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?= get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://use.typekit.net/kbl6uoa.css">

	<!--[if IE]>
	<script src="<?php echo get_template_directory_uri().'/assets/scripts/html5shiv.js'; ?>"></script>
<![endif]-->

</head>
<?php	
global $wp_query;
$latest_post = $wp_query->posts[0];

$queried_obj = get_queried_object();
$archive_type = 'post';

$intro_heading = get_field( 'intro_heading', get_option('page_for_posts', true) );
$intro_text =  get_field( 'intro_text', get_option('page_for_posts', true) );

$args = array(
	'heading' 	=> $intro_heading,
	'text' 		=> $intro_text,
);
output_intro_element( $args );
?>

<?php 
$latest_image = ( $temp_img = get_field( 'main_image', $latest_post->ID ) ) ? $temp_img : get_default_post_image();
$latest_parmalink = get_the_permalink( $latest_post->ID );
$latest_title = get_the_title( $latest_post->ID );
$latest_excerpt = apply_custom_excerpt( $latest_post->post_content );

$today_date = getdate();
$pub_date = get_the_date( 'd-m-Y', $post->ID );

$pub_date_display = $pub_date;
if( date('d-m-Y',$today_date[0]) === $pub_date ) : 
	$pub_date_display = 'Idag';

else :
	$time_since_pub = $today_date[0] - strtotime( $pub_date );
	$day = 60*60*24;
	$days_ago = (int)($time_since_pub/$day);
	
	if( $days_ago === 1 ) : 
		$pub_date_display = '1 dag sedan';

	elseif ( $days_ago <= 7 ) :
		$pub_date_display = $days_ago.' dagar sedan';

	endif;
endif;
?>
<section class="section section__latest-post marg-bot-150">
	<div class="latest-post">
		<div class="container-fluid">
			<div class="row justify-content-center">

				<div class="no-padding col-12 col-md-10">
					<div class="row justify-content-center">
						
						<div class="image-col col-12 col-lg-7">

							<div class="image-container">
								<a href="<?= $latest_parmalink; ?>">
									
									<picture>
										<source media="(min-width: 0px)" data-srcset="<?= $latest_image['sizes']['post_main'] ?>, <?= $latest_image['sizes']['post_main_retina'] ?> 2x">
											<img class="lazyload" data-src="<?= $latest_image['sizes']['post_main'] ?>" alt="<?= $latest_image['alt'] ?>">
										</picture>

									</a>
								</div>


							</div>

							<div class="content-col col-12 col-lg-5">

								<div class="blog-item">
									<a href="<?= $latest_parmalink; ?>">

										<p class="small-heading">Senaste nyheten</p>

										<h3 class="heading"><?= $latest_title ?></h3>

										<?php if ( $archive_type === 'news' ) : ?>
											
											<?php 
											$terms = wp_get_post_terms( $latest_post->ID, 'news_city' );
											$news_cities = array();

											foreach ( $terms as $term ) : 

												$news_cities[] = $term->name;

											endforeach;
											?>
											
											<?php if ( $news_cities ) : ?>

												<div class="categories"><?= implode(' - ', $news_cities ) ?></div>

											<?php endif; ?>

										<?php endif; ?>

										<div class="excerpt"><?= $latest_excerpt ?></div>

										<div class="pub-date"><?= $pub_date_display ?></div>

									</a>
								</div>

							</div>

						</div>					
					</div>

				</div>
			</div>
		</div>
	</section>


	<?php 

	get_template_part('templates/content/content', 'posts-archive');

	?>


	<?php 

	if( have_rows('area_cta', get_option('page_for_posts', true) ) ) :

		$cnt = 1;

		while( have_rows('area_cta', get_option('page_for_posts', true) ) ) : the_row();

			$bg_type = get_sub_field('bg_type');

			$args = array(
				'id' 				=> get_sub_field('id'),
				'row_number' 		=> $cnt,
				'marg_bot' 			=> '100',
				'pad_top' 			=> get_sub_field('padding_top'),
				'pad_bot' 			=> get_sub_field('padding_bot'),
				'bg_type' 			=> $bg_type,
				'heading' 			=> get_sub_field('heading'),
				'text' 				=> get_sub_field('text'),
				'content_type' 		=> 'link',
				'position' 			=> get_sub_field('positioning'),
			);

			if ( $bg_type === 'image' ) : 

				$args['bg_image'] = get_sub_field('bg_image');

			elseif ( $bg_type === 'color' ) :

				$args['bg_color'] = get_sub_field('bg_color');

			endif;

			if( have_rows('links') ) :

				while( have_rows('links') ) : the_row();

					$link_type = get_sub_field('link_type');

					$link = array(
						'link_type' => $link_type
					);

					if( $link_type !== 'office' && $link_type !== 'office_contact' ) :

						$permalink = get_sub_field($link_type);

						if ( $link_type === 'dynamic' ) : 
							$standard_slug = str_replace( get_home_url(), '', $permalink);
							$permalink = get_dynamic_link( $standard_slug );
						endif; 

						$link['title'] = get_sub_field('link_title');
						$link['link'] = $permalink;

					endif; 

					$args['links'][] = $link;

				endwhile;

			endif;

			output_block_cta( $args );
			$cnt++;

		endwhile;
	endif;
	?>
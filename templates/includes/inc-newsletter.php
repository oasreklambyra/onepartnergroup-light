<?php 
	$newsletter = get_field( 'newsletter', 'options' ); 
?>
<?php if( $newsletter ) : ?>
	<div class="newsletter">
		<?= $newsletter ?>
	</div>
<?php endif; ?>
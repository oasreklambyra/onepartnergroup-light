<?php 
	$seen = (isset($_COOKIE['seen_cookie_box'])) ? $_COOKIE['seen_cookie_box'] : 'null';
	$cookie_text = get_field( 'cookie_text', 'options' );

	if( $seen !== 'seen' && $cookie_text ) :
?>
	<div class="cookie-container">
		<div class="container-fluid pad-0">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10">
					<div class="cookie-content">

						<p><?= $cookie_text; ?></p>
						<br>
						<button id="cookie-btn" class="btn--blue">Jag förstår</button>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>